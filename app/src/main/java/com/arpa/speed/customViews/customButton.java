package com.arpa.speed.customViews;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

import com.arpa.speed.utils.BaseApplication;

public class customButton extends AppCompatButton {
    public customButton(Context context) {
        super(context);
        this.setTypeface(BaseApplication.typeFace);
    }

    public customButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(BaseApplication.typeFace);

    }


}
