package com.arpa.speed.customViews;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

import com.arpa.speed.utils.BaseApplication;

public class customEditText extends AppCompatEditText {
    public customEditText(Context context) {
        super(context);

        this.setTypeface(BaseApplication.typeFace);

    }

    public customEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(BaseApplication.typeFace);

    }

    public String text() {
        return (this.getText() != null ? this.getText().toString() : "");
    }
}
