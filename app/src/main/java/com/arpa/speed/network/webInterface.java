package com.arpa.speed.network;

import com.arpa.speed.models.business.businessDbModel;
import com.arpa.speed.models.itemAttach.itemAttachDbModel;
import com.arpa.speed.models.itemCats.itemCatsDbModel;
import com.arpa.speed.models.items.itemsDbModel;
import com.arpa.speed.models.login.loginDbModel;
import com.arpa.speed.models.restTable.restTableDbModel;
import com.arpa.speed.models.restTableCat.restTableCatDbModel;
import com.arpa.speed.models.savedTrans.SavedTransactionsModel;
import com.arpa.speed.models.transaction.ObjectToSend;
import com.arpa.speed.models.userDefaults.userDefaultsDbModel;
import com.arpa.speed.modules.order.dbModel.Model;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface webInterface {

    @GET("/serv/Token/GetPingServerToken")
    Observable<loginDbModel> getPingServerToken(
            @Query("username") String username
            ,
            @Query("password") String password
    );

    @GET("/serv/AppReport/GetRestTableCategories")
    Observable<restTableCatDbModel> getRestTableCategories(
            @Query("userId") Integer userId
    );

    @GET("/serv/AppReport/GetRestTable")
    Observable<restTableDbModel> getRestTable(
            @Query("userId") Integer userId
    );

    @GET("/serv/AppReport/GetDefaults")
    Observable<userDefaultsDbModel> getDefaults(
            @Query("userId") Integer userId
    );

    @GET("/serv/AppReport/GetBusiness")
    Observable<businessDbModel> getBusiness(
            @Query("userId") Integer userId
    );

    @GET("/serv/AppReport/getItemCats")
    Observable<itemCatsDbModel> getItemCats(
            @Query("userId") Integer userId
    );

    @GET("/serv/AppReport/GetItems")
    Observable<itemsDbModel> getItems(
            @Query("userId") Integer userId
    );

    @GET("/serv/AppReport/GetItemAttachment")
    Observable<itemAttachDbModel> getItemAttachment(
            @Query("itemId") Integer itemId
    );

    @GET("/serv/AppReport/GetTransactions")
    Observable<SavedTransactionsModel> getTransactions(
            @Query("userId") Integer userId,
            @Query("transactionId") Integer transactionId
    );

    @POST("/serv/AppReport/PostTransaction")
    Observable<Model> postTransaction(@Body ObjectToSend data);



}
