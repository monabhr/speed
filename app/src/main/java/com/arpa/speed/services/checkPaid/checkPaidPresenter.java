package com.arpa.speed.services.checkPaid;

import android.app.Service;

public class checkPaidPresenter implements checkPaidContract.check_paid_presenter {

    checkPaidContract.check_paid_service paidService;
    checkPaidContract.check_paid_model model = new checkPaidModel();

    public checkPaidPresenter(checkPaidContract.check_paid_service service) {
        this.paidService = service;
        model.onAttachPresenter(this);
    }

    @Override
    public void onCheckPaidAmount() {
        model.onCheckPaidAmount();
    }

    @Override
    public void onCheckPaidAmountSucceed() {
        paidService.onCheckPaidAmountSucceed();
    }

    @Override
    public void onCheckPaidAmountFailed(String message) {
        paidService.onCheckPaidAmountFailed(message);
    }
}
