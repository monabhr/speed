package com.arpa.speed.services.checkPaid;

import android.os.Handler;
import android.os.Looper;

import com.arpa.speed.boxDatabase.DatabaseAdapter;
import com.arpa.speed.boxDatabase.saveTransEntity;
import com.arpa.speed.boxDatabase.savedTLineEntity;
import com.arpa.speed.boxDatabase.tableOrderEntity;
import com.arpa.speed.models.login.loginDbModel;
import com.arpa.speed.models.savedTrans.SavedTransactionsModel;
import com.arpa.speed.models.savedTrans.Transline;
import com.arpa.speed.modules.login.dbModel.userModel;
import com.arpa.speed.network.webInterface;
import com.arpa.speed.utils.Constants;
import com.arpa.speed.utils.RxRetoGenerator;
import com.arpa.speed.utils.hawkHandler;
import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class checkPaidModel implements checkPaidContract.check_paid_model {

    checkPaidContract.check_paid_presenter presenter;
    Disposable disposable;
    DatabaseAdapter dbAdapter = new DatabaseAdapter();
    userModel userModel;
    hawkHandler hawk = hawkHandler.getInstance();

    @Override
    public void onAttachPresenter(checkPaidContract.check_paid_presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onCheckPaidAmount() {
        try {

            userModel = hawk.getData(Constants.user_model);
            webInterface webInterface = RxRetoGenerator.createPingService(com.arpa.speed.network.webInterface.class,
                    userModel.getServerAddress());


            Observable<SavedTransactionsModel> observable = webInterface.getTransactions(userModel.getUserID(), 0);
            observable
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<SavedTransactionsModel>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            Logger.d("SavedTransactionsModel onSubscribe");
                            disposable = d;
                        }

                        @Override
                        public void onNext(SavedTransactionsModel savedTransactionsModel) {
                            saveTransEntity(savedTransactionsModel);
                            presenter.onCheckPaidAmountSucceed();
                        }

                        @Override
                        public void onError(Throwable e) {
                            presenter.onCheckPaidAmountFailed(e.getLocalizedMessage());
                        }

                        @Override
                        public void onComplete() {

                        }
                    });


        } catch (Exception ex) {
            presenter.onCheckPaidAmountFailed(ex.getLocalizedMessage());
        }

    }

    private void saveTransEntity(SavedTransactionsModel objs) {

        List<saveTransEntity> objEntity = new ArrayList<>();

        if (objs.getData() != null) {
            for (com.arpa.speed.models.savedTrans.Datum data : objs.getData()) {

                saveTransEntity item = new saveTransEntity();

                if (data.getPaidAmount() > 0) {
                    deleteOrder(data.getTransactionID(), data.getRestaurantTableID());
                    deleteSaveTransEntity(data.getTransactionID(), data.getRestaurantTableID());
                } else {
                    item.creatorUserID = data.getCreatorUserID();
                    item.businessID = data.getBusinessID();
                    item.paidAmount = data.getPaidAmount();
                    item.transNumber = data.getTransNumber();
                    item.totalSum = 0;
                    item.transDiscountPercent = data.getTransDiscountPercent();
                    item.transDiscountPercent = data.getTransDiscountAmount();
                    item.restaurantTableID = data.getRestaurantTableID();
                    item.transactionID = Integer.parseInt(data.getTransactionID() + "");
                    item.setArchived(false);

                    for (Transline s : data.getTranslines()) {
                        savedTLineEntity obj = new savedTLineEntity();
                        obj.setItemId(s.getItemId());
                        obj.setTransactionId(Integer.parseInt(s.getTransactionId() + ""));
                        obj.setTollAmount(s.getTollAmount());
                        obj.setTaxAmount(s.getTaxAmount());
                        obj.setPrice(s.getPrice());
                        obj.setnQty(Integer.parseInt(s.getNQty().toString()));
                        obj.setTranslineId(s.getTranslineId());
                        obj.setItemCategoryId(s.getItemCategoryId());

                        item.translines.add(obj);
                        addOrderList(item, s);

                    }

                    objEntity.add(item);

                }
            }

            dbAdapter.insertSavedTransactions(objEntity);
        }
    }

    private void deleteSaveTransEntity(Integer transactionID, Integer restaurantTableID) {
        List<saveTransEntity> list = dbAdapter.getSavedTransactions(transactionID, restaurantTableID);
        for (saveTransEntity ent : list) {
            dbAdapter.clearSaveTransEntity(ent);
        }
    }

    private void addOrderList(saveTransEntity item, Transline obj) {

        tableOrderEntity order = new tableOrderEntity();

        tableOrderEntity reservedOrder = dbAdapter.getOrderByTableId(
                Long.parseLong(item.getRestaurantTableID() + ""), obj.getTranslineId());

        if (reservedOrder != null) {
            order = reservedOrder;
            order.setId(reservedOrder.getId());
        }

        order.setPaidAmount(item.getPaidAmount());
        order.setTableID(item.getRestaurantTableID());
        order.setTranslineID(obj.getTranslineId());
        order.setTransNumber(item.getTransNumber());
        order.setUserId(item.getCreatorUserID());
        order.setRowSum(Double.parseDouble(obj.getAmount().toString()));
        order.setUnitsRatio(0.0);
        order.setItemName(obj.getItemName());
        order.setItemCount(obj.getnQty());
        order.setTransactionID(obj.getTransactionId());
        order.setItemID(obj.getItemId());
        order.setSalePrice(Double.parseDouble(obj.getPrice().toString()));
        order.setItemCategoryID(obj.getItemCategoryId());
        order.setArchived(false);

        dbAdapter.insertOrder(order);
    }

    void deleteOrder(int transactionId, int tableId) {

        List<tableOrderEntity> reservedList = dbAdapter.getOrderByTableId(Long.parseLong(tableId + ""));

        for (tableOrderEntity ent : reservedList) {
            dbAdapter.clearOrder(ent);
        }

    }
}
