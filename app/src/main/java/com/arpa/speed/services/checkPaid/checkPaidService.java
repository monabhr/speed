package com.arpa.speed.services.checkPaid;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.arpa.speed.R;
import com.arpa.speed.modules.home.HomeActivity;
import com.arpa.speed.utils.BaseApplication;
import com.arpa.speed.utils.publicUtils;
import com.orhanobut.logger.Logger;

import java.util.Timer;
import java.util.TimerTask;

import static android.provider.ContactsContract.Directory.PACKAGE_NAME;

public class checkPaidService extends Service
        implements checkPaidContract.check_paid_service {

    private static final String TAG = checkPaidService.class.getSimpleName();

    checkPaidContract.check_paid_presenter presenter = new checkPaidPresenter(this);

    /**
     * The name of the channel for notifications.
     */
    private static String CHANNEL_ID = "speed_channel";
    private static int Notif_ID = 1339;
    Handler mServiceHandler = new Handler();
    private Timer mTimer = null;    //timer handling

    private final IBinder mBinder = new LocalBinder();
    private static final String EXTRA_STARTED_FROM_NOTIFICATION = PACKAGE_NAME +
            ".started_from_notification";

    private boolean mChangingConfig = false;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
       /* Logger.i(TAG, "in onBind()");
        //return (mBinder);
        stopForeground(true);
        mChangingConfig = false;
        return mBinder;*/
    }

  /*  @Override
    public void onRebind(Intent intent) {
        // Called when a client (MainActivity in case of this sample) returns to the foreground
        // and binds once again with this service. The service should cease to be a foreground
        // service when that happens.
        Logger.i(TAG, "in onRebind()");
        stopForeground(true);
        mChangingConfig = false;
        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Logger.i(TAG, "Last client unbound from service");

        // Called when the last client (MainActivity in case of this sample) unbinds from this
        // service. If this method is called due to a configuration change in MainActivity, we
        // do nothing. Otherwise, we make this service a foreground service.
        Logger.i(TAG, "Starting foreground service");
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.O) {
            startForeground(Notif_ID, getNotification());
        } else {
            startService(new Intent(getBaseContext(), checkPaidService.class));
        }

        return true; // Ensures onRebind() is called when a client re-binds.
    }
*/

    /**
     * Returns the {@link NotificationCompat} used as part of the foreground service.
     */
    private Notification getNotification() {
        Intent intent = new Intent(this, checkPaidService.class);

        //CharSequence text = Utils.getLocationText(mLocation);

        // Extra to help us figure out if we arrived in onStartCommand via the notification or not.
        intent.putExtra(EXTRA_STARTED_FROM_NOTIFICATION, true);

        // The PendingIntent that leads to a call to onStartCommand() in this service.
        PendingIntent servicePendingIntent = PendingIntent.getService(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);


        PendingIntent activityPendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, HomeActivity.class), 0);

        //String CHANNEL_ID =CHANNEL_ID;


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            Intent intentt = new Intent(this, checkPaidService.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, intentt, PendingIntent.FLAG_ONE_SHOT);

            NotificationCompat.Action action1 = new NotificationCompat.Action.Builder(R.drawable.ic_launcher_foreground,
                    "Previous", activityPendingIntent).build();
            NotificationCompat.Action action2 = new NotificationCompat.Action.Builder(R.drawable.ic_launcher_foreground,
                    "Previous", servicePendingIntent).build();

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText("")
                    .setSmallIcon(R.drawable.ic_launcher_background)
                    .addAction(action1)
                    .setPriority(NotificationManager.IMPORTANCE_LOW)
                    .addAction(action2)
                    .setAutoCancel(true);

            Notification notification = builder.build();
            return notification;


        } else {


            Notification builder = new Notification.Builder(this)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText("")
                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                    .addAction(R.drawable.ic_launcher_foreground, getString(R.string.launch_activity),
                            activityPendingIntent)
                    .addAction(R.drawable.ic_launcher_foreground, getString(R.string.remove_location_updates),
                            servicePendingIntent)
                    .setPriority(Notification.PRIORITY_LOW)
                    .setAutoCancel(true).build();

            return builder;
        }


    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (mTimer != null) // Cancel if already existed
            mTimer.cancel();
        else
            mTimer = new Timer();   //recreate new
        mTimer.scheduleAtFixedRate(new TimeDisplay(), 0, 60000);   //Schedule task

        HandlerThread handlerThread = new HandlerThread(TAG);
        handlerThread.start();
        mServiceHandler = new Handler(handlerThread.getLooper());

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Logger.e(TAG, "onStartCommand");
        startServiceWithNotification();
        return START_STICKY;
    }

    final Runnable ToastRunnable = new Runnable() {
        public void run() {
            publicUtils.toast("getting app count");
            mServiceHandler.postDelayed(ToastRunnable, 60000);
        }
    };


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mChangingConfig = true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        System.gc();
        mTimer.cancel();    //For Cancel Timer
        //publicUtils.toast("Service is Destroyed");
        mServiceHandler.removeCallbacksAndMessages(null);
    }

    //class TimeDisplay for handling task
    class TimeDisplay extends TimerTask {
        @Override
        public void run() {
            // run on another thread
            mServiceHandler.post(new Runnable() {
                @Override
                public void run() {
                    // display toast
                    if (publicUtils.isConnectedToInternet())
                        presenter.onCheckPaidAmount();
                    // publicUtils.toast("Service is running");
                }
            });
        }
    }

    /**
     * Class used for the client Binder.  Since this service runs in the same process as its
     * clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        public checkPaidService getService() {
            return checkPaidService.this;
        }
    }

    @Override
    public void onCheckPaidAmountSucceed() {
        sendMessageToActivity(true);
    }

    private static void sendMessageToActivity(Boolean state) {
        Intent intent = new Intent("GPSLocationUpdates");
        intent.putExtra("Status", state);
        Bundle b = new Bundle();
        b.putBoolean("Status", state);

        intent.putExtra("Location", b);
        LocalBroadcastManager.getInstance(BaseApplication.getApp()).sendBroadcast(intent);
    }

    @Override
    public void onCheckPaidAmountFailed(String message) {

    }

    private void startServiceWithNotification() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    "اسپپد",
                    NotificationManager.IMPORTANCE_LOW);

            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);

            Notification notification = new NotificationCompat
                    .Builder(this, CHANNEL_ID)
                    .setContentTitle("")
                    .setVibrate(null)
                    .setSound(null, 0)
                    .setOngoing(true)
                    //.setContentIntent(contentPendingIntent)
                    .setVisibility(NotificationCompat.VISIBILITY_PRIVATE)
                    .setPriority(NotificationManager.IMPORTANCE_LOW)
                    .setContentText("").build();

            this.startForeground(Notif_ID, notification);
        }


    }

/*    public void removeCheckPaidUpdates() {
        Logger.i(TAG, "Removing paid updates");
        try {
            stopSelf();
        } catch (SecurityException unlikely) {
            Logger.e(TAG, "Lost  permission. Could not remove updates. " + unlikely);
        }
    }

    public void requestCheckPaidUpdates() {
        Logger.i(TAG, "Requesting location updates");
        //Utils.setRequestingLocationUpdates(this, true);

        Intent intent = new Intent(getApplicationContext(), checkPaidService.class);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            ContextCompat.startForegroundService(getBaseContext(), intent);
        } else {
            startService(intent);
        }


        try {
            presenter.onCheckPaidAmount();

        } catch (SecurityException unlikely) {
            // Utils.setRequestingLocationUpdates(this, false);
            Logger.e(TAG, "Lost location permission. Could not request updates. " + unlikely);
        }
    }*/
}

