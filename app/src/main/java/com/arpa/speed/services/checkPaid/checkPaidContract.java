package com.arpa.speed.services.checkPaid;

public interface checkPaidContract  {

    interface check_paid_service{
        void onCheckPaidAmountSucceed();
        void onCheckPaidAmountFailed(String message);
    }


    interface  check_paid_presenter{
        void onCheckPaidAmount();
        void onCheckPaidAmountSucceed();
        void onCheckPaidAmountFailed(String message);
    }

    interface  check_paid_model{

        void onAttachPresenter(check_paid_presenter presenter);
        void onCheckPaidAmount();



    }

}
