package com.arpa.speed.modules.restTableCat;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arpa.speed.R;
import com.arpa.speed.boxDatabase.restTableCat;
import com.arpa.speed.customViews.customTextView;

import java.util.List;

public class restTableCatAdapter extends RecyclerView.Adapter<restTableCatAdapter.ViewHolder> {

    List<restTableCat> restTableCatList;
    Context mContext;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    public restTableCatAdapter(Context mContext, List<restTableCat> restTableCatList) {
        this.mInflater = LayoutInflater.from(mContext);
        this.restTableCatList = restTableCatList;
        this.mContext = mContext;
    }

    @Override
    @NonNull
    public restTableCatAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.recycler_table_cat_items, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull restTableCatAdapter.ViewHolder holder, int position) {
        holder.cat_name.setText(restTableCatList.get(position).restaurantTableCategoryName);
    }

    @Override
    public int getItemCount() {
        return restTableCatList.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        customTextView cat_name;
      //  RippleView cat_name_ripple;

        ViewHolder(View itemView) {
            super(itemView);
            cat_name = itemView.findViewById(R.id.cat_name);
            itemView.setOnClickListener(this);
             //cat_name_ripple = itemView.findViewById(R.id.cat_name_ripple);
            //cat_name_ripple.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemRestClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public restTableCat getItem(int position) {
        return restTableCatList.get(position);
    }


    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemRestClick(View view, int position);
    }
}
