package com.arpa.speed.modules.restTable;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arpa.speed.R;
import com.arpa.speed.boxDatabase.DatabaseAdapter;
import com.arpa.speed.boxDatabase.restTableEntity;
import com.arpa.speed.boxDatabase.saveTransEntity;
import com.arpa.speed.customViews.customTextView;

import java.util.List;


public class restTableAdapter extends RecyclerView.Adapter<restTableAdapter.ViewHolder> {
    List<restTableEntity> restTableList;
    Context mContext;
    DatabaseAdapter databaseAdapter;
    private LayoutInflater mInflater;
    private restTableAdapter.ItemTableClickListener mClickListener;
    int row_index;
    private boolean onBind;

    public restTableAdapter(Context mContext, List<restTableEntity> restTableList) {
        this.mInflater = LayoutInflater.from(mContext);
        this.restTableList = restTableList;
        this.mContext = mContext;
        databaseAdapter = new DatabaseAdapter();
    }

    @NonNull
    @Override
    public restTableAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recycler_table_items, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull restTableAdapter.ViewHolder holder, int position) {

        restTableEntity current = restTableList.get(position);
        onBind = true;
        holder.table_name.setText(current.tableName);
        setTableStatus(holder, Integer.parseInt(current.restaurantTableID.toString()));
        onBind = false;
    }

    private void setTableStatus(restTableAdapter.ViewHolder holder, int tableId) {
        saveTransEntity archived = databaseAdapter.getSavedTransactions(tableId);

        if (archived != null) {
            if (archived.getPaidAmount() > 0) {
                holder.table_name.setBackground(ContextCompat.getDrawable(mContext, R.drawable.press_focus_view_2));
            } else {
                holder.table_name.setBackground(ContextCompat.getDrawable(mContext, R.drawable.pressed_focused_reserved));
            }
        }

    }

    @Override
    public int getItemCount() {
        return restTableList.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        customTextView table_name;
      //  RippleView table_name_ripple;

        ViewHolder(View itemView) {
            super(itemView);
            table_name = itemView.findViewById(R.id.table_name);
            table_name.setOnClickListener(this);

            //table_name_ripple = itemView.findViewById(R.id.table_name_ripple);
            // itemView.setOnClickListener(this);
            //table_name_ripple.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemTableClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public restTableEntity getItem(int position) {
        return restTableList.get(position);
    }

    // allows clicks events to be caught
    public void setClickListener(restTableAdapter.ItemTableClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemTableClickListener {
        void onItemTableClick(View view, int position);
    }

}
