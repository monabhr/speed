package com.arpa.speed.modules.archive;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arpa.speed.R;
import com.arpa.speed.boxDatabase.DatabaseAdapter;
import com.arpa.speed.boxDatabase.saveTransEntity;
import com.arpa.speed.boxDatabase.tableOrderEntity;
import com.arpa.speed.customViews.customImageView;
import com.arpa.speed.customViews.customTextView;
import com.arpa.speed.models.order.invoiceOrderModel;
import com.arpa.speed.utils.publicUtils;

import java.util.List;

public class archiveAdapter extends RecyclerView.Adapter<archiveAdapter.ViewHolder> {

    List<tableOrderEntity> orderList;
    Context mContext;
    private LayoutInflater mInflater;
    private archiveAdapter.archiveItemClickListener mClickListener;
    DatabaseAdapter dbAdapter = new DatabaseAdapter();

    public archiveAdapter(Context mContext, List<tableOrderEntity> orderList) {
        this.orderList = orderList;
        this.mInflater = LayoutInflater.from(mContext);
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public archiveAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.recycler_archive_items, viewGroup, false);
        return new archiveAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull archiveAdapter.ViewHolder holder, int position) {
        String tableName = dbAdapter.getTablesName(Long.parseLong(orderList.get(position).getTableID() + ""));
        holder.archive_table.setText(tableName);
        holder.archive_row.setText(position + 1 + "");

        if (position % 2 == 1) {
            holder.itemView.setBackgroundColor(Color.parseColor("#FFFFFF"));
        } else {
            holder.itemView.setBackgroundColor(Color.parseColor("#e6e4e4"));
        }

    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        customTextView archive_table, archive_row;
        customImageView archive_send,archive_delete;

        ViewHolder(View itemView) {
            super(itemView);
            archive_table = itemView.findViewById(R.id.archive_table);
            archive_row = itemView.findViewById(R.id.archive_row);
            archive_send = itemView.findViewById(R.id.archive_send);
            archive_delete = itemView.findViewById(R.id.archive_delete);

            archive_table.setOnClickListener(this);
            archive_row.setOnClickListener(this);
            archive_send.setOnClickListener(this);
            archive_delete.setOnClickListener(this);



        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null)
                mClickListener.onArchiveItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public tableOrderEntity getItem(int position) {
        return orderList.get(position);
    }

    // allows clicks events to be caught
    public void setClickListener(archiveAdapter.archiveItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface archiveItemClickListener {
        void onArchiveItemClick(View view, int position);
    }
}
