package com.arpa.speed.modules.home;

import android.view.Menu;
import android.view.MenuItem;

import com.arpa.speed.boxDatabase.saveTransEntity;
import com.arpa.speed.boxDatabase.tableOrderEntity;
import com.arpa.speed.models.login.loginDbModel;
import com.arpa.speed.modules.login.dbModel.userModel;
import com.arpa.speed.modules.syncData.dataSyncModel;

import java.util.List;

import io.reactivex.disposables.Disposable;

public class homePresenter implements homeContracts.home_Presenter {

    homeContracts.home_View view;
    homeContracts.home_Model model = new homeModel();
    homeContracts.data_sync_Model sync_ = new dataSyncModel();

    public homePresenter(homeContracts.home_View home_view) {
        this.view = home_view;
        model.onAttachPresenter(this);
        sync_.onAttachPresenter(this);
    }

    @Override
    public void doOnbackPressed() {
        view.doOnbackPressed();
    }

    @Override
    public void onRightSidebarBtnClick() {
        view.onRightSidebarBtnClick();
    }


    @Override
    public void onCreateMenuToolbar(Menu menu) {
        view.onCreateMenuToolbar(menu);
    }

    @Override
    public void onSelectMenuToolbar(MenuItem item) {
        view.onSelectMenuToolbar(item);
    }

    @Override
    public void onShowSyncDialog() {
        view.onShowSyncDialog();
    }

    @Override
    public void onSync(userModel userModel) {
        sync_.onSync(userModel);
    }

    @Override
    public void onSyncFailed(String message) {
        view.onSyncFailed(message);
    }

    @Override
    public void onSyncSucceed(loginDbModel model) {
        view.onSyncSucceed(model);
    }

    @Override
    public void onShowProgress(int count, String title) {
        view.onShowProgress(count, title);
    }

    @Override
    public void onHideProgress() {
        view.onHideProgress();
    }

    @Override
    public void onDispose(Disposable d) {
        view.onDispose(d);
    }

    @Override
    public void onCatch(String position, String cause) {
        view.onCatch(position, cause);
    }

    @Override
    public void onFinishSyncDialog(String operationType, String password) {
        view.onFinishSyncDialog(operationType, password);
    }

    @Override
    public void onNavigationItemSelect(MenuItem item) {
        view.onNavigationItemSelect(item);
    }

    @Override
    public void onLoadArchiveCount() {
        model.onLoadArchiveCount();
    }

    @Override
    public void onArchiveCountSucceed(List<tableOrderEntity> unsentList) {
        view.onArchiveCountSucceed(unsentList);
    }

    @Override
    public void onArchiveCountFailed(String message) {
        view.onArchiveCountFailed(message);
    }

    @Override
    public void onShowArchiveDialog() {
        view.onShowArchiveDialog();
    }
}
