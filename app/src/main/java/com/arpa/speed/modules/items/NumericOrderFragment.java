package com.arpa.speed.modules.items;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.arpa.speed.R;
import com.arpa.speed.customIconView.titleValueTextView;
import com.arpa.speed.customViews.customButton;
import com.arpa.speed.customViews.customTextView;
import com.arpa.speed.models.order.invoiceOrderModel;
import com.arpa.speed.utils.Constants;
import com.arpa.speed.utils.publicUtils;

public class NumericOrderFragment extends DialogFragment implements OnClickListener {

    private titleValueTextView order_price, order_name;
    private customTextView header_text;
    private NumberPicker numberPicker;
    private customButton order_yes, order_no;
    private long itemID;

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.order_yes) {
            counterDialogListener listener = (counterDialogListener) getActivity();
            listener.onFinishCountDialog(Constants.edit_order_row, numberPicker.getValue() + "", itemID);
        }
        if (v.getId() == R.id.order_no) {
            counterDialogListener listener = (counterDialogListener) getActivity();
            listener.onFinishCountDialog(Constants.delete_order_row, numberPicker.getValue()+ "", getArguments().getLong("itemId"));

        }
        dismiss();
    }


    // 1. Defines the listener interface with a method passing back data result.
    public interface counterDialogListener {
        void onFinishCountDialog(String operationType, String inputText, long itemID);
    }

    public NumericOrderFragment() {
    }

    public static NumericOrderFragment newInstance(String title, invoiceOrderModel orderDetails) {
        NumericOrderFragment frag = new NumericOrderFragment();
        Bundle args = new Bundle();

        args.putString("title", title);
        args.putString("name", orderDetails.itemName);
        args.putLong("itemId", orderDetails.itemID);
        args.putString("count", orderDetails.itemCount + "");
        args.putString("price", publicUtils.commaStringDouble(orderDetails.salePrice));

        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.custom_order_dialog, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Get field from view
        order_yes = view.findViewById(R.id.order_yes);
        order_no = view.findViewById(R.id.order_no);

        numberPicker = view.findViewById(R.id.order_count);
        numberPicker.setMaxValue(100);
        numberPicker.setMinValue(1);

        // Fetch arguments from bundle and set title

        numberPicker.setValue(Integer.parseInt(getArguments().getString("count")));

        itemID = getArguments().getLong("itemId");
        String title = getArguments().getString("title", "Enter Name");
        getDialog().setTitle(title);


        header_text = view.findViewById(R.id.order_name);
        //order_name = view.findViewById(R.id.order_name);
        String name = getArguments().getString("name", "0");
        header_text.setText(name);

        order_price = view.findViewById(R.id.order_price);
        String price = getArguments().getString("price", "0");
        order_price.setValueText(price);

        // 2. Setup a callback when the "Done" button is pressed on keyboard
        order_yes.setOnClickListener(this);
        order_no.setOnClickListener(this);

        // Show soft keyboard automatically and request focus to field
        // numberPicker.requestFocus();

        /*getDialog().*/
     /*   getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);*/
    }

/*    @Override
    public boolean onEditorAction(titleValueTextView v, int actionId, KeyEvent event) {
        if (EditorInfo.IME_ACTION_DONE == actionId) {
            // Return input text back to activity through the implemented listener
            EditNameDialogListener listener = (EditNameDialogListener) getActivity();
            listener.onFinishEditDialog(numberPicker.getValue()+ "");
            // Close the dialog and return back to the parent activity
            dismiss();
            return true;
        }
        return false;
    }*/
}
