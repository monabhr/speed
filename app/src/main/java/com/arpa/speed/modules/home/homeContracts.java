package com.arpa.speed.modules.home;

import android.view.Menu;
import android.view.MenuItem;

import com.arpa.speed.boxDatabase.saveTransEntity;
import com.arpa.speed.boxDatabase.tableOrderEntity;
import com.arpa.speed.models.login.loginDbModel;
import com.arpa.speed.modules.login.dbModel.userModel;
import com.arpa.speed.modules.login.loginPresenter;

import java.util.List;

import io.reactivex.disposables.Disposable;

public interface homeContracts {

    interface home_View {

        void doOnbackPressed();

        void onRightSidebarBtnClick();

        void onCreateMenuToolbar(Menu menu);

        void onSelectMenuToolbar(MenuItem item);

        void onShowSyncDialog();

        void onSyncFailed(String message);

        void onSyncSucceed(loginDbModel model);

        void onShowProgress(int count, String title);

        void onHideProgress();

        void onDispose(Disposable d);

        void onCatch(String position, String cause);

        void onFinishSyncDialog(String operationType, String password);

        void onNavigationItemSelect(MenuItem item);

        void onArchiveCountSucceed(List<tableOrderEntity> unsentList);

        void onArchiveCountFailed(String message);

        void onShowArchiveDialog();


    }

    interface home_Presenter {
        void doOnbackPressed();

        void onRightSidebarBtnClick();

        void onCreateMenuToolbar(Menu menu);

        void onSelectMenuToolbar(MenuItem item);

        void onShowSyncDialog();

        void onSync(userModel userModel);

        void onSyncFailed(String message);

        void onSyncSucceed(loginDbModel model);

        void onShowProgress(int count, String title);

        void onHideProgress();

        void onDispose(Disposable d);

        void onCatch(String position, String cause);

        void onFinishSyncDialog(String operationType, String password);

        void onNavigationItemSelect(MenuItem item);

        void onLoadArchiveCount();

        void onArchiveCountSucceed(List<tableOrderEntity> unsentList);

        void onArchiveCountFailed(String message);

        void onShowArchiveDialog();


    }

    interface home_Model {

        void onAttachPresenter(homePresenter homePresenter);
        void onLoadArchiveCount();

    }

    interface data_sync_Model{

        void onAttachPresenter(homePresenter homePresenter);

        void onSync(userModel userModel);

    }

}
