package com.arpa.speed.modules.login;

import android.os.Handler;
import android.os.Looper;

import com.arpa.speed.R;
import com.arpa.speed.boxDatabase.DatabaseAdapter;
import com.arpa.speed.boxDatabase.businessEntity;
import com.arpa.speed.boxDatabase.itemCatsEntity;
import com.arpa.speed.boxDatabase.itemsEntity;
import com.arpa.speed.boxDatabase.restTableCat;
import com.arpa.speed.boxDatabase.restTableEntity;
import com.arpa.speed.boxDatabase.saveTransEntity;
import com.arpa.speed.boxDatabase.savedTLineEntity;
import com.arpa.speed.models.business.businessDbModel;
import com.arpa.speed.models.itemCats.itemCatsDbModel;
import com.arpa.speed.models.items.itemsDbModel;
import com.arpa.speed.models.login.Data;
import com.arpa.speed.models.login.loginDbModel;
import com.arpa.speed.models.restTable.restTableDbModel;
import com.arpa.speed.models.restTableCat.Datum;
import com.arpa.speed.models.restTableCat.restTableCatDbModel;
import com.arpa.speed.models.savedTrans.SavedTransactionsModel;
import com.arpa.speed.models.savedTrans.Transline;
import com.arpa.speed.models.userDefaults.userDefaultsDbModel;
import com.arpa.speed.modules.login.dbModel.userModel;
import com.arpa.speed.network.webInterface;
import com.arpa.speed.utils.BaseApplication;
import com.arpa.speed.utils.Constants;
import com.arpa.speed.utils.RxRetoGenerator;
import com.arpa.speed.utils.hawkHandler;
import com.orhanobut.logger.Logger;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.arpa.speed.utils.Constants.connection_failed;
import static com.arpa.speed.utils.Constants.failed_to_connect_to;
import static com.google.android.gms.common.util.CollectionUtils.mutableListOf;

public class loginModel implements loginContracts.Model {
    private loginContracts.Presenter presenter;
    Disposable disposable;
    DatabaseAdapter dbAdapter = new DatabaseAdapter();
    private Data userData;
    loginDbModel modell;
    Handler handler = new Handler(Looper.myLooper());

    @Override
    public void onAttachPresenter(loginContracts.Presenter presenter) {
        this.presenter = presenter;
    }

    /*  @Override
      public void onLoginClick(userModel userModel) {

          try {

              webInterface webInterface = RxRetoGenerator.createPingService(com.arpa.speed.network.webInterface.class, userModel.getServerAddress());

              presenter.onShowProgress(1, BaseApplication.getApp().getString(R.string.userLoginInfo));

              Observable<loginDbModel> loginRequest = webInterface.getPingServerToken(userModel.getUsername(), userModel.getPassword());

              loginRequest
                      .flatMap(response -> {
                          try {
                              if (response.getData() != null) {
                                  modell = response;
                                  userData = response.getData();
                                  dbAdapter.clearDatabase();

                                  saveLoginData(userModel.getServerAddress());
                                  return webInterface.getRestTableCategories(userData.getUserId());
                              } else {
                                  handler.post(
                                          () -> {
                                              presenter.onLoginFailed(response.getError());
                                              presenter.onHideProgress();
                                          }
                                  );


                                  return Observable.error(new Throwable());
                              }
                          } catch (Throwable t) {
                              return Observable.error(t);
                          }
                      })
                      .flatMap(response -> {
                          handler.post(
                                  () -> presenter.onShowProgress(response.getData().size(), BaseApplication.getApp().getString(R.string.restTableCats))
                          );

                          saveRestTableCat(response);
                          return webInterface.getRestTable(userData.getUserId());
                      })
                      .flatMap(response -> {
                          //presenter.onShowProgress(response.getData().size(),"RestTable");
                          handler.post(
                                  () -> presenter.onShowProgress(response.getData().size(), BaseApplication.getApp().getString(R.string.restTables))
                          );

                          saveRestTable(response);
                          return webInterface.getDefaults(userData.getUserId());
                      })
                      .flatMap(response -> {
                          handler.post(
                                  () -> presenter.onShowProgress(response.getData().size(), BaseApplication.getApp().getString(R.string.userDefaults))
                          );
                          saveDefaults(response);
                          return webInterface.getBusiness(userData.getUserId());
                      })

                      .flatMap(response -> {
                          handler.post(
                                  () -> presenter.onShowProgress(response.getData().size(), BaseApplication.getApp().getString(R.string.business))
                          );
                          saveBusiness(response);
                          return webInterface.getItems(userData.getUserId());
                      })
                      .flatMap(response -> {
                          //presenter.onShowProgress(response2.getData().size() , "Items");
                          handler.post(
                                  () -> presenter.onShowProgress(response.getData().size(), BaseApplication.getApp().getString(R.string.items))
                          );
                          saveItems(response);
                          return webInterface.getItemCats(userData.getUserId());
                      })
                      .flatMap(response -> {
                          handler.post(
                                  () -> presenter.onShowProgress(response.getData().size(), BaseApplication.getApp().getString(R.string.itemCats))
                          );
                          saveItemCats(response);

                          return webInterface.getTransactions(userData.getUserId(), 0);
                          //return webInterface.getItemAttachment(userData.getUserId());
                      })
                      *//* .flatMap(response -> {
     *//**//* handler.post(
                                () -> presenter.onShowProgress(response.getData().size(), BaseApplication.getApp().getString(R.string.transactions))
                        );*//**//*
                        //saveItemAttachment(response);
                        return webInterface.getTransactions(userData.getUserId());
                    })*//*
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .unsubscribeOn(Schedulers.io())
                    .subscribe(new Observer<SavedTransactionsModel>() {

                        @Override
                        public void onSubscribe(Disposable d) {
                            Logger.d("onSubscribe");
                            disposable = d;
                            presenter.onDispose(disposable);
                        }

                        @Override
                        public void onNext(SavedTransactionsModel result) {

                            handler.post(
                                    () -> presenter.onShowProgress(result.getData().size(), BaseApplication.getApp().getString(R.string.transactions))
                            );

                            saveTransModel(result);
                            // saveItemAttachments(itemAttachDbModel);
                            Logger.d("onNext");
                            presenter.onLoginSuccess(modell);
                            //presenter.onHideProgress();
                        }

                        @Override
                        public void onError(Throwable e) {
                            presenter.onHideProgress();

                            if(e.getLocalizedMessage() != null) {
                                if (e.getLocalizedMessage().toLowerCase().contains(failed_to_connect_to)) {
                                    presenter.onLoginFailed(connection_failed);
                                    Logger.e("login failed to : " + e.getLocalizedMessage());
                                } else {
                                    presenter.onLoginFailed(e.getLocalizedMessage());
                                    Logger.e("login failed to : " + e.getLocalizedMessage());
                                }
                            }
                        }

                        @Override
                        public void onComplete() {
                            presenter.onHideProgress();
                        }
                    })
            ;


        } catch (Exception ex) {
            presenter.onCatch("getLogin", ex.getMessage());
        }


    }
*/
    private void saveTransModel(SavedTransactionsModel response) {

        try {
            if (response.getData() != null) {
                List<saveTransEntity> mutableDataList = mutableListOf();

                for (com.arpa.speed.models.savedTrans.Datum x : response.getData()) {
                    saveTransEntity rst = new saveTransEntity();

                    rst.businessID = x.getBusinessID();
                    rst.businessName = x.getBusinessName();
                    rst.docAliasID = x.getDocAliasID();
                    rst.totalSum = x.getTotalSum();
                    rst.transDate = x.getTransDate();
                    rst.transDiscountAmount = x.getTransDiscountAmount();
                    rst.transDiscountPercent = x.getTransDiscountPercent();
                    rst.transNumber = x.getTransNumber();
                    rst.creatorUserID = x.getCreatorUserID();

                    for (Transline t : x.getTranslines()) {

                        savedTLineEntity tr = new savedTLineEntity();
                        tr.amount = t.getAmount();
                        tr.discountAmount = t.getDiscountAmount();
                        tr.discountPercent = t.getDiscountPercent();
                        tr.itemName = t.getItemName();
                        tr.nQty = t.getNQty();
                        tr.price = t.getPrice();
                        tr.taxAmount = t.getTaxAmount();
                        tr.tollAmount = t.getTollAmount();

                        rst.translines.add(tr);
                    }

                    mutableDataList.add(rst);
                }

                dbAdapter.insertSavedTransactions(mutableDataList);

            } else if (response.getError() != null) {
                Logger.e("saveItemCats error : ", response.getError());
            }

        } catch (Exception ex) {
            Logger.e("saveItemCats error : ", ex.getMessage());

        }

    }

   /* private void saveItemCats(itemCatsDbModel response) {
        try {
            if (response.getData() != null) {
                List<itemCatsEntity> mutableDataList = mutableListOf();

                for (com.arpa.speed.models.itemCats.Datum x : response.getData()) {
                    itemCatsEntity rst = new itemCatsEntity();

                    rst.itemCategoryID = Long.parseLong(x.getItemCategoryID().toString());
                    rst.itemCategoryCode = x.getItemCategoryCode();
                    rst.itemCategoryName = x.getItemCategoryName();

                    mutableDataList.add(rst);
                }

                dbAdapter.insertItemCats(mutableDataList);

            } else if (response.getError() != null) {
                Logger.e("saveItemCats error : ", response.getError());
            }

        } catch (Exception ex) {
            Logger.e("saveItemCats error : ", ex.getMessage());

        }
    }

    private void saveItems(itemsDbModel response) {
        try {
            if (response.getData() != null) {
                List<itemsEntity> mutableDataList = mutableListOf();

                for (com.arpa.speed.models.items.Datum x : response.getData()) {

                    itemsEntity rst = new itemsEntity();
                    rst.itemID = Long.parseLong(x.getItemID().toString());
                    rst.itemCode = x.getItemCode();
                    rst.itemName = x.getItemName();
                    rst.itemCategoryID = x.getItemCategoryID();
                    rst.inverseUnitsRatio = x.getInverseUnitsRatio();
                    rst.salePrice = x.getSalePrice();
                    rst.unitsRatio = x.getUnitsRatio();

                    mutableDataList.add(rst);
                }

                dbAdapter.insertItems(mutableDataList);

            } else if (response.getError() != null) {
                Logger.e("saveItems error : ", response.getError());
            }

        } catch (Exception ex) {
            Logger.e("saveItems error : ", ex.getMessage());

        }
    }

    private void saveBusiness(businessDbModel response) {
        try {
            if (response.getData() != null) {
                List<businessEntity> mutableDataList = mutableListOf();

                for (com.arpa.speed.models.business.Datum x : response.getData()) {

                    businessEntity rst = new businessEntity();
                    rst.businessID = Long.parseLong(x.getBusinessID().toString());
                    rst.businessCode = x.getBusinessCode();
                    rst.businessName = x.getBusinessName();
                    rst.address = x.getAddress();
                    rst.mobile = x.getMobile();
                    rst.phoneNo = x.getPhoneNo();
                    rst.lat = x.getLat();
                    rst._long = x.getLong();
                    rst.inActive = x.getInActive();
                    rst.geoRegionID = x.getGeoRegionID();

                    mutableDataList.add(rst);

                }

                dbAdapter.insertBusiness(mutableDataList);

            } else if (response.getError() != null) {
                Logger.e("saveBusiness error : ", response.getError());
            }

        } catch (Exception ex) {
            Logger.e("saveBusiness error : ", ex.getMessage());

        }
    }

    private void saveRestTable(restTableDbModel response) {
        try {
            if (response.getData() != null) {
                List<restTableEntity> mutableDataList = mutableListOf();

                for (com.arpa.speed.models.restTable.Datum x : response.getData()) {

                    restTableEntity rst = new restTableEntity();
                    rst.restaurantTableID = Long.parseLong(x.getRestaurantTableID().toString());
                    rst.tableCode = x.getTableCode();
                    rst.tableName = x.getTableName();
                    rst.tableNumber = x.getTableNumber();
                    rst.restaurantTableCategoryID = x.getRestaurantTableCategoryID();

                    mutableDataList.add(rst);

                }

                dbAdapter.insertRestTables(mutableDataList);

            } else if (response.getError() != null) {
                Logger.e("saveRestTable error : ", response.getError());
            }

        } catch (Exception ex) {
            Logger.e("saveRestTable error : ", ex.getMessage());

        }
    }

    private void saveRestTableCat(restTableCatDbModel response) {
        try {
            if (response.getData() != null) {
                List<restTableCat> mutableDataList = mutableListOf();

                for (Datum x : response.getData()) {

                    restTableCat rst = new restTableCat();
                    rst.restaurantTableCategoryID = Long.parseLong(x.getRestaurantTableCategoryID().toString());
                    rst.restaurantTableCategoryCode = x.getRestaurantTableCategoryCode();
                    rst.restaurantTableCategoryName = x.getRestaurantTableCategoryName();
                    rst.descriptions = x.getDescriptions();

                    mutableDataList.add(rst);
                }

                dbAdapter.insertRestTableCategories(mutableDataList);
            } else if (response.getError() != null) {
                Logger.e("saveRestTableCat error : ", response.getError());
            }

        } catch (Exception ex) {
            Logger.e("saveRestTableCat error : ", ex.getMessage());

        }

    }

    private void saveDefaults(userDefaultsDbModel response) {
        try {
            if (response.getData() != null) {
                com.arpa.speed.models.userDefaults.Datum defs = response.getData().get(0);

                hawkHandler.saveData(Constants.user_defaults, defs);

                //hawkHandler.saveData("defaultTransStateID", defs.getDefaultTransStateID());
                //hawkHandler.saveData("defStockID", defs.getDefStockID());
                // hawkHandler.saveData("defCustomerID", defs.getDefCustomerID());
            } else {
                Logger.e("saveDefaults error : ", response.getError());
            }
        } catch (Exception ex) {
            Logger.e("saveDefaults error : ", ex.getMessage());
        }
    }

    private void saveLoginData(String serverAdr) {

        com.arpa.speed.utils.hawkHandler handler = com.arpa.speed.utils.hawkHandler
                .getInstance();

        userModel model = new userModel();
        model.setUserID(userData.getUserId());
        model.setUsername(userData.getUsername());
        model.setServerAddress(serverAdr);
        model.setLoginStatus(true);

        handler.saveData(Constants.user_model, model);

    }*/
}
