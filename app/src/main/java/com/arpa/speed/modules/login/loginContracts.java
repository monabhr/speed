package com.arpa.speed.modules.login;

import com.arpa.speed.models.login.loginDbModel;
import com.arpa.speed.modules.login.dbModel.userModel;

import io.reactivex.disposables.Disposable;

public interface loginContracts {

    interface View {
        void onCheckInfo(String username, String password, String server);

        /* void onLoginFailed(String message);

         void onLoginSuccess(loginDbModel dbModel);
 */
        void onDispose(Disposable d);

        void onCatch(String position, String cause);

        void onShowProgress(int count, String title);

        void onHideProgress();

        void onSyncFailed(String message);

        void onSyncSucceed(loginDbModel model);

    }

    interface Presenter {

        void onCheckInfo(String username, String password, String server);

        /*void onLoginClick(userModel userModel);

       void onLoginFailed(String message);

        void onLoginSuccess(loginDbModel dbModel);
*/
        void onDispose(Disposable d);

        void onCatch(String position, String cause);

        void onShowProgress(int count, String title);

        void onHideProgress();

        void onSync(userModel userModel);

        void onSyncFailed(String message);

        void onSyncSucceed(loginDbModel model);
    }

    interface Model {
        void onAttachPresenter(Presenter presenter);

        //void onLoginClick(userModel userModel);

    }

    interface data_sync_mode {

        void onAttachLoginPresenter(Presenter presenter);

        void onSyncClicked(userModel userModel);
    }


}
