package com.arpa.speed.modules.archive;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arpa.speed.R;
import com.arpa.speed.boxDatabase.DatabaseAdapter;
import com.arpa.speed.boxDatabase.restTableEntity;
import com.arpa.speed.boxDatabase.saveTransEntity;
import com.arpa.speed.boxDatabase.tableOrderEntity;
import com.arpa.speed.modules.items.ItemsActivity;
import com.arpa.speed.utils.BaseApplication;
import com.arpa.speed.utils.Constants;
import com.arpa.speed.utils.publicUtils;
import com.evrencoskun.tableview.TableView;
import com.google.common.collect.Table;
import com.orhanobut.logger.Logger;

import java.io.Serializable;
import java.util.List;

public class archiveDialogfragment extends DialogFragment implements View.OnClickListener,
        archiveAdapter.archiveItemClickListener {

    RecyclerView archive_recyclerView;
    restTableEntity selectedTable;
    archiveAdapter archiveAdapter;
    DatabaseAdapter dbAdapter = new DatabaseAdapter();

   /* private List<RowHeader> mRowHeaderList;
    private List<ColumnHeader> mColumnHeaderList;
    private List<List<Table.Cell>> mCellList;
*/

    public archiveDialogfragment() {
    }

    public static archiveDialogfragment newInstance(List<tableOrderEntity> ulist) {
        archiveDialogfragment frag = new archiveDialogfragment();
        Bundle args = new Bundle();
        args.putSerializable("unsentlist", (Serializable) ulist);
        frag.setArguments(args);
        return frag;
    }


    @Override
    public void onClick(View v) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.custom_archive_dialog, container);
    }


    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        archive_recyclerView = view.findViewById(R.id.archive_recyclerView);
        List<tableOrderEntity> archiveList = (List<tableOrderEntity>) getArguments().getSerializable("unsentlist");
        if (archiveList != null) {
            archive_recyclerView.setLayoutManager(new LinearLayoutManager(BaseApplication.getApp()));
            archiveAdapter = new archiveAdapter(getActivity(), archiveList);
            archiveAdapter.setClickListener(this);
            archive_recyclerView.setAdapter(archiveAdapter);
        }


      /*  TableView tableView = new TableView(getContext());

        // Create our custom TableView Adapter
        tarchiveAdapter adaptere = new tarchiveAdapter(getContext());

        // Set this adapter to the our TableView
        tableView.setAdapter(adaptere);

        // Let's set datas of the TableView on the Adapter
        adaptere.setAllItems(mColumnHeaderList, mRowHeaderList, mCellList);*/
    }


    @Override
    public void onArchiveItemClick(View view, int position) {

        Long tableId = Long.parseLong(archiveAdapter.getItem(position).getTableID() + "");
        selectedTable = dbAdapter.getTablesById(tableId);

        int viewId = view.getId();

        switch (viewId) {

            case R.id.archive_table:
            case R.id.archive_row: {
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.item_Intent_args, selectedTable);

                Intent itemsPageIntent = new Intent(getActivity(), ItemsActivity.class);
                itemsPageIntent.putExtras(bundle);
                startActivity(itemsPageIntent);
            }
            break;
            case R.id.archive_send:
               // sendToserver();
                publicUtils.toast("sendToserver");
                break;

            case R.id.archive_delete:
               // removeFromArchive();
                publicUtils.toast("removeFromArchive");
                break;

        }


    }
}
