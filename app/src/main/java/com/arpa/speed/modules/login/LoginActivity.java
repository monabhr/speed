package com.arpa.speed.modules.login;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.WindowManager;

import com.arpa.speed.R;
import com.arpa.speed.boxDatabase.DatabaseAdapter;
import com.arpa.speed.boxDatabase.businessEntity;
import com.arpa.speed.boxDatabase.itemsEntity;
import com.arpa.speed.customIconView.iconicEditText;
import com.arpa.speed.customViews.customButton;
import com.arpa.speed.models.login.Data;
import com.arpa.speed.models.login.loginDbModel;
import com.arpa.speed.modules.home.HomeActivity;
import com.arpa.speed.modules.login.dbModel.userModel;
import com.arpa.speed.utils.BaseActivity;
import com.arpa.speed.utils.Constants;
import com.arpa.speed.utils.dialogs;
import com.arpa.speed.utils.hawkHandler;
import com.arpa.speed.utils.publicUtils;
import com.orhanobut.logger.Logger;

import java.util.List;

import io.reactivex.disposables.Disposable;

public class LoginActivity extends BaseActivity
        implements loginContracts.View {

    loginContracts.Presenter presenter = new loginPresenter(this);

    Disposable disposable;
    customButton loginButton;
    dialogs dialogUtil = new dialogs();
    hawkHandler handler;
    iconicEditText serverIconicEditText, userIconicEditText, passwordIconicEditText;
    DatabaseAdapter dbAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dbAdapter = new DatabaseAdapter();
        checkLoginStatus();
        setContentView(R.layout.activity_login);
        bindViews();

    }

    private void checkLoginStatus() {
        handler = com.arpa.speed.utils.hawkHandler
                .getInstance();

        userModel model = handler.getData(Constants.user_model);

        if (model != null) {
            if (model.getLoginStatus()) {
                HomeActivity.Compan.show(this);
                return;
            }
        }

    }

    private void bindViews() {
        userIconicEditText = findViewById(R.id.userIconicEditText);
        passwordIconicEditText = findViewById(R.id.passwordIconicEditText);
        serverIconicEditText = findViewById(R.id.serverIconicEditText);
        loginButton = findViewById(R.id.loginButton);

        loginButton.setOnClickListener(V -> {
            dialog.show();
            presenter.onCheckInfo(userIconicEditText.getText()
                    , passwordIconicEditText.getText(),
                    serverIconicEditText.getText());
        });


    }

    @Override
    public void onCheckInfo(String username, String password, String server) {

        userModel userModel = new userModel();

        if (username != null) {
            userModel.setUsername(publicUtils.arabicToDecimal(username));
        } else {
            publicUtils.toast(getString(R.string.enter_user_name));
        }

        if (server != null) {
            userModel.setServerAddress(publicUtils.arabicToDecimal(server));
        } else {
            publicUtils.toast(getString(R.string.enter_valid_server_address));
        }

        userModel.setPassword(publicUtils.arabicToDecimal(password));

        if (publicUtils.isConnectedToInternet()) {
            //presenter.onLoginClick(userModel);
            presenter.onSync(userModel);
        } else {
            publicUtils.toast(getString(R.string.check_internet_connection));
            presenter.onHideProgress();

            Logger.d("no internet");
        }


    }

    @Override
    public void onSyncSucceed(loginDbModel model) {
        doAfterLogin(model);
    }

    @Override
    public void onSyncFailed(String message) {
        Logger.e("onLoginFailed ", message);
        publicUtils.toast(message);
    }

    private void doAfterLogin(loginDbModel dbModel) {
        Data model = dbModel.getData();
        if (dbModel.getData() != null) {
            Intent homeIntent = new Intent(this, HomeActivity.class);
            homeIntent.putExtra("userId", model.getUserId());
            homeIntent.putExtra("userName", model.getUsername());
            startActivity(homeIntent);
        }
    }

    @Override
    public void onDispose(Disposable d) {
        disposable = d;
    }

    @Override
    public void onCatch(String position, String cause) {
        dialog.hide();
        Logger.e(position, cause);
    }

    @Override
    public void onShowProgress(int count, String title) {
        dialog.setMessage(getString(R.string.isLoading) + " " + title);
        dialog.setTitle(title);
        dialog.setMax(count);
        for (int i = 0; i < count; i++) {
            //set the progress
            dialog.setProgress(i + 1);
        }

        dialog.show();
    }

    @Override
    public void onHideProgress() {
        dialogUtil.hideProgressDialog(dialog);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //disposable.dispose();
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        checkPermissions();
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        );
    }

    private void checkPermissions() {
        try {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                }

                if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 3);
                }

                if (checkSelfPermission(Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.INTERNET}, 5);
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    if (checkSelfPermission(Manifest.permission.FOREGROUND_SERVICE) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.FOREGROUND_SERVICE}, 7);
                    }
                }
            }

            //  checkLoginStatus();

        } catch (Exception ex) {
            Logger.e("LoginActivity checkPermissions", ex.getMessage());

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        publicUtils.freeMemory();
        Logger.d("Garbage", "Collecting");
        System.gc();
    }
}
