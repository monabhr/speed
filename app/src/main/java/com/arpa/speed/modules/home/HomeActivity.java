package com.arpa.speed.modules.home;

import android.app.Activity;
import android.content.BroadcastReceiver;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;

import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;


import com.arpa.speed.R;
import com.arpa.speed.boxDatabase.DatabaseAdapter;
import com.arpa.speed.boxDatabase.restTableCat;
import com.arpa.speed.boxDatabase.restTableEntity;
import com.arpa.speed.boxDatabase.saveTransEntity;
import com.arpa.speed.boxDatabase.tableOrderEntity;
import com.arpa.speed.customViews.customTextView;
import com.arpa.speed.models.login.loginDbModel;
import com.arpa.speed.modules.archive.archiveAdapter;
import com.arpa.speed.modules.archive.archiveDialogfragment;
import com.arpa.speed.modules.items.ItemsActivity;
import com.arpa.speed.modules.login.dbModel.userModel;
import com.arpa.speed.modules.restTable.restTableAdapter;
import com.arpa.speed.modules.restTableCat.restTableCatAdapter;
import com.arpa.speed.modules.syncData.syncDataFragment;
import com.arpa.speed.services.checkPaid.checkPaidService;
import com.arpa.speed.utils.BaseActivity;
import com.arpa.speed.utils.BaseApplication;
import com.arpa.speed.utils.Constants;
import com.arpa.speed.utils.CustomTypefaceSpan;
import com.arpa.speed.utils.dialogs;
import com.arpa.speed.utils.hawkHandler;
import com.arpa.speed.utils.publicUtils;
import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;
import com.orhanobut.logger.Logger;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.Disposable;
import kotlin.jvm.internal.Intrinsics;

public class HomeActivity extends BaseActivity implements
        homeContracts.home_View
        , restTableCatAdapter.ItemClickListener
        , restTableAdapter.ItemTableClickListener
        , syncDataFragment.syncDialogListener,
        NavigationView.OnNavigationItemSelectedListener
       //, archiveAdapter.archiveItemClickListener
         {

    private static final String TAG = HomeActivity.class.getSimpleName();

    homeContracts.home_Presenter presenter = new homePresenter(this);

    NavigationView nav_view;
    ActionBar actionBar;
    DrawerLayout home_drawer_layout;
    RecyclerView categoriesRecyclerView, tablesRecyclerView;
    DatabaseAdapter databaseAdapter;
    restTableCatAdapter catAdapter;
    restTableAdapter tableAdapter;
    List<restTableCat> restTableCatList;
    restTableCat selectedTblCat;
    restTableEntity selectedTable;
    List<restTableEntity> restTableList;
    Toolbar home_toolbar;
    boolean menuResult;
    dialogs dialogUtil = new dialogs();
    userModel user_model;
    hawkHandler hawHandler;
    String user_name = "";
    Disposable disposable;
    private ProgressBar progressBar = null;
    int colCount;
    Typeface font;
    customTextView archive_count;
    List<tableOrderEntity> archiveList = new ArrayList<>();
    // A reference to the service used to get location updates.
    public checkPaidService mService = null;

    // Tracks the bound state of the service.
    private boolean mBound = false;
    tableStateBroadcastReceiver tableStateReveiver;
    FloatingActionButton archive_flb;
    archiveAdapter archivAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        doBindService();

        tableStateReveiver = new tableStateBroadcastReceiver();

        LocalBroadcastManager.getInstance(this).registerReceiver(
                tableStateReveiver, new IntentFilter("GPSLocationUpdates"));


        initialize();
        bindViews();
        setToolBar();
        setNavigationViewFont();
        setTableCatsFragment();
        setTablesFragment();


        presenter.onLoadArchiveCount();

    }

    @Override
    protected void onResume() {
        super.onResume();
        tableStateReveiver = new tableStateBroadcastReceiver();
        final IntentFilter intentFilter = new IntentFilter("GPSLocationUpdates");
        LocalBroadcastManager.getInstance(this).registerReceiver(tableStateReveiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (tableStateReveiver != null)
            LocalBroadcastManager.getInstance(this).unregisterReceiver(tableStateReveiver);
        tableStateReveiver = null;
    }



    public class tableStateBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            Boolean status = intent.getBooleanExtra("Status", true);
            //Bundle b = intent.getExtras();
            //Boolean status = b.getBoolean("Status");

            if (status) {
                setTablesFragment();
            }
        }
    }

    private void setNavigationViewFont() {

        Menu m = nav_view.getMenu();

        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);

            if (mi.getTitle() == getString(R.string.setting)) {
                mi.setIcon(new IconDrawable(this, FontAwesomeIcons.fa_cogs)
                        .colorRes(R.color.colorPrimaryLight));
            }

            if (mi.getTitle() == getString(R.string.logout)) {
                mi.setIcon(new IconDrawable(this, FontAwesomeIcons.fa_sign_out)
                        .colorRes(R.color.colorPrimaryLight));
            }

            //the method we have create in activity
            applyFontToMenuItem(mi);

            //for aapplying a font to subMenu ...
           /* SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    subMenuItem.
                            applyFontToMenuItem(subMenuItem);
                }
            }*/


        }
    }

    private void applyFontToMenuItem(MenuItem mi) {

        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font, 25, 0), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    private void setToolBar() {

        setSupportActionBar(home_toolbar);

        actionBar = getSupportActionBar();

        setActionBarTitle();

        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(
                new IconDrawable(this, FontAwesomeIcons.fa_bars)
                        .colorRes(R.color.colorAccent)
                        .actionBarSize());

        actionBar.setHomeButtonEnabled(true);

        // actionBar.setCustomView();

    }

    private void setActionBarTitle() {
        //SpannableString mNewTitle = new SpannableString(getResources().getString(R.string.appName));
        SpannableString mNewTitle = new SpannableString(user_name);
        mNewTitle.setSpan(new CustomTypefaceSpan("", font, 30, 0), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        actionBar.setTitle(mNewTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        presenter.onCreateMenuToolbar(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        presenter.onSelectMenuToolbar(item);
        return menuResult;
    }

    private void initialize() {
        databaseAdapter = new DatabaseAdapter();
        font = BaseApplication.typeFace;
        hawHandler = hawkHandler.getInstance();
        colCount = publicUtils.calculateNoOfColumns(getApplicationContext());
        user_model = hawHandler.getData(Constants.user_model);
        if (user_model != null) {
            user_name = user_model.getUsername();
        }

    }

    private void bindViews() {
        home_drawer_layout = findViewById(R.id.home_drawer_layout);
        categoriesRecyclerView = findViewById(R.id.categoriesRecyclerView);
        tablesRecyclerView = findViewById(R.id.tablesRecyclerView);
        home_toolbar = findViewById(R.id.home_toolbar);
        nav_view = findViewById(R.id.nav_view);
        progressBar = findViewById(R.id.refresh_recycler_progressbar);
        archive_count = findViewById(R.id.archive_count);
        archive_flb = findViewById(R.id.archive_flb);

        nav_view.setNavigationItemSelectedListener(item -> {
            // set item as selected to persist highlight
            presenter.onNavigationItemSelect(item);
            return true;
        });

        archive_flb.setOnClickListener(V -> {
            presenter.onShowArchiveDialog();
        });

    }

    private void setTableCatsFragment() {
        restTableCatList = databaseAdapter.getAllRestTableCategories();

        int numberOfColumns = restTableCatList.size();

        if (numberOfColumns > 0) {

            //int noOfColumns = publicUtils.calculateNoOfColumns(getApplicationContext());

            categoriesRecyclerView.setLayoutManager(new GridLayoutManager(this, this.getResources().getInteger(R.integer.number_of_grid_items)));
            //categoriesRecyclerView.setLayoutManager(new GridLayoutManager(this, colCount));

            catAdapter = new restTableCatAdapter(this, restTableCatList);
            catAdapter.setClickListener(this);
            categoriesRecyclerView.setAdapter(catAdapter);

            // Scroll RecyclerView a little to make later scroll take effect.
            categoriesRecyclerView.scrollToPosition(1);
        }
    }

    @Override
    public void onItemRestClick(View view, int position) {
        // Stop showing the progress bar.
        progressBar.setVisibility(View.VISIBLE);
        selectedTblCat = catAdapter.getItem(position);
        setTablesFragment();
    }

    private void setTablesFragment() {

        progressBar.setVisibility(View.VISIBLE);

        if (restTableCatList.size() > 0) {
            if (selectedTblCat != null) {
                restTableList = databaseAdapter.getTablesByCategory(selectedTblCat.restaurantTableCategoryID);
            } else {
                restTableList = databaseAdapter.getTablesByCategory(restTableCatList.get(0).restaurantTableCategoryID);
            }


            int numberOfColumns = restTableList.size();

            if (numberOfColumns > 0) {
                tablesRecyclerView.setLayoutManager(new GridLayoutManager(this, this.getResources().getInteger(R.integer.number_of_grid_items)));

                tableAdapter = new restTableAdapter(this, restTableList);
                tableAdapter.setClickListener(this);
                tablesRecyclerView.setAdapter(tableAdapter);
            } else {
                publicUtils.toast(String.format("%s %s ", selectedTblCat.restaurantTableCategoryName, getString(R.string.no_data_exists)));
            }
        }
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onItemTableClick(View view, int position) {
        selectedTable = tableAdapter.getItem(position);

        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.item_Intent_args, selectedTable);

        Intent itemsPageIntent = new Intent(this, ItemsActivity.class);
        itemsPageIntent.putExtras(bundle);
        startActivity(itemsPageIntent);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        presenter.doOnbackPressed();
    }

    @Override
    public void doOnbackPressed() {
        AlertDialog.Builder logoutDialog = new AlertDialog.Builder(mContext);
        logoutDialog.setTitle(R.string.logout)
                .setMessage(R.string.do_you_wanna_logout)
                .setPositiveButton(R.string.yes, (dialog, which) -> {
                    dialog.dismiss();
                    exitApp();
                })
                .setNegativeButton(R.string.no, (dialog, which) -> dialog.dismiss())
                .show();

    }

    @Override
    public void onRightSidebarBtnClick() {
        openDrawer();
    }

    @Override
    public void onSyncFailed(String message) {
        publicUtils.toast(message);
    }

    @Override
    public void onSyncSucceed(loginDbModel model) {
        setTablesFragment();
    }

    @Override
    public void onCreateMenuToolbar(Menu menu) {
        getMenuInflater().inflate(R.menu.home_top_menu, menu);
        // Set an icon in the ActionBar
        menu.findItem(R.id.sync_data).setIcon(
                new IconDrawable(this, FontAwesomeIcons.fa_refresh)
                        .colorRes(R.color.colorAccent)
                        .actionBarSize());
    }

    @Override
    public void onSelectMenuToolbar(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sync_data:
                presenter.onShowSyncDialog();
                menuResult = true;
                break;

            case android.R.id.home:
                presenter.onRightSidebarBtnClick();
                break;
            default:
                menuResult = super.onOptionsItemSelected(item);
                break;
        }
    }

    @Override
    public void onShowSyncDialog() {
        FragmentManager fm = getSupportFragmentManager();
        syncDataFragment syncFragment = syncDataFragment.newInstance(user_model);
        syncFragment.show(fm, "");
    }

    @Override
    public void onFinishSyncDialog(String operationType, String password) {
        try {
            if (operationType.equals(Constants.sync_confirm)) {
                user_model.setPassword(publicUtils.arabicToDecimal(password));
                hawHandler.getData(Constants.user_model);
                presenter.onSync(user_model);
            }
        } catch (Exception ex) {
            Logger.e(TAG, ex.getMessage());
        }
    }

    @Override
    public void onNavigationItemSelect(MenuItem item) {
        item.setChecked(true);

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_setting:

                break;

            case R.id.nav_logout:
                presenter.doOnbackPressed();
                break;
        }

        // close drawer when item is tapped
        home_drawer_layout.closeDrawers();

    }

    @Override
    public void onArchiveCountSucceed(List<tableOrderEntity> unsentList) {
        if (unsentList.size() > 0)
            archiveList = unsentList;

        archive_count.setText(unsentList.size() + "");
    }

    @Override
    public void onArchiveCountFailed(String message) {
        publicUtils.toast(message);
    }

    @Override
    public void onShowArchiveDialog() {
        if (archiveList.size() > 0) {
            FragmentManager fm = getSupportFragmentManager();
            archiveDialogfragment archiveFragment = archiveDialogfragment.newInstance(archiveList);
            archiveFragment.show(fm, "");
        }
    }

    @Override
    public void onShowProgress(int count, String title) {
        dialog.setTitle(title);
        dialog.setMax(count);
        for (int i = 0; i < count; i++) {
            //set the progress
            dialog.setProgress(i + 1);
        }

        dialog.show();
    }

//    @Override
//    public void onShowProgress(int count, String title) {
//        FragmentManager fm = getSupportFragmentManager();
//        lottieDialogFragment lottieFragment = lottieDialogFragment.newInstance(title,0,count);
//        lottieFragment.show(fm, "");
//    }

    @Override
    public void onHideProgress() {
        dialogUtil.hideProgressDialog(dialog);
    }

    @Override
    public void onDispose(Disposable d) {
        disposable = d;
    }

    @Override
    public void onCatch(String position, String cause) {
        Logger.e(TAG, String.format("%s   %s", position, cause));
    }

    private void exitApp() {
        hawHandler.clearData(Constants.user_model);

        closeDrawer();
        this.finishAffinity();
        System.exit(0);
    }

    private void openDrawer() {
        home_drawer_layout.openDrawer(GravityCompat.END);
    }

    private void closeDrawer() {
        home_drawer_layout.closeDrawers();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    public static class Compan {

        public Compan() {
        }

        public static final void show(@NotNull Activity packageContext) {
            Intrinsics.checkParameterIsNotNull(packageContext, "packageContext");
            Intent i = new Intent((Context) packageContext, HomeActivity.class);
            packageContext.startActivity(i);
            packageContext.overridePendingTransition(2130771974, 2130771977);
        }
    }

    public void doBindService() {

        Intent intent = new Intent(HomeActivity.this, checkPaidService.class);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            ContextCompat.startForegroundService(getBaseContext(), intent);
        } else {
            startService(intent);
        }


    }

    private void onStopService() {
        Intent intent = new Intent(HomeActivity.this, checkPaidService.class);
        stopService(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        onStopService();
    }
}
