package com.arpa.speed.modules.itemCats;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arpa.speed.R;
import com.arpa.speed.boxDatabase.itemCatsEntity;
import com.arpa.speed.customViews.customTextView;

import java.util.List;

public class itemCatsAdapter extends RecyclerView.Adapter<itemCatsAdapter.ViewHolder> {

    List<itemCatsEntity> itemCatsList;
    Context mContext;
    private LayoutInflater mInflater;
    private itemCatsAdapter.ItemClickListener mClickListener;

    public itemCatsAdapter(Context mContext, List<itemCatsEntity> itemCatsList) {
        this.mInflater = LayoutInflater.from(mContext);
        this.itemCatsList = itemCatsList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public itemCatsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.recycler_item_cats, viewGroup, false);
        return new itemCatsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull itemCatsAdapter.ViewHolder holder, int position) {
        holder.itemCat_name.setText(itemCatsList.get(position).itemCategoryName);
    }

    @Override
    public int getItemCount() {
        return itemCatsList.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        customTextView itemCat_name;
        //RippleView item_cat_ripple;
        ViewHolder(View itemView) {
            super(itemView);
            itemCat_name = itemView.findViewById(R.id.itemCat_name);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemCatClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public itemCatsEntity getItem(int position) {
        return itemCatsList.get(position);
    }

    // allows clicks events to be caught
    public void setClickListener(itemCatsAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemCatClick(View view, int position);
    }

}
