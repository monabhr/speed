package com.arpa.speed.modules.items;

import android.view.Menu;
import android.view.MenuItem;

import com.arpa.speed.boxDatabase.DatabaseAdapter;
import com.arpa.speed.boxDatabase.TransLineEntity;
import com.arpa.speed.boxDatabase.TransactionEntity;
import com.arpa.speed.boxDatabase.itemsEntity;
import com.arpa.speed.boxDatabase.orderEntity;
import com.arpa.speed.boxDatabase.restTableEntity;
import com.arpa.speed.models.order.invoiceOrderModel;
import com.arpa.speed.models.transaction.ObjectToSend;
import com.arpa.speed.models.transaction.transactionModel;
import com.arpa.speed.models.transaction.translineModel;
import com.arpa.speed.modules.order.dbModel.Data;
import com.arpa.speed.modules.order.dbModel.Model;

import java.util.List;

import io.reactivex.disposables.Disposable;

public class itemsPresenter implements itemContract.item_Presenter {

    itemContract.item_View view;
    itemContract.item_model model = new itemsModel();


    public itemsPresenter(itemContract.item_View view) {
        this.view = view;
        model.attachPresenter(this);
    }

    @Override
    public void onShowCountDialog(invoiceOrderModel item) {
        view.onShowCountDialog(item);
    }

    @Override
    public void onSetTotalSum(double sumRowPrice, int sumCount) {
        view.onSetTotalSum(sumRowPrice, sumCount);
    }

    @Override
    public void onAddOrderCount(itemsEntity item) {
        view.onAddOrderCount(item);
    }

    @Override
    public void onCreateMenuToolbar(Menu menu) {
        view.onCreateMenuToolbar(menu);
    }

    @Override
    public void onSelectMenuToolbar(MenuItem item) {
        view.onSelectMenuToolbar(item);
    }

    @Override
    public void onSaveOrderClicked() {
        view.onSaveOrderClicked();
    }

    @Override
    public void onShowProgress() {
        view.onShowProgress();
    }

    @Override
    public void onHideProgress() {
        view.onHideProgress();
    }

    @Override
    public void onSave(ObjectToSend data, String serverAddress) {
        model.onSave(data, serverAddress);
    }

    @Override
    public void onArchive(TransactionEntity transaction, List<TransLineEntity> translines, List<invoiceOrderModel> orders) {
        model.onArchive(transaction, translines, orders);
    }

    @Override
    public void onArchiveSucceed() {
        view.onArchiveSucceed();
    }

    @Override
    public void onArchiveFailed(String message) {
        view.onArchiveFailed(message);
    }

    @Override
    public void onUpdateTableOrderEntity(long transactionId) {
        model.onUpdateTableOrderEntity(transactionId);
    }

    @Override
    public void onSaveOrderSucceed(Model trans) {
        view.onSaveOrderSucceed(trans);
    }

    @Override
    public void onSaveOrderFailed(String message) {
        view.onSaveOrderFailed(message);
    }

    @Override
    public void onDispose(Disposable d) {
        view.onDispose(d);
    }

    @Override
    public void onSaveTransactionResult(Model entity) {
        model.onSaveTransactionResult(entity);
    }

    @Override
    public void onSaveTransactionResultSucceed(transactionModel result) {
        view.onSaveTransactionResultSucceed(result);
    }

   /* @Override
    public void onSaveTransaction(transactionModel entity) {
        model.onSaveTransaction(entity);
    }

    @Override
    public void onSaveTransline(List<translineModel> translineLst, long transactionID) {
        model.onSaveTransline(translineLst, transactionID);
    }*/

    @Override
    public void onSaveOrder(List<invoiceOrderModel> orderList, long transactionID) {
        model.onSaveOrder(orderList, transactionID);
    }

    @Override
    public void onchangeRestTableStatus(restTableEntity entity, int status) {
        model.onchangeRestTableStatus(entity, status);
    }

    @Override
    public void onRemoveOrderRow(invoiceOrderModel order) {
        model.onRemoveOrderRow(order);
    }

    @Override
    public void onRemovesucceed() {
        view.onRemovesucceed();
    }

    @Override
    public void onSaveOrderSucceed() {
        view.onSaveOrderSucceed();
    }

    @Override
    public void onShowSaveArchiveDialog() {
        view.onShowSaveArchiveDialog();
    }

    @Override
    public void OnArchiveOrderClicked() {
        view.OnArchiveOrderClicked();
    }
}
