package com.arpa.speed.modules.items;

import android.view.Menu;
import android.view.MenuItem;

import com.arpa.speed.boxDatabase.TransLineEntity;
import com.arpa.speed.boxDatabase.TransactionEntity;
import com.arpa.speed.boxDatabase.itemsEntity;
import com.arpa.speed.boxDatabase.restTableEntity;
import com.arpa.speed.models.order.invoiceOrderModel;
import com.arpa.speed.models.transaction.ObjectToSend;
import com.arpa.speed.models.transaction.transactionModel;
import com.arpa.speed.modules.order.dbModel.Model;

import java.util.List;

import io.reactivex.disposables.Disposable;

public interface itemContract {

    interface item_View {
        void onShowCountDialog(invoiceOrderModel item);

        void onCreateMenuToolbar(Menu menu);

        void onSetTotalSum(double sumRowPrice, int sumCount);

        void onAddOrderCount(itemsEntity item);

        void onSelectMenuToolbar(MenuItem item);

        void onSaveOrderClicked();

        void onShowProgress();

        void onHideProgress();

        void onSaveOrderSucceed(Model result);

        void onSaveOrderFailed(String message);

        void onDispose(Disposable d);

        void onSaveTransactionResultSucceed(transactionModel result);

        void onRemovesucceed();

        void onSaveOrderSucceed();

        void onShowSaveArchiveDialog();

        void OnArchiveOrderClicked();
        
        void onArchiveSucceed();

        void onArchiveFailed(String message);

    }

    interface item_Presenter {
        // void onShowCountDialog(invoiceOrderModel item);
        void onShowCountDialog(invoiceOrderModel item);

        void onSetTotalSum(double sumRowPrice, int sumCount);

        void onAddOrderCount(itemsEntity item);

        void onCreateMenuToolbar(Menu menu);

        void onSelectMenuToolbar(MenuItem item);

        void onSaveOrderClicked();

        void onShowProgress();

        void onHideProgress();

        void onSave(ObjectToSend objs, String serverAddress);

        //void OnArchive(ObjectToSend objs);

        void onSaveOrderSucceed(Model result);

        void onSaveOrderFailed(String message);

        void onDispose(Disposable d);

        void onSaveTransactionResult(Model entity);

        void onSaveTransactionResultSucceed(transactionModel result);

        // void onSaveTransaction(transactionModel entity);

        //void onSaveTransline(List<translineModel> translineLst,long transactionID);

        void onSaveOrder(List<invoiceOrderModel> orderList, long transactionID);

        void onchangeRestTableStatus(restTableEntity entity, int status);

        void onRemoveOrderRow(invoiceOrderModel order);

        void onRemovesucceed();

        void onSaveOrderSucceed();

        void onShowSaveArchiveDialog();

        void OnArchiveOrderClicked();

        void onArchive(TransactionEntity transaction, List<TransLineEntity> translines, List<invoiceOrderModel> orders);

        void onArchiveSucceed();

        void onArchiveFailed(String message);
        void onUpdateTableOrderEntity(long transactionId);



    }

    interface item_model {

        void attachPresenter(item_Presenter item_presenter);

        void onSave(ObjectToSend objs, String serverAddress);

        void onSaveTransactionResult(Model entity);

        //void onSaveTransaction(transactionModel entity);

        //void onSaveTransline(List<translineModel> translineLst,long transactionID);

        void onSaveOrder(List<invoiceOrderModel> orderList, long transactionID);

        void onchangeRestTableStatus(restTableEntity entity, int status);

        void onRemoveOrderRow(invoiceOrderModel order);

        //void OnArchive(ObjectToSend objs);

        void onArchive(TransactionEntity transaction, List<TransLineEntity> translines, List<invoiceOrderModel> orders);

        void onUpdateTableOrderEntity(long transactionId);


    }

}
