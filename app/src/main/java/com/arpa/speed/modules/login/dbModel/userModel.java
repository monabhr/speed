package com.arpa.speed.modules.login.dbModel;

public class userModel {

    private String username, password, serverAddress;
    private Boolean loginStatus;
    private int userID;

    public userModel() {

    }

    public userModel(String username, String password, String serverAddress,Boolean loginStatus,int userID) {
        this.username = username;
        this.password = password;
        this.serverAddress = serverAddress;
        this.loginStatus = loginStatus;
        this.userID = userID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public Boolean getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(Boolean loginStatus) {
        this.loginStatus = loginStatus;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getServerAddress() {
        return serverAddress;
    }

    public void setServerAddress(String serverAddress) {
        this.serverAddress = serverAddress;
    }

    @Override
    public String toString() {
        return "userModel{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", serverAddress='" + serverAddress + '\'' +
                '}';
    }
}
