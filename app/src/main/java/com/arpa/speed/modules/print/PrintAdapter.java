package com.arpa.speed.modules.print;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.pdf.PdfDocument;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.print.PageRange;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintDocumentInfo;
import android.print.PrintManager;
import android.print.pdf.PrintedPdfDocument;
import android.util.SparseIntArray;

import com.arpa.speed.R;
import com.arpa.speed.models.order.invoiceOrderModel;
import com.orhanobut.logger.Logger;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PrintAdapter extends PrintDocumentAdapter {
    //https://developer.android.com/training/printing/custom-docs#java
    //https://support.google.com/cloudprint/answer/1686197?visit_id=636826184764423232-1899368832&rd=1
    //https://github.com/kesenhoo/AndroidApiDemo/blob/master/app/src/main/java/com/example/android/apis/app/PrintCustomContent.java
    //https://support.google.com/cloudprint/answer/1686197?visit_id=636826184764423232-1899368832&rd=1
    //https://support.google.com/cloudprint/answer/3153824
    //https://smallbusiness.chron.com/print-two-printers-time-43982.html

    //https://github.com/aosp-mirror/platform_frameworks_base/blob/master/core/java/android/print/PrintManager.java


    String TAG = PrintAdapter.class.getSimpleName();
    PrintedPdfDocument mPdfDocument;
    Activity activity;
    List<invoiceOrderModel> orderList = new ArrayList<>();
    // ArrayList<E> writtenPagesArray = new ArrayList<E>();
    //List<invoiceOrderModel> writtenPagesArray = new ArrayList<>();
    List<PdfDocument.PageInfo> writtenPagesArray;
    PageRange[] wpages;
    int totalPages = 0;

    public PrintAdapter(Activity activity, List<invoiceOrderModel> orderList) {
        this.activity = activity;
        this.orderList = orderList;
    }

    @Override
    public void onLayout(PrintAttributes oldAttributes, PrintAttributes newAttributes, CancellationSignal cancellationSignal, LayoutResultCallback callback, Bundle extras) {
        // Create a new PdfDocument with the requested page attributes
        mPdfDocument = new PrintedPdfDocument(activity, newAttributes);

        // Respond to cancellation request
        if (cancellationSignal.isCanceled()) {
            callback.onLayoutCancelled();
            return;
        }

        // Compute the expected number of printed pages
        int pages = computePageCount(newAttributes);

        if (pages > 0) {
            // Return print information to print framework
            PrintDocumentInfo info = new PrintDocumentInfo
                    .Builder("print_output.pdf")
                    .setContentType(PrintDocumentInfo.CONTENT_TYPE_DOCUMENT)
                    .setPageCount(pages)
                    .build();

            // Content layout reflow is complete
            callback.onLayoutFinished(info, true);
        } else {
            // Otherwise report an error to the print framework
            callback.onLayoutFailed("Page count calculation failed.");
        }

    }

    private int computePageCount(PrintAttributes printAttributes) {
        int itemsPerPage = 4; // default item count for portrait mode

        PrintAttributes.MediaSize pageSize = printAttributes.getMediaSize();
        if (!pageSize.isPortrait()) {
            // Six items per page in landscape orientation
            itemsPerPage = 6;
        }

        // Determine number of print items
        int printItemCount = getPrintItemCount();

        totalPages = (int) Math.ceil(printItemCount / itemsPerPage);
        return totalPages == 0 ? 1 : totalPages; //(int) Math.ceil(printItemCount / itemsPerPage);
    }

    private int getPrintItemCount() {
        return orderList.size();
    }

    @Override
    public void onWrite(PageRange[] pages, ParcelFileDescriptor destination, CancellationSignal cancellationSignal, WriteResultCallback callback) {

        SparseIntArray mWrittenPages = new SparseIntArray();

        // Iterate over each page of the document,
        // check if it's in the output range.
        for (int i = 0; i < totalPages; i++) {
            // Check to see if this page is in the output range.
            if (containsPage(pages, i)) {

                // If so, add it to writtenPagesArray. writtenPagesArray.size()
                // is used to compute the next output page index.
                //writtenPagesArray.add(writtenPagesArray.size(), i);
                // writtenPagesArray.add(writtenPagesArray.size());

                PdfDocument.PageInfo newPage = new PdfDocument.PageInfo.Builder(100,
                        100, i).create();

                writtenPagesArray.add(newPage);
                PdfDocument.Page page = mPdfDocument.startPage(i);

                mWrittenPages.append(mWrittenPages.size(), i);

                // check for cancellation
                if (cancellationSignal.isCanceled()) {
                    callback.onWriteCancelled();
                    mPdfDocument.close();
                    mPdfDocument = null;
                    return;
                }

                // Draw page content for printing
                drawPage(page);

                //pages += page;

                // Rendering is complete, so page can be finalized.
                mPdfDocument.finishPage(page);
            }
        }

        // Write PDF document to file
        try {
            mPdfDocument.writeTo(new FileOutputStream(
                    destination.getFileDescriptor()));
        } catch (IOException e) {
            callback.onWriteFailed(e.toString());
            return;
        } finally {
            mPdfDocument.close();
            mPdfDocument = null;
        }

        //writtenPages
        try {
            PageRange[] writtenPages = computeWrittenPages(mWrittenPages);
            // Signal the print framework the document is complete
            callback.onWriteFinished(writtenPages);
        } catch (Exception ex) {
            Logger.e(TAG, ex.getLocalizedMessage());
        }
        //callback.onWriteFinished(pages);

    }

    private PageRange[] computeWrittenPages(SparseIntArray writtenPages) {

        List<PageRange> pageRanges = new ArrayList<PageRange>();

        int start = 0;
        int end = 0;
        final int writtenPageCount = writtenPages.size();
        for (int i = 0; i < writtenPageCount; i++) {
            if (start <= 0) {
                start = writtenPages.valueAt(i);
            }
            int oldEnd = end = start;
            while (i < writtenPageCount && (end - oldEnd) <= 1) {
                oldEnd = end;
                end = writtenPages.valueAt(i);
                i++;
            }
            PageRange pageRange = new PageRange(start, end);
            pageRanges.add(pageRange);
            start = end = -1;
        }

        PageRange[] pageRangesArray = new PageRange[pageRanges.size()];
        pageRanges.toArray(pageRangesArray);
        return pageRangesArray;

    }


    private boolean containsPage(PageRange[] pages, int i) {
        // check if contains
        try {
            PageRange pr = pages[i];
        } catch (IllegalArgumentException e) {
            return false;
        }
        return true;
    }

    private void drawPage(PdfDocument.Page page) {
        Canvas canvas = page.getCanvas();

        // units are in points (1/72 of an inch)
        int titleBaseLine = 72;
        int leftMargin = 54;

        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setTextSize(36);
        canvas.drawText("Test Title", leftMargin, titleBaseLine, paint);

        paint.setTextSize(11);
        canvas.drawText("Test paragraph", leftMargin, titleBaseLine + 25, paint);

        paint.setColor(Color.BLUE);
        canvas.drawRect(100, 100, 172, 172, paint);
    }


}
