package com.arpa.speed.modules.order;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

//import com.andexert.library.RippleView;
import com.arpa.speed.R;
import com.arpa.speed.customViews.customTextView;
import com.arpa.speed.models.order.invoiceOrderModel;
import com.arpa.speed.utils.publicUtils;

import java.util.List;

public class orderAdapter
        extends RecyclerView.Adapter<orderAdapter.ViewHolder> {

    List<invoiceOrderModel> itemList;
    Context mContext;
    private LayoutInflater mInflater;
    private orderAdapter.ItemClickListener mClickListener;
    private int position;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public orderAdapter(Context mContext, List<invoiceOrderModel> itemList) {
        this.mInflater = LayoutInflater.from(mContext);
        this.itemList = itemList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.recycler_order_item, viewGroup, false);
        return new orderAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull orderAdapter.ViewHolder holder, int position) {
        holder.order_item_name.setText(itemList.get(position).getItemName());
        holder.item_price.setText(publicUtils.commaStringDouble(itemList.get(position).getRowSum()));
        holder.item_count.setText(itemList.get(position).getItemCount() + "");

        if (position % 2 == 1) {
            holder.itemView.setBackgroundColor(Color.parseColor("#FFFFFF"));
        } else {
            holder.itemView.setBackgroundColor(Color.parseColor("#FFFAF8FD"));
        }

       /* holder.order_item_name.setOnLongClickListener(v -> {
            setPosition(holder.getAdapterPosition());
            return false;
        });*/
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener, View.OnCreateContextMenuListener {
        customTextView order_item_name, item_count, item_price;
        LinearLayout order_linear;

        ViewHolder(View itemView) {
            super(itemView);

            order_item_name = itemView.findViewById(R.id.order_item_name);
            item_count = itemView.findViewById(R.id.item_count);
            item_price = itemView.findViewById(R.id.item_price);
            order_linear = itemView.findViewById(R.id.order_linear);

            itemView.setOnClickListener(this);
            order_item_name.setOnClickListener(this);
            itemView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onOrderClick(view, getAdapterPosition());
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v,
                                        ContextMenu.ContextMenuInfo menuInfo) {
            menu.setHeaderTitle(R.string.order);
            menu.add(0, 0, 0, R.string.update);//groupId, itemId, order, title
            menu.add(0, 1, 0, R.string.delete);
        }
    }

    // convenience method for getting data at click position
    public invoiceOrderModel getItem(int position) {
        return itemList.get(position);
    }

    // allows clicks events to be caught
    public void setClickListener(orderAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onOrderClick(View view, int position);
    }

}
