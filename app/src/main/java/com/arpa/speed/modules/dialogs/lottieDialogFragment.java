package com.arpa.speed.modules.dialogs;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieComposition;
import com.arpa.speed.R;
import com.arpa.speed.customIconView.titleValueTextView;
import com.arpa.speed.customViews.customTextView;
import com.arpa.speed.utils.BaseApplication;
import com.arpa.speed.utils.Constants;
import com.orhanobut.logger.Logger;

public class lottieDialogFragment extends DialogFragment {

    customTextView loading_header, api_min_max, loading_title;
    LottieAnimationView  av_from_code;


    public lottieDialogFragment() {
    }

    public static lottieDialogFragment newInstance(String title, int min, int max) {
        lottieDialogFragment frag = new lottieDialogFragment();
        Bundle args = new Bundle();

        args.putString("title", title);
        args.putInt("min", min);
        args.putInt("max", max);

        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.custom_loading_dialog, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Get field from view
        //loading_header = view.findViewById(R.id.loading_header);
      //  api_min_max = view.findViewById(R.id.api_min_max);

        loading_title = view.findViewById(R.id.loading_title);
        // lottie_animation_view = view.findViewById(R.id.lottie_animation_view);
        av_from_code = view.findViewById(R.id.av_from_code);

        String min_max = getArguments().getInt("min") + "/" + getArguments().getInt("max");
        Integer max = getArguments().getInt("max");
        String title = getArguments().getString("title", "Enter Name");
        //getDialog().setTitle(title);
        loading_title.setText(String.format("%d %s", R.string.isLoading, title));

        try {
            LottieComposition.Factory.fromAssetFileName(BaseApplication.getApp(), "cooking", composition -> {
                av_from_code.setComposition(composition);
                av_from_code.setProgress(max);
                av_from_code.loop(true);

                av_from_code.playAnimation();

            });
        } catch (Exception ex) {
            Logger.e("lottie asses error", ex.getMessage());
        }

    }


}
