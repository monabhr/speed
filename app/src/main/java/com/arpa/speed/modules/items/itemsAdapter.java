package com.arpa.speed.modules.items;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arpa.speed.R;
import com.arpa.speed.boxDatabase.itemsEntity;
import com.arpa.speed.customViews.customTextView;

import java.util.List;

public class itemsAdapter extends RecyclerView.Adapter<itemsAdapter.ViewHolder> {

    List<itemsEntity> itemList;
    Context mContext;
    private LayoutInflater mInflater;
    private itemsAdapter.ItemClickListener mClickListener;



    public itemsAdapter(Context mContext, List<itemsEntity> itemList) {
        this.mInflater = LayoutInflater.from(mContext);
        this.itemList = itemList;
        this.mContext = mContext;
    }


    @NonNull
    @Override
    public itemsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.recycler_items, viewGroup, false);
        return new itemsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull itemsAdapter.ViewHolder holder, int position) {
        holder.item_name.setText(itemList.get(position).itemName);

    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        customTextView item_name;
        //RippleView item_name_ripple;
        ViewHolder(View itemView) {
            super(itemView);
            item_name = itemView.findViewById(R.id.item_name);
            itemView.setOnClickListener(this);

            //item_name_ripple = itemView.findViewById(R.id.item_name_ripple);
           // item_name_ripple.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemsClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public itemsEntity getItem(int position) {
        return itemList.get(position);
    }

    // allows clicks events to be caught
    public void setClickListener(itemsAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemsClick(View view, int position);
    }
}
