package com.arpa.speed.modules.login;

import com.arpa.speed.models.login.loginDbModel;
import com.arpa.speed.modules.syncData.dataSyncModel;
import com.arpa.speed.modules.login.dbModel.userModel;

import io.reactivex.disposables.Disposable;

public class loginPresenter implements loginContracts.Presenter {

    loginContracts.View view;
    loginModel model = new loginModel();
    loginContracts.data_sync_mode sync_ = new dataSyncModel();

    public loginPresenter(loginContracts.View view) {
        this.view = view;
        model.onAttachPresenter(this);
        sync_.onAttachLoginPresenter(this);
    }

    @Override
    public void onCheckInfo(String username, String password, String server) {
        view.onCheckInfo(username, password, server);
    }

    /*  @Override
      public void onLoginClick(userModel userModel) {
          model.onLoginClick(userModel);
      }

      @Override
      public void onLoginFailed(String message) {
          view.onLoginFailed(message);
      }

      @Override
      public void onLoginSuccess(loginDbModel dbModel) {
          view.onLoginSuccess(dbModel);
      }
  */
    @Override
    public void onDispose(Disposable d) {
        view.onDispose(d);
    }

    @Override
    public void onCatch(String position, String cause) {
        view.onCatch(position, cause);
    }

    @Override
    public void onShowProgress(int count, String title) {
        view.onShowProgress(count, title);
    }

    @Override
    public void onHideProgress() {
        view.onHideProgress();
    }

    @Override
    public void onSync(userModel userModel) {
        sync_.onSyncClicked(userModel);
    }

    @Override
    public void onSyncFailed(String message) {
        view.onSyncFailed(message);
    }

    @Override
    public void onSyncSucceed(loginDbModel model) {
        view.onSyncSucceed(model);
    }
}
