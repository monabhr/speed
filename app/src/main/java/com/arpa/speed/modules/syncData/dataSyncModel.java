package com.arpa.speed.modules.syncData;

import android.os.Handler;
import android.os.Looper;

import com.arpa.speed.R;
import com.arpa.speed.boxDatabase.DatabaseAdapter;
import com.arpa.speed.boxDatabase.businessEntity;
import com.arpa.speed.boxDatabase.itemCatsEntity;
import com.arpa.speed.boxDatabase.itemsEntity;
import com.arpa.speed.boxDatabase.restTableCat;
import com.arpa.speed.boxDatabase.restTableEntity;
import com.arpa.speed.boxDatabase.saveTransEntity;
import com.arpa.speed.boxDatabase.savedTLineEntity;
import com.arpa.speed.boxDatabase.tableOrderEntity;
import com.arpa.speed.models.business.businessDbModel;
import com.arpa.speed.models.itemCats.itemCatsDbModel;
import com.arpa.speed.models.items.itemsDbModel;
import com.arpa.speed.models.login.Data;
import com.arpa.speed.models.login.loginDbModel;
import com.arpa.speed.models.restTable.restTableDbModel;
import com.arpa.speed.models.restTableCat.Datum;
import com.arpa.speed.models.restTableCat.restTableCatDbModel;
import com.arpa.speed.models.savedTrans.SavedTransactionsModel;
import com.arpa.speed.models.savedTrans.Transline;
import com.arpa.speed.models.userDefaults.userDefaultsDbModel;
import com.arpa.speed.modules.home.homeContracts;
import com.arpa.speed.modules.home.homePresenter;
import com.arpa.speed.modules.login.dbModel.userModel;
import com.arpa.speed.modules.login.loginContracts;
import com.arpa.speed.network.webInterface;
import com.arpa.speed.utils.BaseApplication;
import com.arpa.speed.utils.Constants;
import com.arpa.speed.utils.RxRetoGenerator;
import com.arpa.speed.utils.hawkHandler;
import com.orhanobut.logger.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.google.android.gms.common.util.CollectionUtils.mutableListOf;

public class dataSyncModel implements homeContracts.data_sync_Model
        , loginContracts.data_sync_mode {

    private homeContracts.home_Presenter presenter;
    private loginContracts.Presenter login_presenter;

    Disposable disposable;
    DatabaseAdapter dbAdapter = new DatabaseAdapter();
    private Data userData;
    loginDbModel modell;
    Handler handler = new Handler(Looper.myLooper());

    @Override
    public void onAttachPresenter(homePresenter homePresenter) {
        this.presenter = homePresenter;
    }

    @Override
    public void onSync(userModel userModel) {
        try {
            syncData(userModel);
        } catch (Exception ex) {
            if (presenter != null)
                presenter.onCatch("onSync ", ex.getMessage());
            else
                login_presenter.onCatch("onSync ", ex.getMessage());
        }
    }

    @Override
    public void onAttachLoginPresenter(loginContracts.Presenter presenter) {
        this.login_presenter = presenter;
    }

    @Override
    public void onSyncClicked(userModel userModel) {
        try {
            syncData(userModel);
        } catch (Exception ex) {
            Logger.e("onSyncClicked ", ex.getMessage());
        }
    }

    private void syncData(userModel userModel) {
        try {
            webInterface webInterface = RxRetoGenerator.createPingService(com.arpa.speed.network.webInterface.class, userModel.getServerAddress());

            if (presenter != null)
                presenter.onShowProgress(1, BaseApplication.getApp().getString(R.string.userLoginInfo));
            else
                login_presenter.onShowProgress(1, BaseApplication.getApp().getString(R.string.userLoginInfo));

            Observable<loginDbModel> observable = webInterface.getPingServerToken(userModel.getUsername(), userModel.getPassword());

            observable
                    .flatMap(response -> {
                        try {
                            if (response.getData() != null) {
                                modell = response;
                                userData = response.getData();
                                dbAdapter.clearDatabase();

                                saveLoginData(userModel.getServerAddress());
                                return webInterface.getRestTableCategories(userData.getUserId());
                            } else {
                                handler.post(
                                        () -> {
                                            if (presenter != null) {
                                                presenter.onSyncFailed(response.getError());
                                                presenter.onHideProgress();
                                            }

                                            if (login_presenter != null) {
                                                login_presenter.onSyncFailed(response.getError());
                                                login_presenter.onHideProgress();
                                            }
                                        }
                                );

                                return observable.error(new IOException(response.getError()));
                            }
                        } catch (Throwable t) {
                            return observable.error(t);
                        }
                    })
                    .flatMap(response -> {
                        handler.post(
                                () -> {
                                    if (presenter != null) {
                                        presenter.onShowProgress(response.getData().size(), BaseApplication.getApp().getString(R.string.restTableCats));
                                    }

                                    if (login_presenter != null) {
                                        login_presenter.onShowProgress(response.getData().size(), BaseApplication.getApp().getString(R.string.restTableCats));
                                    }
                                }
                        );

                        saveRestTableCat(response);
                        return webInterface.getRestTable(userData.getUserId());
                    })
                    .flatMap(response -> {
                        handler.post(
                                () -> {
                                    if (presenter != null) {
                                        presenter.onShowProgress(response.getData().size(), BaseApplication.getApp().getString(R.string.restTables));
                                    }

                                    if (login_presenter != null) {
                                        login_presenter.onShowProgress(response.getData().size(), BaseApplication.getApp().getString(R.string.restTables));
                                    }
                                }
                        );

                        saveRestTable(response);
                        return webInterface.getDefaults(userData.getUserId());
                    })
                    .flatMap(response -> {
                        handler.post(
                                () -> {
                                    if (presenter != null) {
                                        presenter.onShowProgress(response.getData().size(), BaseApplication.getApp().getString(R.string.userDefaults));
                                    }

                                    if (login_presenter != null) {
                                        login_presenter.onShowProgress(response.getData().size(), BaseApplication.getApp().getString(R.string.userDefaults));
                                    }
                                }
                        );
                        saveDefaults(response);
                        return webInterface.getBusiness(userData.getUserId());
                    })

                    .flatMap(response -> {
                        handler.post(
                                () ->
                                {
                                    if (presenter != null) {
                                        presenter.onShowProgress(response.getData().size(), BaseApplication.getApp().getString(R.string.business));
                                    }

                                    if (login_presenter != null) {
                                        login_presenter.onShowProgress(response.getData().size(), BaseApplication.getApp().getString(R.string.business));
                                    }
                                }
                        );
                        saveBusiness(response);
                        return webInterface.getItems(userData.getUserId());
                    })
                    .flatMap(response -> {
                        handler.post(
                                () ->
                                {
                                    if (presenter != null) {
                                        presenter.onShowProgress(response.getData().size(), BaseApplication.getApp().getString(R.string.items));
                                    }

                                    if (login_presenter != null) {
                                        login_presenter.onShowProgress(response.getData().size(), BaseApplication.getApp().getString(R.string.items));
                                    }
                                }
                        );
                        saveItems(response);
                        return webInterface.getItemCats(userData.getUserId());
                    })
                    .flatMap(response -> {
                        handler.post(
                                () ->
                                {
                                    if (presenter != null) {
                                        presenter.onShowProgress(response.getData().size(), BaseApplication.getApp().getString(R.string.itemCats));
                                    }

                                    if (login_presenter != null) {
                                        login_presenter.onShowProgress(response.getData().size(), BaseApplication.getApp().getString(R.string.itemCats));
                                    }
                                }
                        );
                        saveItemCats(response);
                        // return webInterface.getItemAttachment(userData.getUserId());
                        return webInterface.getTransactions(userData.getUserId(), 0);
                    })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .unsubscribeOn(Schedulers.io())
                    .subscribe(new Observer<SavedTransactionsModel>() {

                        @Override
                        public void onSubscribe(Disposable d) {
                            Logger.d("onSubscribe");

                            disposable = d;
                            if (presenter != null)
                                presenter.onDispose(disposable);
                            if (login_presenter != null)
                                login_presenter.onDispose(disposable);
                        }

                        @Override
                        public void onNext(SavedTransactionsModel obj) {
                            saveTransEntity(obj);
                            Logger.d("onNext");
                            if (presenter != null)
                                presenter.onSyncSucceed(modell);
                            if (login_presenter != null)
                                login_presenter.onSyncSucceed(modell);
                        }

                        @Override
                        public void onError(Throwable e) {
                            Logger.e(e.getMessage());
                            if (presenter != null)
                                presenter.onSyncFailed(e.getMessage());
                            if (login_presenter != null)
                                login_presenter.onSyncFailed(e.getMessage());

                        }

                        @Override
                        public void onComplete() {
                            if (presenter != null)
                                presenter.onHideProgress();
                            if (login_presenter != null)
                                login_presenter.onHideProgress();
                        }
                    });

        } catch (Exception ex) {
            if (presenter != null)
                presenter.onCatch("syncDats", ex.getMessage());

            if (login_presenter != null)
                login_presenter.onCatch("syncDats", ex.getMessage());
        }
    }

    private void saveItemCats(itemCatsDbModel response) {
        try {
            if (response.getData() != null) {
                List<itemCatsEntity> mutableDataList = mutableListOf();

                for (com.arpa.speed.models.itemCats.Datum x : response.getData()) {
                    itemCatsEntity rst = new itemCatsEntity();

                    rst.itemCategoryID = Long.parseLong(x.getItemCategoryID().toString());
                    rst.itemCategoryCode = x.getItemCategoryCode();
                    rst.itemCategoryName = x.getItemCategoryName();

                    mutableDataList.add(rst);
                }

                dbAdapter.insertItemCats(mutableDataList);

            } else if (response.getError() != null) {
                Logger.e("saveItemCats error : ", response.getError());
            }

        } catch (Exception ex) {
            Logger.e("saveItemCats error : ", ex.getMessage());

        }
    }

    private void saveItems(itemsDbModel response) {
        try {
            if (response.getData() != null) {
                List<itemsEntity> mutableDataList = mutableListOf();

                for (com.arpa.speed.models.items.Datum x : response.getData()) {

                    itemsEntity rst = new itemsEntity();
                    rst.itemID = Long.parseLong(x.getItemID().toString());
                    rst.itemCode = x.getItemCode();
                    rst.itemName = x.getItemName();
                    rst.itemCategoryID = x.getItemCategoryID();
                    rst.inverseUnitsRatio = x.getInverseUnitsRatio();
                    rst.salePrice = x.getSalePrice();
                    rst.unitsRatio = x.getUnitsRatio();

                    mutableDataList.add(rst);
                }

                dbAdapter.insertItems(mutableDataList);

            } else if (response.getError() != null) {
                Logger.e("saveItems error : ", response.getError());
            }

        } catch (Exception ex) {
            Logger.e("saveItems error : ", ex.getMessage());

        }
    }

    private void saveBusiness(businessDbModel response) {
        try {
            if (response.getData() != null) {
                List<businessEntity> mutableDataList = mutableListOf();

                for (com.arpa.speed.models.business.Datum x : response.getData()) {

                    businessEntity rst = new businessEntity();
                    rst.businessID = Long.parseLong(x.getBusinessID().toString());
                    rst.businessCode = x.getBusinessCode();
                    rst.businessName = x.getBusinessName();
                    rst.address = x.getAddress();
                    rst.mobile = x.getMobile();
                    rst.phoneNo = x.getPhoneNo();
                    rst.lat = x.getLat();
                    rst._long = x.getLong();
                    rst.inActive = x.getInActive();
                    rst.geoRegionID = x.getGeoRegionID();

                    mutableDataList.add(rst);

                }

                dbAdapter.insertBusiness(mutableDataList);

            } else if (response.getError() != null) {
                Logger.e("saveBusiness error : ", response.getError());
            }

        } catch (Exception ex) {
            Logger.e("saveBusiness error : ", ex.getMessage());

        }
    }

    private void saveDefaults(userDefaultsDbModel response) {
        try {
            if (response.getData() != null) {
                com.arpa.speed.models.userDefaults.Datum defs = response.getData().get(0);
                hawkHandler.saveData(Constants.user_defaults, defs);
            } else {
                Logger.e("saveDefaults error : ", response.getError());
            }
        } catch (Exception ex) {
            Logger.e("saveDefaults error : ", ex.getMessage());
        }
    }

    private void saveRestTable(restTableDbModel response) {
        try {
            if (response.getData() != null) {
                List<restTableEntity> mutableDataList = mutableListOf();

                for (com.arpa.speed.models.restTable.Datum x : response.getData()) {

                    restTableEntity rst = new restTableEntity();
                    rst.restaurantTableID = Long.parseLong(x.getRestaurantTableID().toString());
                    rst.tableCode = x.getTableCode();
                    rst.tableName = x.getTableName();
                    rst.tableNumber = x.getTableNumber();
                    // rst.isReserved = false;
                    rst.restaurantTableCategoryID = x.getRestaurantTableCategoryID();

                    mutableDataList.add(rst);

                }

                dbAdapter.insertRestTables(mutableDataList);

            } else if (response.getError() != null) {
                Logger.e("saveRestTable error : ", response.getError());
            }

        } catch (Exception ex) {
            Logger.e("saveRestTable error : ", ex.getMessage());

        }
    }

    private void saveRestTableCat(restTableCatDbModel response) {
        try {
            if (response.getData() != null) {
                List<restTableCat> mutableDataList = mutableListOf();

                for (Datum x : response.getData()) {

                    restTableCat rst = new restTableCat();
                    rst.restaurantTableCategoryID = Long.parseLong(x.getRestaurantTableCategoryID().toString());
                    rst.restaurantTableCategoryCode = x.getRestaurantTableCategoryCode();
                    rst.restaurantTableCategoryName = x.getRestaurantTableCategoryName();
                    rst.descriptions = x.getDescriptions();

                    mutableDataList.add(rst);
                }

                dbAdapter.insertRestTableCategories(mutableDataList);
            } else if (response.getError() != null) {
                Logger.e("saveRestTableCat error : ", response.getError());
            }

        } catch (Exception ex) {
            Logger.e("saveRestTableCat error : ", ex.getMessage());

        }

    }

    private void saveLoginData(String serverAdr) {

        com.arpa.speed.utils.hawkHandler handler = com.arpa.speed.utils.hawkHandler
                .getInstance();

        userModel model = new userModel();
        model.setUsername(userData.getUsername());
        model.setServerAddress(serverAdr);
        model.setUserID(userData.getUserId());
        model.setLoginStatus(true);

        handler.saveData(Constants.user_model, model);
    }

    private void saveTransEntity(SavedTransactionsModel objs) {

        List<saveTransEntity> objEntity = new ArrayList<>();

        if (objs.getData() != null) {
            for (com.arpa.speed.models.savedTrans.Datum data : objs.getData()) {

                saveTransEntity item = new saveTransEntity();

                if (data.getPaidAmount() > 0) {
                    // isPaid = true;
                    deleteOrder(data.getTransactionID(), data.getRestaurantTableID());
                    deleteSaveTransEntity(data.getTransactionID(), data.getRestaurantTableID());
                } else {
                    item.creatorUserID = data.getCreatorUserID();
                    item.businessID = data.getBusinessID();
                    item.paidAmount = data.getPaidAmount();
                    item.transNumber = data.getTransNumber();
                    item.totalSum = 0;
                    item.transDiscountPercent = data.getTransDiscountPercent();
                    item.transDiscountPercent = data.getTransDiscountAmount();
                    item.restaurantTableID = data.getRestaurantTableID();
                    item.transactionID = Integer.parseInt(data.getTransactionID() + "");

                    for (Transline s : data.getTranslines()) {
                        savedTLineEntity obj = new savedTLineEntity();
                        obj.setItemId(s.getItemId());
                        obj.setTransactionId(Integer.parseInt(s.getTransactionId() + ""));
                        obj.setTollAmount(s.getTollAmount());
                        obj.setTaxAmount(s.getTaxAmount());
                        obj.setPrice(s.getPrice());
                        obj.setnQty(Integer.parseInt(s.getNQty().toString()));
                        obj.setTranslineId(s.getTranslineId());
                        obj.setItemCategoryId(s.getItemCategoryId());

                        item.translines.add(obj);
                        addOrderList(item, s);
                    }

                    objEntity.add(item);

                }
            }

            dbAdapter.insertSavedTransactions(objEntity);
        }
    }

    private void addOrderList(saveTransEntity item, Transline obj) {

        tableOrderEntity order = new tableOrderEntity();

        tableOrderEntity reservedOrder = dbAdapter.getOrderByTableId(
                Long.parseLong(item.getRestaurantTableID() + ""), obj.getTranslineId());

        if (reservedOrder != null) {
            order = reservedOrder;
            order.setId(reservedOrder.getId());
        }

        order.setPaidAmount(item.getPaidAmount());
        order.setTableID(item.getRestaurantTableID());
        order.setTranslineID(obj.getTranslineId());
        order.setTransNumber(item.getTransNumber());
        order.setUserId(item.getCreatorUserID());
        order.setRowSum(Double.parseDouble(obj.getAmount().toString()));
        order.setUnitsRatio(0.0);
        order.setItemName(obj.getItemName());
        order.setItemCount(obj.getnQty());
        order.setTransactionID(obj.getTransactionId());
        order.setItemID(obj.getItemId());
        order.setSalePrice(Double.parseDouble(obj.getPrice().toString()));
        order.setItemCategoryID(obj.getItemCategoryId());
        order.setArchived(false);

        dbAdapter.insertOrder(order);
    }

    private void deleteSaveTransEntity(Integer transactionID, Integer restaurantTableID) {
        List<saveTransEntity> list = dbAdapter.getSavedTransactions(transactionID, restaurantTableID);
        for (saveTransEntity ent : list) {
            dbAdapter.clearSaveTransEntity(ent);
        }
    }

    void deleteOrder(int transactionId, int tableId) {

        List<tableOrderEntity> reservedList = dbAdapter.getOrderByTableId(Long.parseLong(tableId + ""));

        for (tableOrderEntity ent :
                reservedList) {
            dbAdapter.clearOrder(ent);
        }

    }
}
