package com.arpa.speed.modules.items;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;

import com.arpa.speed.R;
import com.arpa.speed.boxDatabase.DatabaseAdapter;
import com.arpa.speed.boxDatabase.TransLineEntity;
import com.arpa.speed.boxDatabase.TransactionEntity;
import com.arpa.speed.boxDatabase.orderEntity;
import com.arpa.speed.boxDatabase.restTableEntity;
import com.arpa.speed.boxDatabase.saveTransEntity;
import com.arpa.speed.boxDatabase.savedTLineEntity;
import com.arpa.speed.boxDatabase.tableOrderEntity;
import com.arpa.speed.models.order.invoiceOrderModel;
import com.arpa.speed.models.savedTrans.Datum;
import com.arpa.speed.models.savedTrans.SavedTransactionsModel;
import com.arpa.speed.models.savedTrans.Transline;
import com.arpa.speed.models.transaction.ObjectToSend;
import com.arpa.speed.models.transaction.transactionModel;
import com.arpa.speed.models.transaction.translineModel;
import com.arpa.speed.modules.order.dbModel.Data;
import com.arpa.speed.modules.order.dbModel.Model;
import com.arpa.speed.network.webInterface;
import com.arpa.speed.utils.BaseApplication;
import com.arpa.speed.utils.RxRetoGenerator;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.orhanobut.logger.Logger;

/*import org.checkerframework.checker.nullness.qual.Nullable;*/

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.google.android.gms.common.util.CollectionUtils.mutableListOf;

public class itemsModel implements itemContract.item_model {

    private itemContract.item_Presenter presenter;
    Disposable disposable;
    DatabaseAdapter dbAdapter = new DatabaseAdapter();
    //Handler handler = new Handler(Looper.myLooper());
    webInterface webInterface;
    int userId;
    SavedTransactionsModel savedTrans;
    List<saveTransEntity> mutableDataList;
    List<TransLineEntity> translineList;
    List<invoiceOrderModel> orderList;
    long transacId = 0;
    String trans_Number = "";
    Context mContext;

    @Override
    public void attachPresenter(itemContract.item_Presenter item_presenter) {
        this.presenter = item_presenter;
        this.mContext = BaseApplication.getApp();
    }

    @Override
    public void onSave(ObjectToSend data, String serverAddress) {
        try {

            userId = data.data.userID;

            webInterface = RxRetoGenerator.createPingService(com.arpa.speed.network.webInterface.class,
                    serverAddress);


            presenter.onShowProgress();

            Observable<Model> saveOrder = webInterface.postTransaction(data);
            saveOrder.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Model>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            disposable = d;
                            presenter.onDispose(disposable);
                        }

                        @Override
                        public void onNext(Model result) {
                            if (result.getError() != null) {
                                String errorText = result.getError();
                                presenter.onSaveOrderFailed(errorText);
                            } else {
                                presenter.onSaveOrderSucceed(result);
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            Logger.e(e.getMessage());
                            presenter.onSaveOrderFailed(e.getMessage());
                        }

                        @Override
                        public void onComplete() {
                            presenter.onHideProgress();
                        }
                    });

        } catch (Exception ex) {
            Logger.e("postTransaction catch  ", ex.getMessage());
        }
    }

    @Override
    public void onSaveTransactionResult(Model entity) {
        try {
            if (entity != null) {

                Data dt = entity.getData();
                orderEntity ent = new orderEntity();

                ent.saleTransactionID = dt.getSaleTransactionID();
                ent.buildTransactionID = dt.getBuildTransactionID();
                ent.buildTransactionNumber = dt.getBuildTransactionNumber();
                ent.paymentTransactionID = dt.getPaymentTransactionID();
                ent.saleTransactionNumber = dt.getSaleTransactionNumber();
                ent.paymentTransactionNumber = dt.getPaymentTransactionNumber();

                // dbAdapter.insertTransactionResult(ent);

                Long transactionId = new Long(0);
                int transNumber = 0;

                if (ent.saleTransactionID != null) {

                    transactionId = Long.parseLong(ent.saleTransactionID);
                    transNumber = Integer.parseInt(ent.saleTransactionNumber);

                } else if (ent.buildTransactionID != null) {
                    transactionId = Long.parseLong(ent.buildTransactionID);
                    transNumber = Integer.parseInt(ent.buildTransactionNumber);

                } else if (ent.paymentTransactionID != null) {

                    transactionId = Long.parseLong(ent.paymentTransactionID);
                    transNumber = Integer.parseInt(ent.paymentTransactionNumber);
                }

                if (transactionId > 0) {

                    transactionModel transactionModel = new transactionModel();
                    transactionModel.transactionID = transactionId;
                    transactionModel.transNumber = transNumber;

                    transacId = transactionId;
                    trans_Number = transNumber + "";

                    reLoadSavedTransactionInfo(userId, Integer.parseInt(transactionId.toString()));

                    presenter.onSaveTransactionResultSucceed(transactionModel);

                } else {
                    presenter.onSaveOrderFailed(mContext.getString(R.string.save_order_failed));
                }


            } else if (entity.getError() != null) {
                Logger.e("saveToDB TransactionResult error : ", entity.getError());
                presenter.onSaveOrderFailed(entity.getError());
            }

        } catch (Exception ex) {
            presenter.onSaveOrderFailed(ex.getLocalizedMessage() != null ? ex.getLocalizedMessage() : ex.getMessage());
            Logger.e("onSaveTransactionResult catch  : ", ex.getMessage());
        }
    }

    private void reLoadSavedTransactionInfo(int userId, int transactionId) {

        try {

            Observable<SavedTransactionsModel> savedRecord =
                    webInterface
                            .getTransactions(userId, Integer.parseInt(transactionId + ""));

            savedRecord.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<SavedTransactionsModel>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            //Logger.e("getTransactions onError ", e.getMessage());
                        }

                        @Override
                        public void onNext(SavedTransactionsModel response) {
                            savedTrans = response;
                            saveTransModel(response);
                        }

                        @Override
                        public void onError(Throwable e) {
                            Logger.e("getTransactions onError ", e.getMessage());
                        }

                        @Override
                        public void onComplete() {

                        }
                    });

        } catch (Exception ex) {
            Logger.e("reLoadSavedTransactionInfo catch ", ex.getMessage());
        }

    }

    private void saveTransModel(SavedTransactionsModel response) {
        try {
            if (response.getData() != null) {

                mutableDataList = mutableListOf();
                translineList = new ArrayList<>();

                for (com.arpa.speed.models.savedTrans.Datum x : response.getData()) {

                    //1--------------------------
                    saveTransEntity rst = new saveTransEntity();

                    rst.setBusinessID(x.getBusinessID());
                    rst.setBusinessName(x.getBusinessName());
                    rst.setDocAliasID(x.getDocAliasID());
                    rst.setTotalSum(x.getTotalSum());
                    rst.setTransDate(x.getTransDate());
                    rst.setTransDiscountAmount(x.getTransDiscountAmount());
                    rst.setTransDiscountPercent(x.getTransDiscountPercent());
                    rst.setTransNumber(x.getTransNumber());
                    rst.setCreatorUserID(x.getCreatorUserID());
                    rst.setRestaurantTableID(x.getRestaurantTableID());
                    rst.setTransactionID(x.getTransactionID());
                    rst.setPaidAmount(x.getPaidAmount());
                    rst.setArchived(false);

                    //2---------------------------------------
                    for (Transline cur : x.getTranslines()) {

                        savedTLineEntity existsTrans = dbAdapter.getSavedTransLines(
                                cur.getTranslineId());

                        if (existsTrans != null) {
                            editCashedList(existsTrans, cur, x);
                        } else {
                            savedTLineEntity tr = new savedTLineEntity();

                            tr.setAmount(cur.getAmount());
                            tr.setDiscountAmount(cur.getDiscountAmount());
                            tr.setDiscountPercent(cur.getDiscountPercent());
                            tr.setItemName(cur.getItemName());
                            tr.setnQty(cur.getNQty());
                            tr.setPrice(cur.getPrice());
                            tr.setTaxAmount(cur.getTaxAmount());
                            tr.setTollAmount(cur.getTollAmount());
                            tr.setItemId(cur.getItemId());
                            tr.setTranslineId(cur.getTranslineId());
                            tr.setTransactionId(cur.getTransactionId());

                            rst.translines.add(tr);

                            try {
                                invoiceOrderModel ordItem = Iterables.tryFind(orderList,
                                        obj -> tr.getItemId().equals(obj.getItemID())).orNull();

                                if (ordItem != null) {
                                    ordItem.setTranslineID(cur.getTranslineId());
                                    ordItem.setTransactionID(cur.getTransactionId());
                                    ordItem.setPaidAmount(x.getPaidAmount());
                                    ordItem.setTransNumber(x.getTransNumber());
                                    ordItem.setArchived(false);
                                }
                            } catch (Exception ex) {
                                Logger.e("Iterables.tryFind  ", ex.getLocalizedMessage());
                            }
                        }
                    }

                    mutableDataList.add(rst);
                    dbAdapter.insertSavedTransactions(mutableDataList);

                    saveOrder(orderList, transacId, false);
                    // setSaveTransArchived|(orderList);
                }
            } else if (response.getError() != null) {
                Logger.e("saveTransModel else error : ", response.getError());
            }

        } catch (Exception ex) {
            Logger.e("saveTransModel catch error : ", ex.getMessage());
        }

    }

    private void saveOrder(List<invoiceOrderModel> orderList, long transactionId, boolean archived) {
        try {

            if (transactionId > 0)
                transacId = transactionId;

            for (invoiceOrderModel ord : orderList) {
                tableOrderEntity transln;

                if (ord.Id > 0) {
                    transln = dbAdapter.findOrderById(ord.Id);
                } else {
                    transln = dbAdapter.getOrders(transacId, ord.translineID);
                }

                if (transln != null) { //edit order
                    {
                        insUpdateOrder(transln, ord, archived);

                        saveTransEntity objEntity = dbAdapter.getSavedTransaction(Integer.parseInt(ord.getTransactionID() + "")
                                , Integer.parseInt(ord.getTableID() + ""));

                        objEntity.setArchived(false);

                        dbAdapter.insertSavedTransactions(objEntity);
                    }


                } else { //insert new order
                    transln = new tableOrderEntity();
                    insUpdateOrder(transln, ord, archived);
                }
            }

            presenter.onSaveOrderSucceed();

        } catch (Exception ex) {
            Logger.e("saveOrder catch error : ", ex.getMessage());
        }
    }

    private void insUpdateOrder(tableOrderEntity transln, invoiceOrderModel ord, boolean archived) {

        if (ord.getId() > 0)
            transln.setId(ord.getId());

        transln.setTransactionID(transacId);
        transln.setTranslineID(ord.translineID);
        transln.setItemCount(ord.itemCount);
        transln.setRowSum(ord.rowSum);
        transln.setItemCode(ord.itemCode);
        transln.setInverseUnitsRatio(ord.inverseUnitsRatio);
        transln.setItemCategoryID(ord.itemCategoryID);
        transln.setItemID(ord.itemID);
        transln.setItemName(ord.itemName);
        transln.setSalePrice(ord.salePrice);
        transln.setTableID(ord.tableID);
        transln.setTotalCount(ord.totalCount);
        transln.setTotalSum(ord.totalSum);
        transln.setUnitsRatio(ord.unitsRatio);
        transln.setPaidAmount(ord.paidAmount);
        transln.setTransNumber(ord.getTransNumber());
        transln.setArchived(archived);


        dbAdapter.insertOrder(transln);
    }

    private void editCashedList(savedTLineEntity existsTrans, Transline current, Datum x) {
        try {

            existsTrans.setDiscountAmount(current.getDiscountAmount());
            existsTrans.setDiscountPercent(current.getDiscountPercent());
            existsTrans.setItemId(current.getItemId());
            existsTrans.setnQty(current.getnQty());
            existsTrans.setPrice(current.getPrice());
            existsTrans.setTaxAmount(current.getTaxAmount());
            existsTrans.setTollAmount(current.getTollAmount());
            existsTrans.setTranslineId(current.getTranslineId());
            existsTrans.setTransactionId(current.getTransactionId());

            invoiceOrderModel ordItem = Iterables.tryFind(orderList,
                    objs -> current.getItemId().equals(objs.getItemID())).orNull();

            if (ordItem != null) {
                ordItem.setTranslineID(current.getTranslineId());
                ordItem.setTransactionID(current.getTransactionId());
                ordItem.setPaidAmount(x.getPaidAmount());
                ordItem.setTransNumber(x.getTransNumber());
            }

            dbAdapter.insertSavedTLineEntity(existsTrans);

        } catch (Exception ex) {
            Logger.e("editCashedList ", ex.getMessage());
        }

    }

    @Override
    public void onSaveOrder(List<invoiceOrderModel> orderLists, long transactionID) {
        try {
            orderList = new ArrayList<>();
            orderList.addAll(orderLists);
        } catch (Exception ex) {
            Logger.e("onSaveOrder  catch  ", ex.getMessage());
        }
    }

    @Override
    public void onchangeRestTableStatus(restTableEntity entity, int status) {
        entity.tableStatus = status;
        dbAdapter.insertRestTables(entity);
    }

    @Override
    public void onRemoveOrderRow(invoiceOrderModel order) {
        try {
            dbAdapter.removeTableOrderEntity(order.Id);
            dbAdapter.removeTransLineEntity(order.getTranslineID());
            presenter.onRemovesucceed();
        } catch (Exception ex) {
            Logger.e("onRemoveOrderRow catch ", ex.getMessage());
        }
    }

    private void archiveSaveTransEntity(TransactionEntity transaction
            , List<TransLineEntity> translines, int itemCategoryId) {
        try {
            saveTransEntity objEntity;

            objEntity = dbAdapter.getSavedTransaction(
                    Integer.parseInt(transaction.getTransactionID().toString())
                    , transaction.getRestaurantTableID());

            if (objEntity == null)//insert new archive transaction
                objEntity = new saveTransEntity();

            objEntity.setCreatorUserID(transaction.getUserID());
            objEntity.setTransactionID(Integer.parseInt(transaction.getTransactionID() + ""));
            objEntity.setBusinessID(transaction.getBusinessID());
            objEntity.setRestaurantTableID(transaction.getRestaurantTableID());
            objEntity.setPaidAmount(0);
            objEntity.setArchived(true);

            if (transaction.getTransNumber() != null) {
                objEntity.setTransNumber(Integer.parseInt(transaction.getTransNumber().toString()));
                objEntity.setArchived(true);
            }

            objEntity.setTotalSum(0);
            objEntity.setTransDiscountAmount(transaction.getTransDiscountAmount());
            objEntity.setTransDiscountPercent(transaction.getTransDiscountPercent());


            for (TransLineEntity s : translines) {
                savedTLineEntity obj = new savedTLineEntity();
                obj.setTranslineId(Integer.parseInt(s.getTransLineID().toString()));
                obj.setItemCategoryId(itemCategoryId);
                obj.setItemId(s.getItemID());
                obj.setTransactionId(Integer.parseInt(s.getTransactionID() + ""));
                obj.setTollAmount(s.getTollAmount());
                obj.setTaxAmount(s.getTaxAmount());
                obj.setPrice(s.getPrice());
                obj.setnQty(Integer.parseInt(s.getnQty().toString()));
                obj.setAmount(s.getAmount());

                objEntity.translines.add(obj);
            }

            dbAdapter.insertSavedTransactions(objEntity);

        } catch (Exception ex) {

            Logger.e("archiveSaveTransEntity ", ex.getMessage());
        }
    }


    @Override
    public void onArchive(TransactionEntity transaction, List<TransLineEntity> translines, List<invoiceOrderModel> orders) {
        try {
            long inserted_Id;

            List<TransLineEntity> insertedTLines;

            if (transaction.getTransactionID() == 0) {//new archive record

                inserted_Id = dbAdapter.insertTransaction(transaction);

                for (TransLineEntity tr : translines) {
                    tr.setTransactionID(inserted_Id);
                }

                dbAdapter.insertTransLine(translines);

            } else { //edit archive record
                inserted_Id = transaction.getTransactionID();

                for (TransLineEntity tr : translines) {
                    tr.setTransactionID(inserted_Id);
                }

                dbAdapter.insertTransLine(translines);
            }

            insertedTLines = dbAdapter.getTranslines(inserted_Id);

            archiveSaveTransEntity(transaction, translines, orders.get(0).itemCategoryID);

            try {
                for (int i = 0; i < insertedTLines.size(); i++) {
                    orders.get(i).setTransactionID(inserted_Id);
                    orders.get(i).setTranslineID(Integer
                            .parseInt(insertedTLines.get(i)
                                    .getTransLineID().toString()));

                    orders.get(i).setArchived(true);
                }

                saveOrder(orders, inserted_Id, true);

            } catch (Exception ex) {
                Logger.e("insertedTLines.size() " + ex.getLocalizedMessage());
            }


            presenter.onArchiveSucceed();

        } catch (Exception ex) {
            presenter.onArchiveFailed(ex.getMessage());
        }
    }

    @Override
    public void onUpdateTableOrderEntity(long transactionId) {
       /* tableOrderEntity order = dbAdapter.findOrderById(transactionId);
        if (order != null) {
            // for (tableOrderEntity ord : orders) {
            order.setArchived(false);
            dbAdapter.insertOrder(order);
        }*/
    }
}
