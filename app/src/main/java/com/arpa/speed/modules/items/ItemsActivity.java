package com.arpa.speed.modules.items;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.print.PrintManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.arpa.speed.R;
import com.arpa.speed.boxDatabase.DatabaseAdapter;
import com.arpa.speed.boxDatabase.TransLineEntity;
import com.arpa.speed.boxDatabase.TransactionEntity;
import com.arpa.speed.boxDatabase.itemCatsEntity;
import com.arpa.speed.boxDatabase.itemsEntity;
import com.arpa.speed.boxDatabase.restTableEntity;
import com.arpa.speed.boxDatabase.saveTransEntity;
import com.arpa.speed.boxDatabase.tableOrderEntity;
import com.arpa.speed.customIconView.titleValueTextView;
import com.arpa.speed.models.order.invoiceOrderModel;
import com.arpa.speed.models.transaction.ObjectToSend;
import com.arpa.speed.models.transaction.transactionModel;
import com.arpa.speed.models.transaction.translineModel;
import com.arpa.speed.modules.itemCats.itemCatsAdapter;
import com.arpa.speed.modules.login.dbModel.userModel;
import com.arpa.speed.modules.order.dbModel.Model;
import com.arpa.speed.modules.order.orderAdapter;
import com.arpa.speed.modules.print.PrintAdapter;
import com.arpa.speed.utils.BaseActivity;
import com.arpa.speed.utils.BaseApplication;
import com.arpa.speed.utils.Constants;
import com.arpa.speed.utils.CustomTypefaceSpan;
import com.arpa.speed.utils.DividerItemDecoration;
import com.arpa.speed.utils.hawkHandler;
import com.arpa.speed.utils.publicUtils;
import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;
import com.orhanobut.logger.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.Disposable;


public class ItemsActivity extends BaseActivity implements
        itemContract.item_View, itemsAdapter.ItemClickListener
        , itemCatsAdapter.ItemClickListener, orderAdapter.ItemClickListener,
        NumericOrderFragment.counterDialogListener,
        SaveArchiveFragment.saveDialogListener {

    itemContract.item_Presenter presenter = new itemsPresenter(this);

    String TAG = ItemsActivity.class.getSimpleName();

    DatabaseAdapter databaseAdapter;
    CardView order_card, order_items_card, card_total_sum;
    restTableEntity selectedTable;
    List<itemCatsEntity> itemCatsList;
    itemCatsAdapter itemCatsAdapter;
    RecyclerView itemCatsRecyclerView, itemRecyclerView, orderRecyclerView;
    itemCatsEntity selectedItemCat;
    Toolbar order_toolbar;
    List<itemsEntity> itemsList;
    itemsAdapter itemsAdapter;
    itemsEntity selectedItem;
    orderAdapter orderAdapter;

    List<invoiceOrderModel> orderList;
    invoiceOrderModel orderModel, selectedOrder;

    titleValueTextView totalSum, totalCount, order_transNumber;
    hawkHandler hawkHandler;
    userModel u_dbModel;
    int user_id = 0;
    int sumOrderCount = 0;
    double sumOrderPrice = 0;
    boolean menuResult;
    transactionModel transactionModel;
    List<translineModel> translineLst;
    Integer itemCategoryID = 0;
    long transaction_Id = 0;
    int trans_Number = 0;
    MenuItem toolbar_menu_item;
    ActionBar itemActionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items);

        initialize();
        bindViews();

        getIntentInfo();

        setToolBar("");

        setItemCatsFragment();
        setItemsFragment();

        loadReservedTableInfo();
        setOrderFragment();

        registerForContextMenu(orderRecyclerView);
    }

    private void setToolBar(String transNumber) {
        setSupportActionBar(order_toolbar);
        itemActionBar = getSupportActionBar();

        String title = "";

        SpannableString tableName = new SpannableString(selectedTable.tableName);
        tableName.setSpan(new CustomTypefaceSpan(
                        "", BaseApplication.typeFace,
                        40,
                        ContextCompat.getColor(this, R.color.colorAccent)),
                0, selectedTable.tableName.length(),
                Spannable.SPAN_INCLUSIVE_INCLUSIVE);

       /* if (transNumber != null && transNumber != "0") {
            SpannableString transNum = new SpannableString(transNumber);
            transNum.setSpan(new CustomTypefaceSpan("", BaseApplication.typeFace, 40, getResources().getColor(R.color.Deep_Yellow)), 0, transNumber.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            title = String.format("%s  |  %s", transNum, tableName);
        } else {
            title = tableName.toString();
        }*/

        itemActionBar.setTitle(tableName);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        presenter.onCreateMenuToolbar(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        presenter.onSelectMenuToolbar(item);
        return menuResult;
    }

    private void initialize() {
        databaseAdapter = new DatabaseAdapter();
        orderList = new ArrayList<>();
        hawkHandler = com.arpa.speed.utils.hawkHandler.getInstance();
        u_dbModel = hawkHandler.getData(Constants.user_model);
        if (u_dbModel != null) {
            user_id = u_dbModel.getUserID();
        }

    }

    private void bindViews() {

        itemCatsRecyclerView = findViewById(R.id.itemCatsRecyclerView);
        itemRecyclerView = findViewById(R.id.itemRecyclerView);
        orderRecyclerView = findViewById(R.id.orderRecyclerView);
        totalSum = findViewById(R.id.totalSum);
        totalCount = findViewById(R.id.totalCount);
        order_transNumber = findViewById(R.id.order_transNumber);

        order_card = findViewById(R.id.order_card);
        order_card.setVisibility(View.GONE);

        card_total_sum = findViewById(R.id.card_total_sum);
        card_total_sum.setVisibility(View.GONE);

        order_items_card = findViewById(R.id.order_items_card);
        order_items_card.setVisibility(View.VISIBLE);

        order_toolbar = findViewById(R.id.order_toolbar);
    }

    private void setVisibility(int visibility) {

        if (visibility == 0) {
            order_items_card.setVisibility(View.GONE);
        } else {
            order_items_card.setVisibility(View.VISIBLE);
        }
        order_card.setVisibility(visibility);
        card_total_sum.setVisibility(visibility);

        /*if (trans_Number > 0)
            card_transNumber.setVisibility(visibility);*/


    }

    private void getIntentInfo() {
        try {
            Intent intent = getIntent();
            Bundle bundle = intent.getExtras();
            selectedTable =
                    (restTableEntity) bundle.getSerializable(Constants.item_Intent_args);


        } catch (Exception ex) {
            Logger.e(TAG, ex.getMessage());
        }

    }

    private void loadReservedTableInfo() {

        try {

            orderList = new ArrayList<>();

            List<tableOrderEntity> reservedList = databaseAdapter
                    .getOrderByTableId(selectedTable.restaurantTableID);
            // .getOrderByTableUserId(selectedTable.restaurantTableID,user_id);

            if (reservedList != null && reservedList.size() > 0) {

                tableOrderEntity current = reservedList.get(0);
                transaction_Id = current.transactionID;

                if (current.getTransNumber() > 0) {
                    trans_Number = current.getTransNumber();
                    setToolBar(trans_Number + "");
                    order_transNumber.setValueText(trans_Number + "");
                }

                setVisibility(View.VISIBLE);

                for (tableOrderEntity ord : reservedList) {

                    if (ord.itemCategoryID > 0)
                        itemCategoryID = ord.itemCategoryID;

                    orderModel = new invoiceOrderModel();

                    orderModel.setArchived(ord.isArchived());
                    orderModel.setId(ord.Id);
                    orderModel.setTransactionID(ord.transactionID);
                    orderModel.setTranslineID(ord.translineID);
                    orderModel.setRowSum(ord.rowSum);
                    orderModel.setItemCount(ord.itemCount);
                    orderModel.setItemID(ord.itemID);
                    orderModel.setTableID(ord.tableID);
                    orderModel.setItemName(ord.itemName);
                    orderModel.setSalePrice(ord.salePrice);
                    orderModel.setItemCategoryID(ord.itemCategoryID);
                    orderModel.setTransNumber(ord.getTransNumber());

                    sumOrderPrice += orderModel.getRowSum();
                    sumOrderCount += orderModel.getItemCount();

                    orderList.add(orderModel);
                }
            }
            presenter.onSetTotalSum(sumOrderPrice, sumOrderCount);
        } catch (Exception ex) {
            Logger.e(TAG + ex.getLocalizedMessage());
        }
    }

    private void setItemCatsFragment() {

        try {

            itemCatsList = databaseAdapter.getAllItemCats();

            int numberOfColumns = itemCatsList.size();

            if (numberOfColumns > 0) {
                itemCatsRecyclerView.setLayoutManager(new GridLayoutManager(this, this.getResources().getInteger(R.integer.number_of_grid_items)));

                itemCatsAdapter = new itemCatsAdapter(this, itemCatsList);
                itemCatsAdapter.setClickListener(this);
                itemCatsRecyclerView.setAdapter(itemCatsAdapter);
            }
           /* else {
                publicUtils.toast(String.format("%s%s", selectedTblCat.restaurantTableCategoryName, getString(R.string.no_data_exists)));
            }*/


        } catch (Exception ex) {
            Logger.e(TAG, ex.getMessage());
        }

    }

    @Override
    public void onItemCatClick(View view, int position) {
        selectedItemCat = itemCatsAdapter.getItem(position);
        setItemsFragment();
    }

    private void setItemsFragment() {
        if (itemCategoryID > 0 && selectedItemCat == null) {
            itemsList = databaseAdapter.getItemsByCategory(Long.parseLong(itemCategoryID.toString()));
        } else if (itemCatsList != null) {
            if (selectedItemCat != null) {
                itemsList = databaseAdapter.getItemsByCategory(selectedItemCat.itemCategoryID);
            } else {
                itemsList = databaseAdapter.getItemsByCategory(itemCatsList.get(0).itemCategoryID);
            }
        }

        if (itemsList != null) {

            int numberOfColumns = itemsList.size();

            if (numberOfColumns > 0) {
                itemRecyclerView.setLayoutManager(new GridLayoutManager(this, this.getResources().getInteger(R.integer.number_of_grid_items)));

                itemsAdapter = new itemsAdapter(this, itemsList);
                itemsAdapter.setClickListener(this);
                itemRecyclerView.setAdapter(itemsAdapter);
            } else {
                publicUtils.toast(String.format("%s , %s", selectedItemCat.itemCategoryName, getString(R.string.no_data_exists)));
            }
        }
    }

    @Override
    public void onItemsClick(View view, int position) {
        selectedItem = itemsAdapter.getItem(position);
        presenter.onAddOrderCount(selectedItem);

        setVisibility(View.VISIBLE);
    }

    @Override
    public void onAddOrderCount(itemsEntity item) {
        try {

            invoiceOrderModel model = fillOrderModel(selectedItem);

            boolean isExits = orderList.contains(model);

            int count = 1;
            double sumRowPrice;
            sumOrderCount += 1;

            if (isExits) {

                int index = publicUtils.memberIndex(orderList, model);

                count = orderList.get(index).itemCount + 1;
                sumRowPrice = count * selectedItem.salePrice;

                sumOrderPrice += selectedItem.salePrice;//sumRowPrice;

                model.setItemCount(count);
                // model.setnQty(BigDecimal.valueOf(count));

                model.setRowSum(sumRowPrice);
                // model.setAmount(sumRowPrice);
                model.setSalePrice(selectedItem.salePrice);

                model.setTotalCount(count);

                orderList.set(index, model);

                setOrderFragment();

            } else {

                sumRowPrice = count * selectedItem.salePrice;


                sumOrderPrice += sumRowPrice;

                //model.setnQty(BigDecimal.valueOf(count));
                model.setItemCount(count);
                model.setRowSum(sumRowPrice);
                model.setTotalCount(count);

                // model.setAmount(sumRowPrice);
                model.setSalePrice(selectedItem.salePrice);

                orderList.add(model);

                setOrderFragment();
            }

            presenter.onSetTotalSum(sumOrderPrice, sumOrderCount);

        } catch (Exception ex) {
            Logger.e(TAG, ex.getMessage());
        }
    }

    private void setOrderFragment() {
        try {
            if (orderList != null) {
                orderRecyclerView.setLayoutManager(new GridLayoutManager(this, 1));
                orderRecyclerView.addItemDecoration(new DividerItemDecoration(this));
                orderAdapter = new orderAdapter(this, orderList);
                orderAdapter.setClickListener(this);
                orderRecyclerView.setAdapter(orderAdapter);
            }

        } catch (Exception ex) {
            Logger.e(TAG, ex.getMessage());
        }
    }

    @Override
    public void onOrderClick(View view, int position) {
        selectedOrder = orderAdapter.getItem(position);
        presenter.onShowCountDialog(selectedOrder);
    }

    private invoiceOrderModel fillOrderModel(itemsEntity item) {
        orderModel = new invoiceOrderModel();
        orderModel.setItemID(Integer.parseInt(item.itemID + ""));
        orderModel.setItemCategoryID(item.itemCategoryID);
        orderModel.setItemName(item.itemName);
        orderModel.setItemCode(item.itemCode);
        orderModel.setSalePrice(item.salePrice);
        orderModel.setTableID(selectedTable.restaurantTableID);
        orderModel.setRowSum(orderModel.getItemCount() * item.salePrice);
        orderModel.setItemCount(0);
        orderModel.setTotalCount(0);
        orderModel.setUnitsRatio(0.0);

        if (transaction_Id > 0)
            orderModel.setTransactionID(transaction_Id);


        return orderModel;
    }

    @Override
    public void onShowCountDialog(invoiceOrderModel order) {
        showCountDialog(order);
    }

    @Override
    public void onCreateMenuToolbar(Menu menu) {
        getMenuInflater().inflate(R.menu.order_top_menu, menu);
        // Set an icon in the ActionBar

       /* menu.findItem(R.id.print_order).setIcon(
                new IconDrawable(this, FontAwesomeIcons.fa_print)
                        .colorRes(R.color.colorAccent)
                        .actionBarSize());*/

        menu.findItem(R.id.save_order).setIcon(
                new IconDrawable(this, FontAwesomeIcons.fa_save)
                        .colorRes(R.color.colorAccent)
                        //.sizeDp(12));
                        .actionBarSize());

        menu.findItem(R.id.expand_collapse).setIcon(
                new IconDrawable(this, FontAwesomeIcons.fa_arrow_up)
                        .colorRes(R.color.colorAccent)
                        // .sizeDp(12));
                        .actionBarSize());
    }

    private void showCountDialog(invoiceOrderModel order) {
        FragmentManager fm = getSupportFragmentManager();
        NumericOrderFragment numericOrderFragment = NumericOrderFragment.newInstance("Some Title", order);
        numericOrderFragment.show(fm, "number_picker_dialog");
    }

    @Override
    public void onFinishCountDialog(String operationType, String order_count, long itemID) {

        try {
            int count = Integer.parseInt(order_count);
            double sumRowPrice = selectedOrder.getRowSum();

            sumOrderCount = (sumOrderCount - selectedOrder.getItemCount()) + count;

            boolean isExits = orderList.contains(selectedOrder);

            if (isExits) { //update row

                int index = publicUtils.memberIndex(orderList, selectedOrder);

                if (operationType.equals(Constants.delete_order_row)) {
                    sumOrderCount = (sumOrderCount - selectedOrder.getItemCount());
                    sumOrderPrice = (sumOrderPrice - selectedOrder.getRowSum());

                    if (selectedOrder.Id > 0) {
                        //remove record locally
                        //orderList.remove(selectedOrder);
                        presenter.onRemoveOrderRow(selectedOrder);
                    }
                    orderList.remove(selectedOrder);

                } else {

                    sumRowPrice = count * selectedOrder.getSalePrice();

                    sumOrderPrice = (sumOrderPrice -
                            (selectedOrder.getSalePrice() * selectedOrder.getItemCount()))
                            + sumRowPrice;

                    selectedOrder.setItemCount(count);
                    selectedOrder.setRowSum(sumRowPrice);
                    selectedOrder.setTotalCount(count);

                    orderList.set(index, selectedOrder);
                }
                setOrderFragment();
            }
            presenter.onSetTotalSum(sumOrderPrice, sumOrderCount);


        } catch (Exception ex) {
            Logger.e(TAG, ex.getMessage());
        }
    }

    @Override
    public void onSetTotalSum(double sumRowPrice, int sumCount) {
        setSumTotal(sumRowPrice, sumCount);
    }

    private void setSumTotal(double sumRowPrice, int sumCount) {
        try {
            totalSum.setValueText(publicUtils.commaStringDouble(sumRowPrice));
            totalCount.setValueText(sumCount + "");
        } catch (Exception ex) {
            Logger.e(TAG, ex.getMessage());
        }
    }

    @Override
    public void onSelectMenuToolbar(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_order:
                item.setEnabled(false);
                toolbar_menu_item = item;
                //presenter.onShowSaveArchiveDialog();
                presenter.onSaveOrderClicked();
                menuResult = true;
                break;

          /*  case R.id.print_order:
                item.setEnabled(false);

                publicUtils.toast("print_order");
                menuResult = true;
                break;*/

            case R.id.expand_collapse:
                setExpandCollapse();
                menuResult = true;
                break;

            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                menuResult = true;
                break;
            default:
                menuResult = super.onOptionsItemSelected(item);
                break;
        }
    }

    private void setExpandCollapse() {

        //Animation slideUp = AnimationUtils.loadAnimation(this, R.anim.slide_up);

       /* if (itemRecyclerView.getVisibility() == View.GONE) {
            itemRecyclerView.setVisibility(View.VISIBLE);
            itemRecyclerView.startAnimation(slideUp);
        }*/


        if (itemRecyclerView.getVisibility() == View.VISIBLE) {
           /* TranslateAnimation animate = new TranslateAnimation(0, itemRecyclerView.getWidth(), 0, 0);
            animate.setDuration(500);
            animate.setFillAfter(true);*/
            //itemRecyclerView.startAnimation(animate);
            itemRecyclerView.setVisibility(View.GONE);
            itemCatsRecyclerView.setVisibility(View.GONE);

        } else if (itemRecyclerView.getVisibility() == View.GONE) {
            itemRecyclerView.setVisibility(View.VISIBLE);
            itemCatsRecyclerView.setVisibility(View.VISIBLE);
        }

        /*TranslateAnimation animateItemCats = new TranslateAnimation(0, itemCatsRecyclerView.getWidth(), 0, 0);
        animateItemCats.setDuration(400);

        if (itemCatsRecyclerView.getVisibility() == View.GONE) {
            animateItemCats.setFillAfter(true);
            itemCatsRecyclerView.startAnimation(animateItemCats);
            itemCatsRecyclerView.setVisibility(View.VISIBLE);
        } else {
            animateItemCats.setFillAfter(true);
            itemCatsRecyclerView.startAnimation(animateItemCats);
            itemCatsRecyclerView.setVisibility(View.GONE);
        }*/
    }

    @Override
    public void onSaveOrderFailed(String message) {
        publicUtils.toast(message);
        toolbar_menu_item.setEnabled(true);
        presenter.onHideProgress();

    }

    @Override
    public void onDispose(Disposable d) {

    }


    @Override
    public void onSaveOrderClicked() {
        if (publicUtils.isConnectedToInternet()) {
            sendToServer();
        } else {
            //publicUtils.toast(getString(R.string.check_internet_connection));
            toolbar_menu_item.setEnabled(true);
            presenter.onShowSaveArchiveDialog();
        }
    }

    private void sendToServer() {
        try {

            if (publicUtils.isConnectedToInternet()) {
                if (orderList.size() == 0 && transaction_Id == 0) {
                    publicUtils.toast(getString(R.string.no_item_to_save));
                    toolbar_menu_item.setEnabled(true);
                } else {
                    com.arpa.speed.models.userDefaults.Datum defaults = hawkHandler.getData(Constants.user_defaults);
                    int def_customerID = defaults.getDefCustomerID();

                    if (def_customerID > 0) {

                        transactionModel = new transactionModel();
                        transactionModel.businessID = def_customerID;
                        transactionModel.restaurantTableID = Integer.parseInt(selectedTable.restaurantTableID.toString());
                        transactionModel.userID = user_id;
                        transactionModel.sumOfCogs = 0;

                        if (orderList.get(0).isArchived())
                            transactionModel.transactionID = 0;
                        else transactionModel.transactionID = transaction_Id;

                        translineLst = new ArrayList<>();

                        for (invoiceOrderModel ord : orderList) {
                            translineModel ml = new translineModel();

                            if (ord.isArchived()) {
                                ml.setTransLineID(0);
                                ml.setTransactionID(0);

                            } else {
                                ml.setTransLineID(ord.translineID);
                                ml.setTransactionID(transaction_Id);
                            }

                            ml.setAmount(ord.rowSum);
                            ml.setDiscountAmount(0);
                            ml.setDiscountPercent(0);
                            ml.setItemID(ord.itemID);
                            ml.setnQty(BigDecimal.valueOf(ord.itemCount));
                            ml.setPrice(ord.salePrice);


                            translineLst.add(ml);
                        }

                        ObjectToSend objS = new ObjectToSend();
                        objS.data = transactionModel;
                        objS.items = translineLst;

                        presenter.onSave(objS, u_dbModel.getServerAddress());

                    } else {
                        publicUtils.toast(getString(R.string.select_default_customer_plz));
                    }
                }
            } else {
                publicUtils.toast(getString(R.string.check_internet_connection));
                toolbar_menu_item.setEnabled(true);
            }

        } catch (Exception ex) {
            if (ex.getLocalizedMessage() != null) {
                Logger.e(TAG, ex.getLocalizedMessage());
            }
            Logger.e(TAG, ex.getMessage());
        }

    }

    @Override
    public void onShowProgress() {
        dialog.setTitle(getString(R.string.sendint_to_server));
        dialog.show();
    }

    @Override
    public void onHideProgress() {
        dialog.hide();
    }

    @Override
    public void onSaveOrderSucceed(Model transactionDbModel) {
        presenter.onSaveTransactionResult(transactionDbModel);
        toolbar_menu_item.setEnabled(true);
    }

    @Override
    public void onSaveTransactionResultSucceed(transactionModel result) {

        for (invoiceOrderModel model : orderList) {
            model.setArchived(false);
            presenter.onUpdateTableOrderEntity(model.Id);
        }


        presenter.onSaveOrder(orderList, result.transactionID);
        presenter.onchangeRestTableStatus(selectedTable, Constants.reserved);
        toolbar_menu_item.setEnabled(true);
        //  publicUtils.toast(getString(R.string.order_saved));
    }

    @Override
    public void onRemovesucceed() {
        if (toolbar_menu_item != null)
            toolbar_menu_item.setEnabled(true);
    }

    @Override
    public void onSaveOrderSucceed() {
        sumOrderPrice = 0;
        sumOrderCount = 0;
        loadReservedTableInfo();
        toolbar_menu_item.setEnabled(true);
        doPrint();
    }

    private void doPrint() {
        //Printing framework works only api level 19 and above (android kitkat)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // Get a PrintManager instance
            PrintManager printManager = (PrintManager) this.getSystemService(Context.PRINT_SERVICE);

            // Set job name, which will be displayed in the print queue
            String jobName = this.getString(R.string.app_name) + " Document";

            // Start a print job, passing in a PrintDocumentAdapter implementation
            // to handle the generation of a print document
            printManager.print(jobName, new PrintAdapter(this, orderList),
                    null);

            // Save the job object for later status checking
            //mPrintJobs.add(printJob);

        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int position = -1;
        try {
            position = orderAdapter.getPosition();

        } catch (Exception e) {
            Logger.d(TAG, e.getLocalizedMessage(), e);
            return super.onContextItemSelected(item);
        }
        switch (item.getItemId()) {
            case 0:
                publicUtils.toast(item.getTitle().toString());
                break;
            case 1:
                publicUtils.toast(item.getTitle().toString());
                break;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onShowSaveArchiveDialog() {
        FragmentManager fm = getSupportFragmentManager();
        SaveArchiveFragment saveArchiveFragment = new SaveArchiveFragment();
        saveArchiveFragment.show(fm, "");
    }

    @Override
    public void OnArchiveOrderClicked() {
        try {

            if (orderList.size() == 0 && (transaction_Id == 0 || transaction_Id > 0)) {
                publicUtils.toast(getString(R.string.no_item_to_save));
                toolbar_menu_item.setEnabled(true);
            } else {
                com.arpa.speed.models.userDefaults.Datum defaults = hawkHandler.getData(Constants.user_defaults);
                int def_customerID = defaults.getDefCustomerID();

                if (def_customerID > 0) {

                    TransactionEntity objTrans = new TransactionEntity();

                    if (orderModel.getTransactionID() > 0) {
                        objTrans.setTransactionID(orderModel.getTransactionID());
                    } else {
                        objTrans.setTransactionID(new Long(0));
                    }

                    objTrans.setBusinessID(def_customerID);
                    objTrans.setUserID(user_id);
                    objTrans.setRestaurantTableID(Integer.parseInt(selectedTable.restaurantTableID.toString()));
                    objTrans.setTransDiscountPercent(0.0);
                    objTrans.setTransDiscountAmount(0.0);

                    List<TransLineEntity> objItems = new ArrayList<>();

                    for (invoiceOrderModel ord : orderList) {
                        TransLineEntity ml = new TransLineEntity();

                        ml.setTransLineID(Long.parseLong(ord.translineID + ""));
                        ml.setAmount(ord.rowSum);
                        ml.setTransactionID(ord.getTransactionID());
                        ml.setDiscountAmount(0);
                        ml.setDiscountPercent(0);
                        ml.setItemID(ord.itemID);
                        ml.setnQty(Long.parseLong(ord.itemCount + ""));
                        ml.setPrice(ord.salePrice);
                        ml.setTransactionID(transaction_Id);

                        objItems.add(ml);
                    }

                    presenter.onArchive(objTrans, objItems, orderList);
                } else {
                    publicUtils.toast(getString(R.string.check_internet_connection));
                }
            }
        } catch (Exception ex) {
            if (ex.getLocalizedMessage() != null) {
                Logger.e(TAG, ex.getLocalizedMessage());
            }
            Logger.e(TAG, ex.getMessage());
        }


    }

    @Override
    public void onArchiveSucceed() {
        publicUtils.toast(getString(R.string.saved_in_unsent_list));
    }

    @Override
    public void onArchiveFailed(String message) {
        Logger.e(TAG + "onArchiveFailed ", message);
        publicUtils.toast(message);
    }

    @Override
    public void onFinishSaveArchiveDialog(String operationType) {
        if (operationType == Constants.archive_order) {
            presenter.OnArchiveOrderClicked();

        }
       /* else {
          //  presenter.onSaveOrderClicked();
            orderList = new ArrayList<>();
           // orderRecyclerView.notify();
            //setOrderFragment();
        }*/


    }
}
