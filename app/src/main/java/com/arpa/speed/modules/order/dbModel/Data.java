
package com.arpa.speed.modules.order.dbModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("saleTransactionID")
    @Expose
    private String saleTransactionID;
    @SerializedName("paymentTransactionID")
    @Expose
    private String paymentTransactionID;
    @SerializedName("buildTransactionID")
    @Expose
    private String buildTransactionID;
    @SerializedName("saleTransactionNumber")
    @Expose
    private String saleTransactionNumber;
    @SerializedName("paymentTransactionNumber")
    @Expose
    private String paymentTransactionNumber;
    @SerializedName("buildTransactionNumber")
    @Expose
    private String buildTransactionNumber;

    public String getSaleTransactionID() {
        return saleTransactionID;
    }

    public void setSaleTransactionID(String saleTransactionID) {
        this.saleTransactionID = saleTransactionID;
    }

    public String getPaymentTransactionID() {
        return paymentTransactionID;
    }

    public void setPaymentTransactionID(String paymentTransactionID) {
        this.paymentTransactionID = paymentTransactionID;
    }

    public String getBuildTransactionID() {
        return buildTransactionID;
    }

    public void setBuildTransactionID(String buildTransactionID) {
        this.buildTransactionID = buildTransactionID;
    }

    public String getSaleTransactionNumber() {
        return saleTransactionNumber;
    }

    public void setSaleTransactionNumber(String saleTransactionNumber) {
        this.saleTransactionNumber = saleTransactionNumber;
    }

    public String getPaymentTransactionNumber() {
        return paymentTransactionNumber;
    }

    public void setPaymentTransactionNumber(String paymentTransactionNumber) {
        this.paymentTransactionNumber = paymentTransactionNumber;
    }

    public String getBuildTransactionNumber() {
        return buildTransactionNumber;
    }

    public void setBuildTransactionNumber(String buildTransactionNumber) {
        this.buildTransactionNumber = buildTransactionNumber;
    }

}
