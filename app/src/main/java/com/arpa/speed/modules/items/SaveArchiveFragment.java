package com.arpa.speed.modules.items;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arpa.speed.R;
import com.arpa.speed.customViews.customButton;
import com.arpa.speed.utils.Constants;

public class SaveArchiveFragment extends DialogFragment implements View.OnClickListener {

    private customButton cancel_button, archive_button;

    public SaveArchiveFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.custom_save_archive_dialog, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Get field from view

        //save_button = view.findViewById(R.id.save_button);
        cancel_button = view.findViewById(R.id.cancel_button);
        archive_button = view.findViewById(R.id.archive_button);

        cancel_button.setOnClickListener(this);
        archive_button.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.cancel_button) {
            saveDialogListener listener = (saveDialogListener) getActivity();
            listener.onFinishSaveArchiveDialog(Constants.cancel_order);
        }
        if (v.getId() == R.id.archive_button) {
            saveDialogListener listener = (saveDialogListener) getActivity();
            listener.onFinishSaveArchiveDialog(Constants.archive_order);
        }
        dismiss();
    }

    // 1. Defines the listener interface with a method passing back data result.
    public interface saveDialogListener {
        void onFinishSaveArchiveDialog(String operationType);
    }
}
