package com.arpa.speed.modules.syncData;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arpa.speed.R;
import com.arpa.speed.customIconView.titleValueEditText;
import com.arpa.speed.customIconView.titleValueTextView;
import com.arpa.speed.customViews.customButton;
import com.arpa.speed.modules.login.dbModel.userModel;
import com.arpa.speed.utils.Constants;

public class syncDataFragment extends DialogFragment implements View.OnClickListener {

    titleValueTextView sync_username, sync_server;
    titleValueEditText sync_password;

    customButton sync_yes, sync_no;

    public syncDataFragment() {
    }

    public static syncDataFragment newInstance(userModel userModel) {

        syncDataFragment frag = new syncDataFragment();
        Bundle args = new Bundle();

        args.putString("username", userModel.getUsername());
        args.putString("server", userModel.getServerAddress());

        frag.setArguments(args);
        return frag;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.custom_sync_dialog, container);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        sync_username = view.findViewById(R.id.sync_username);
        sync_server = view.findViewById(R.id.sync_server);
        sync_password = view.findViewById(R.id.sync_password);
        sync_yes = view.findViewById(R.id.sync_yes);
        sync_no = view.findViewById(R.id.sync_no);

        String username = getArguments().getString("username", "");
        sync_username.setValueText(username);

        String server = getArguments().getString("server", "");
        sync_server.setValueText(server);

        sync_yes.setOnClickListener(this);
        sync_no.setOnClickListener(this);
    }

    // 1. Defines the listener interface with a method passing back data result.
    public interface syncDialogListener {
        void onFinishSyncDialog(String operationType, String password);
    }

    @Override
    public void onClick(View v) {
        syncDialogListener listener = (syncDialogListener) getActivity();

        if (v.getId() == R.id.sync_yes) {
            listener.onFinishSyncDialog(Constants.sync_confirm, sync_password.getValueText() + "");
        }
        if (v.getId() == R.id.sync_no) {
            listener.onFinishSyncDialog(Constants.sync_cancel, sync_password.getValueText() + "");
        }

        dismiss();
    }

}
