package com.arpa.speed.modules.home;


import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;

import com.arpa.speed.boxDatabase.DatabaseAdapter;
import com.arpa.speed.boxDatabase.saveTransEntity;
import com.arpa.speed.boxDatabase.tableOrderEntity;
import com.arpa.speed.models.login.Data;
import com.arpa.speed.models.login.loginDbModel;
import com.arpa.speed.models.order.invoiceOrderModel;
import com.google.common.base.Predicates;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimaps;
import com.orhanobut.logger.Logger;

import java.security.acl.Group;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import io.reactivex.disposables.Disposable;

public class homeModel implements homeContracts.home_Model {

    String TAG = "homeModel --> homeContracts.home_Model -->  ";
    private homeContracts.home_Presenter presenter;
    Disposable disposable;
    DatabaseAdapter dbAdapter = new DatabaseAdapter();
    private Data userData;
    loginDbModel modell;
    Handler handler = new Handler(Looper.myLooper());

    @Override
    public void onAttachPresenter(homePresenter homePresenter) {
        this.presenter = homePresenter;
    }

    @Override
    public void onLoadArchiveCount() {
        try {

            List<tableOrderEntity> list  = new ArrayList<>();
            List<tableOrderEntity> unsentList = dbAdapter.getArchives();

            //remove duplicates
            for (tableOrderEntity ent : unsentList) {
                tableOrderEntity ordItem = Iterables.tryFind(list, obj -> (ent.getTransactionID()+"").equals(obj.getTransactionID()+"")).orNull();

                if(ordItem == null) {
                    list.add(ent);
                }
            }

            presenter.onArchiveCountSucceed(list);
        } catch (Exception ex) {
            Logger.e(TAG, ex.getLocalizedMessage() != null ? ex.getLocalizedMessage() : ex.getMessage());
            presenter.onArchiveCountFailed(ex.getMessage());
        }
    }

}
