package com.arpa.speed.boxDatabase;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class userEntity implements Serializable {

    @Id(assignable = true)
    public long id;
    String username;
    Integer userId;
}
