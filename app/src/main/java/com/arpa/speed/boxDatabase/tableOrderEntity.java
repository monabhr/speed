package com.arpa.speed.boxDatabase;

import java.io.Serializable;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.annotation.Uid;

@Entity
public class tableOrderEntity implements Serializable{

    @Id(assignable = true)
    public long Id;

    @Uid(5216557797043256942L)
    public long tableID;

    public long transactionID;
    public int translineID;
    public int transNumber;
    public int itemID;

    public String itemCode;
    public String itemName;
    public Integer itemCategoryID;
    public Double salePrice;
    public Double unitsRatio;
    public Double inverseUnitsRatio;
    public int itemCount;
    public Double rowSum;
    public Double totalSum;
    public int totalCount;
    public Double paidAmount;
    public int userId;
    public boolean isArchived;

    public boolean isArchived() {
        return isArchived;
    }

    public void setArchived(boolean archived) {
        isArchived = archived;
    }

    public int getTransNumber() {
        return transNumber;
    }

    public void setTransNumber(int transNumber) {
        this.transNumber = transNumber;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Double getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(Double paidAmount) {
        this.paidAmount = paidAmount;
    }

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public long getTableID() {
        return tableID;
    }

    public void setTableID(long tableID) {
        this.tableID = tableID;
    }

    public long getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(long transactionID) {
        this.transactionID = transactionID;
    }

    public int getTranslineID() {
        return translineID;
    }

    public void setTranslineID(int translineID) {
        this.translineID = translineID;
    }

    public int getItemID() {
        return itemID;
    }

    public void setItemID(int itemID) {
        this.itemID = itemID;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Integer getItemCategoryID() {
        return itemCategoryID;
    }

    public void setItemCategoryID(Integer itemCategoryID) {
        this.itemCategoryID = itemCategoryID;
    }

    public Double getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Double salePrice) {
        this.salePrice = salePrice;
    }

    public Double getUnitsRatio() {
        return unitsRatio;
    }

    public void setUnitsRatio(Double unitsRatio) {
        this.unitsRatio = unitsRatio;
    }

    public Double getInverseUnitsRatio() {
        return inverseUnitsRatio;
    }

    public void setInverseUnitsRatio(Double inverseUnitsRatio) {
        this.inverseUnitsRatio = inverseUnitsRatio;
    }

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    public Double getRowSum() {
        return rowSum;
    }

    public void setRowSum(Double rowSum) {
        this.rowSum = rowSum;
    }

    public Double getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(Double totalSum) {
        this.totalSum = totalSum;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (o.getClass() == this.getClass()) {
            long a = this.transactionID;
            long b = ((tableOrderEntity) o).transactionID;
            return a == b;
        }
        return false;
    }
}
