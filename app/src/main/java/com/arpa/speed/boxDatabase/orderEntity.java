package com.arpa.speed.boxDatabase;

import java.io.Serializable;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class orderEntity implements Serializable {

    @Id(assignable = true)
    public long id;
    public String saleTransactionID;
    public String paymentTransactionID;
    public String buildTransactionID;
    public String saleTransactionNumber;
    public String paymentTransactionNumber;
    public String buildTransactionNumber;



}
