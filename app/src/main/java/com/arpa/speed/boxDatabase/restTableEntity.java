package com.arpa.speed.boxDatabase;

import java.io.Serializable;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.annotation.Uid;

@Entity
public class restTableEntity implements Serializable{


    @Id(assignable = true)
    @Uid(6883584495139604790L)
    public Long restaurantTableID;
    public String tableName;
    public String tableCode;
    public Integer tableNumber;
    public Integer restaurantTableCategoryID;

    @Uid(5671362940157273919L)
    public int tableStatus;
}
