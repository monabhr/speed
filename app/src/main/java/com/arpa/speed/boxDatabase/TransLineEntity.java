package com.arpa.speed.boxDatabase;

import java.io.Serializable;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.annotation.Uid;

@Entity
public class TransLineEntity implements Serializable {
    @Id(assignable = true)
    public  Long transLineID;

    @Uid(7416747099965929215L)
    public int itemID;
    public Long transactionID;

    public Long nQty;
    public int stockAreaID;
    public double price;
    public double amount;
    public double discountAmount;
    public double discountPercent;
    public double taxAmount;
    public double tollAmount;


    public Long getTransLineID() {
        return transLineID;
    }

    public void setTransLineID(Long transLineID) {
        this.transLineID = transLineID;
    }

    public int getItemID() {
        return itemID;
    }

    public void setItemID(int itemID) {
        this.itemID = itemID;
    }

    public Long getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(Long transactionID) {
        this.transactionID = transactionID;
    }

    public Long getnQty() {
        return nQty;
    }

    public void setnQty(Long nQty) {
        this.nQty = nQty;
    }

    public int getStockAreaID() {
        return stockAreaID;
    }

    public void setStockAreaID(int stockAreaID) {
        this.stockAreaID = stockAreaID;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public double getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(double discountPercent) {
        this.discountPercent = discountPercent;
    }

    public double getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(double taxAmount) {
        this.taxAmount = taxAmount;
    }

    public double getTollAmount() {
        return tollAmount;
    }

    public void setTollAmount(double tollAmount) {
        this.tollAmount = tollAmount;
    }
}
