package com.arpa.speed.boxDatabase;

import java.io.Serializable;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.annotation.Uid;

@Entity
public class itemsEntity implements Serializable {


    @Id(assignable = true)
    @Uid(4085771935997760072L)
    public long  itemID;

    public String itemCode;

    public String itemName;

    public Integer itemCategoryID;

    @Uid(7706716885276117706L)
    public Double salePrice;

    @Uid(4654506067649974723L)
    public Double unitsRatio;

    @Uid(751807410142870312L)
    public Double inverseUnitsRatio;

}
