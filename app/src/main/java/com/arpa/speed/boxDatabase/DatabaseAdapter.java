package com.arpa.speed.boxDatabase;

import com.arpa.speed.models.order.invoiceOrderModel;
import com.arpa.speed.utils.BaseApplication;

import java.util.List;

import io.objectbox.BoxStore;
import io.objectbox.query.QueryBuilder;

public class DatabaseAdapter {

    private BoxStore boxStore;

    public DatabaseAdapter() {
        boxStore = BaseApplication.getApp().getBoxStore();
    }

    public void clearDatabase() {
        boxStore.boxFor(restTableCat.class).removeAll();
        boxStore.boxFor(restTableEntity.class).removeAll();

        boxStore.boxFor(itemCatsEntity.class).removeAll();
        boxStore.boxFor(itemsEntity.class).removeAll();

        boxStore.boxFor(businessEntity.class).removeAll();
        boxStore.boxFor(itemattachEntity.class).removeAll();

        boxStore.boxFor(saveTransEntity.class).removeAll();
        boxStore.boxFor(TransactionEntity.class).removeAll();

        boxStore.boxFor(TransLineEntity.class).removeAll();
        boxStore.boxFor(orderEntity.class).removeAll();
        //boxStore.boxFor(tableOrderEntity.class).removeAll();
        boxStore.boxFor(saveTransEntity.class).removeAll();
    }


    public void clearOrder(int transactionId) {

        List<tableOrderEntity> paidList = boxStore.boxFor(tableOrderEntity.class)
                .query()
                .equal(tableOrderEntity_.transactionID, transactionId)
                .build().find();

        for (tableOrderEntity order : paidList) {
            boxStore.boxFor(orderEntity.class).remove(order.Id);
        }


       /* boxStore.boxFor(TransactionEntity.class).removeAll();
        boxStore.boxFor(TransLineEntity.class).removeAll();
        boxStore.boxFor(orderEntity.class).removeAll();
        boxStore.boxFor(tableOrderEntity.class).removeAll();
        boxStore.boxFor(saveTransEntity.class).removeAll();*/
    }

    public void clearOrder(tableOrderEntity entity) {
        boxStore.boxFor(tableOrderEntity.class).remove(entity.Id);
    }


    public void clearSaveTransEntity(saveTransEntity entity) {
        boxStore.boxFor(saveTransEntity.class).remove(entity.savedTransId);
    }

    public void insertRestTableCategories(List<restTableCat> entities) {
        boxStore.boxFor(restTableCat.class).put(entities);
    }

    public void insertRestTables(List<restTableEntity> entities) {
        boxStore.boxFor(restTableEntity.class).put(entities);
    }

    public void insertRestTables(restTableEntity entity) {
        boxStore.boxFor(restTableEntity.class).put(entity);
    }

    public void insertItemCats(List<itemCatsEntity> entities) {
        boxStore.boxFor(itemCatsEntity.class).put(entities);
    }

    public void insertSavedTransactions(List<saveTransEntity> entities) {
        boxStore.boxFor(saveTransEntity.class).put(entities);
    }

    public void insertSavedTLineEntity(savedTLineEntity entity) {
        boxStore.boxFor(savedTLineEntity.class).put(entity);
    }


    public List<saveTransEntity> getSavedTransactions(int transactionId, int restaurantTableID) {
        return boxStore.boxFor(saveTransEntity.class)
                .query()
                .equal(saveTransEntity_.transactionID, transactionId)
                .equal(saveTransEntity_.restaurantTableID, restaurantTableID)
                .build().find();
    }

    public saveTransEntity getSavedTransaction(int transactionId, int restaurantTableID) {
        return boxStore.boxFor(saveTransEntity.class)
                .query()
                .equal(saveTransEntity_.transactionID, transactionId)
                .equal(saveTransEntity_.restaurantTableID, restaurantTableID)
                .build().findFirst();
    }

    public saveTransEntity getSavedTransactions(int restaurantTableID) {
        return boxStore.boxFor(saveTransEntity.class)
                .query()
                .equal(saveTransEntity_.restaurantTableID, restaurantTableID)
                .build().findFirst();
    }

    public List<saveTransEntity> getSavedTransactions() {
        return boxStore.boxFor(saveTransEntity.class).getAll();
    }

    //savedTLineEntity
    public savedTLineEntity getSavedTransLines(int translineId) {
        return boxStore.boxFor(savedTLineEntity.class)
                .query()
                .equal(savedTLineEntity_.translineId, translineId)
                //.equal(savedTLineEntity_.itemId, itemId)
                .build().findFirst();
    }

    public void insertSavedTransactions(saveTransEntity entity) {
        boxStore.boxFor(saveTransEntity.class).put(entity);
    }


    public savedTLineEntity getTranslineById(long transactionId, int itemId) {
        return boxStore.boxFor(savedTLineEntity.class)
                .query()
                .equal(savedTLineEntity_.transactionId, transactionId)
                .equal(savedTLineEntity_.itemId, itemId)
                .build().findFirst();
    }

    public List<TransLineEntity> getTranslines(long transactionId) {
        return boxStore.boxFor(TransLineEntity.class)
                .query()
                .equal(TransLineEntity_.transactionID, transactionId)
                .build().find();
    }

    public TransactionEntity getTransactionById(long transactionId) {
        return boxStore.boxFor(TransactionEntity.class)
                .query()
                .equal(TransactionEntity_.transactionID, transactionId)
                .build().findFirst();
    }

    public List<saveTransEntity> getUnsentOrders() {
        return boxStore.boxFor(saveTransEntity.class)
                .query()
                .equal(saveTransEntity_.isArchived, true)
                .build().find();
    }

    public List<tableOrderEntity> getArchives() {
        return boxStore.boxFor(tableOrderEntity.class)
                .query()
                .equal(tableOrderEntity_.isArchived, true)
                /*.filter((tableOrderEntity) -> {
                            return tableOrderEntity.isArchived = true;
                        }
                )*/
                .build().find();
        //.property(tableOrderEntity_.transactionID).distinct();
    }

    public List<tableOrderEntity> getArchivedOrders() {
        return boxStore.boxFor(tableOrderEntity.class)
                .query()
                .equal(tableOrderEntity_.isArchived, true)
                .build().find();
    }


    public int getLastInsertedSaved() {
        return boxStore.boxFor(saveTransEntity.class).getAll().size();
    }


    public List<saveTransEntity> getTransById(int transactionId) {
        return boxStore.boxFor(saveTransEntity.class)
                .query().equal(saveTransEntity_.transactionID, transactionId).build().find();
    }

    public void insertItems(List<itemsEntity> entities) {
        boxStore.boxFor(itemsEntity.class).put(entities);
    }

    public void insertBusiness(List<businessEntity> entities) {
        boxStore.boxFor(businessEntity.class).put(entities);
    }

    public void insertOrder(List<tableOrderEntity> entities) {
        boxStore.boxFor(tableOrderEntity.class).put(entities);
    }

    public tableOrderEntity findOrderById(long orderId) {
        return boxStore.boxFor(tableOrderEntity.class)
                .query().equal(tableOrderEntity_.Id, orderId).build().findFirst();
    }

    public List<tableOrderEntity> getOrders(long transactionId) {
        return boxStore.boxFor(tableOrderEntity.class)
                .query().equal(tableOrderEntity_.transactionID, transactionId).build().find();
    }


    public tableOrderEntity getOrders(long transactionId, int translineId) {
        return boxStore.boxFor(tableOrderEntity.class)
                .query().equal(tableOrderEntity_.transactionID, transactionId)
                .equal(tableOrderEntity_.translineID, translineId)
                .build().findFirst();
    }

    public void insertOrder(tableOrderEntity entity) {
        boxStore.boxFor(tableOrderEntity.class).put(entity);
    }


    public List<tableOrderEntity> getOrderByTableUserId(Long tableId, int userId) {
        return boxStore.boxFor(tableOrderEntity.class)
                .query()
                .equal(tableOrderEntity_.tableID, tableId)
                .equal(tableOrderEntity_.userId, userId)
                .build().find();
    }

    public tableOrderEntity getOrderByTableId(Long tableId, int translineId) {
        return boxStore.boxFor(tableOrderEntity.class)
                .query()
                .equal(tableOrderEntity_.tableID, tableId)
                .equal(tableOrderEntity_.translineID, translineId)
                //.equal(tableOrderEntity_., translineId)
                .build().findFirst();
    }

    public List<tableOrderEntity> getOrderByTableId(Long tableId) {
        return boxStore.boxFor(tableOrderEntity.class)
                .query()
                .equal(tableOrderEntity_.tableID, tableId)
                .build().find();
    }

    public tableOrderEntity getOrderByTransTableId(long transactionId, int translineID, int tableId) {
        return boxStore.boxFor(tableOrderEntity.class)
                .query()
                .equal(tableOrderEntity_.translineID, translineID)
                .equal(tableOrderEntity_.transactionID, transactionId)
                .equal(tableOrderEntity_.tableID, tableId)
                .build().findFirst();
    }

    public int getLastOrderId() {
        return boxStore.boxFor(tableOrderEntity.class).getAll().size();
    }

    public void removeTableOrderEntity(long Id) {
        boxStore.boxFor(tableOrderEntity.class).remove(Id);
    }

    public void removeTransLineEntity(long Id) {
        boxStore.boxFor(TransLineEntity.class).remove(Id);
    }

    public void removeTransactionEntity(long Id) {
        boxStore.boxFor(tableOrderEntity.class).remove(Id);
    }

    public List<restTableCat> getAllRestTableCategories() {
        return boxStore.boxFor(restTableCat.class).getAll();
    }

    public List<restTableEntity> getTablesByCategory(Long categoryID) {
        return boxStore.boxFor(restTableEntity.class)
                .query().equal(restTableEntity_.restaurantTableCategoryID, categoryID).build().find();
    }

    public restTableEntity getTablesById(Long tableID) {
        return boxStore.boxFor(restTableEntity.class)
                .query().equal(restTableEntity_.restaurantTableID, tableID).build().findFirst();
    }

    public String getTablesName(Long tableID) {
        return boxStore.boxFor(restTableEntity.class)
                .query().equal(restTableEntity_.restaurantTableID, tableID).build().findFirst().tableName;
    }

    public List<itemCatsEntity> getAllItemCats() {
        return boxStore.boxFor(itemCatsEntity.class).getAll();
    }

    public List<itemsEntity> getItemsByCategory(Long itemCategoryID) {
        return boxStore.boxFor(itemsEntity.class)
                .query().equal(itemsEntity_.itemCategoryID, itemCategoryID).build().find();
    }

    public List<itemsEntity> getAllItems() {
        return boxStore.boxFor(itemsEntity.class).getAll();
    }

    public Long getItemsNumberInCategory(Long itemCategoryID) {
        return boxStore.boxFor(itemsEntity.class)
                .query().equal(itemsEntity_.itemCategoryID, itemCategoryID).build().count();
    }

    public List<businessEntity> getAllBusinesses() {
        return boxStore.boxFor(businessEntity.class).query().build().find();
    }

    public businessEntity getBusinessById(Long businessId) {
        return boxStore.boxFor(businessEntity.class)
                .query().equal(businessEntity_.businessID, businessId).build().findFirst();
    }

    public long insertTransaction(TransactionEntity transactionEntity) {
        long inserted_Id = boxStore.boxFor(TransactionEntity.class)
                .put(transactionEntity);

        return inserted_Id;
    }

    public void insertTransactionResult(orderEntity orderEntity) {
        boxStore.boxFor(orderEntity.class).put(orderEntity);
    }

    public void insertTransLine(List<TransLineEntity> transLineEntity) {
        boxStore.boxFor(TransLineEntity.class).put(transLineEntity);
    }

    public void insertTransLine(TransLineEntity transLineEntity) {
        boxStore.boxFor(TransLineEntity.class).put(transLineEntity);
    }


    public TransactionEntity getTransactionByTableID(Long restaurantTableID) {
        return boxStore.boxFor(TransactionEntity.class)
                .query().equal(TransactionEntity_.restaurantTableID, restaurantTableID).build().findFirst();
    }

    public List<TransLineEntity> getTransLinesByTransactionID(Long transactionID) {
        return boxStore.boxFor(TransLineEntity.class)
                .query().equal(TransLineEntity_.transactionID, transactionID).build().find();
    }

    public itemsEntity getItemsByItemsID(Long itemID) {
        return boxStore.boxFor(itemsEntity.class)
                .query().equal(itemsEntity_.itemID, itemID).build().findFirst();
    }

}
