package com.arpa.speed.boxDatabase;

import com.arpa.speed.models.savedTrans.Transline;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.annotation.Uid;

@Entity
public class saveTransEntity implements Serializable {

    @Id(assignable = true)
    public long savedTransId;

    public Integer transactionID;
    public String transDate;
    public String businessName;
    public Integer transNumber;
    public Integer docAliasID;
    public Integer totalSum;
    public Integer creatorUserID;
    //@Uid(6480988451845371149L)
    public double transDiscountAmount;
    @Uid(7781290577514461186L)
    public double transDiscountPercent;
    public Integer businessID;

    @Uid(8365187672030629514L)
    public  int restaurantTableID;

    public double paidAmount;

    public boolean isArchived;

    public List<savedTLineEntity> translines;

    public boolean isArchived() {
        return isArchived;
    }

    public void setArchived(boolean archived) {
        isArchived = archived;
    }

    public long getSavedTransId() {
        return savedTransId;
    }

    public void setSavedTransId(long savedTransId) {
        this.savedTransId = savedTransId;
    }

    public Integer getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(Integer transactionID) {
        this.transactionID = transactionID;
    }

    public String getTransDate() {
        return transDate;
    }

    public void setTransDate(String transDate) {
        this.transDate = transDate;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public Integer getTransNumber() {
        return transNumber;
    }

    public void setTransNumber(Integer transNumber) {
        this.transNumber = transNumber;
    }

    public Integer getDocAliasID() {
        return docAliasID;
    }

    public void setDocAliasID(Integer docAliasID) {
        this.docAliasID = docAliasID;
    }

    public Integer getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(Integer totalSum) {
        this.totalSum = totalSum;
    }

    public Integer getCreatorUserID() {
        return creatorUserID;
    }

    public void setCreatorUserID(Integer creatorUserID) {
        this.creatorUserID = creatorUserID;
    }

    public double getTransDiscountAmount() {
        return transDiscountAmount;
    }

    public void setTransDiscountAmount(double transDiscountAmount) {
        this.transDiscountAmount = transDiscountAmount;
    }

    public double getTransDiscountPercent() {
        return transDiscountPercent;
    }

    public void setTransDiscountPercent(double transDiscountPercent) {
        this.transDiscountPercent = transDiscountPercent;
    }

    public Integer getBusinessID() {
        return businessID;
    }

    public void setBusinessID(Integer businessID) {
        this.businessID = businessID;
    }

    public int getRestaurantTableID() {
        return restaurantTableID;
    }

    public void setRestaurantTableID(int restaurantTableID) {
        this.restaurantTableID = restaurantTableID;
    }

    public double getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(double paidAmount) {
        this.paidAmount = paidAmount;
    }

    public List<savedTLineEntity> getTranslines() {
        return translines;
    }

    public void setTranslines(List<savedTLineEntity> translines) {
        this.translines = translines;
    }
}
