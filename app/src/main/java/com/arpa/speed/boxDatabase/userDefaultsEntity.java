package com.arpa.speed.boxDatabase;
import java.io.Serializable;

import io.objectbox.annotation.Id;

public class userDefaultsEntity implements Serializable {

    @Id(assignable = true)
    public  long Id;

    public   Integer defaultTransStateID;
    public   Integer defStockID;
    public   Integer defCustomerID;
}
