package com.arpa.speed.boxDatabase;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.annotation.Uid;

@Entity
public class itemCatsEntity implements Serializable{


    @Id(assignable = true)
    @Uid(4844992590361524667L)
    public long itemCategoryID;
    public String itemCategoryName;
    public String itemCategoryCode;

}
