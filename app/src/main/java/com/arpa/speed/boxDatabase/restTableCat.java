package com.arpa.speed.boxDatabase;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.annotation.Uid;

@Entity
public class restTableCat implements Serializable {


    @Id(assignable = true)
    @Uid(1723211285073675164L)
     public Long restaurantTableCategoryID;
    public String restaurantTableCategoryCode;
    public String restaurantTableCategoryName;
    public String descriptions;
}
