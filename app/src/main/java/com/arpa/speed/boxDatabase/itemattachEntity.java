package com.arpa.speed.boxDatabase;
import java.io.Serializable;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class itemattachEntity implements Serializable {
    @Id(assignable = true)
    public long Id;

    public String attachment;
    public Integer itemID;
}
