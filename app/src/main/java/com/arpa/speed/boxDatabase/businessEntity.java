package com.arpa.speed.boxDatabase;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.annotation.Uid;

@Entity
public class businessEntity implements Serializable{

    @Id(assignable = true)
    @Uid(2441435250929865223L)
    public long businessID;

    public String businessCode;
    public String businessName;
    public String address;
    public String mobile;
    public String phoneNo;
    public Integer lat;
    public Integer _long;
    public Integer geoRegionID;
    public Boolean inActive;
}
