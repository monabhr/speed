package com.arpa.speed.boxDatabase;

import java.io.Serializable;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.annotation.Uid;

@Entity
public class TransactionEntity implements Serializable {
    @Id(assignable = true)
    @Uid(3785954946332710586L)
    public  Long transactionID;

    public  Long transNumber;

    @Uid(8618419996126002323L)
    public  int userID;

    @Uid(8049808483109422114L)
    public  int businessID;
    public Double transDiscountAmount;
    public Double transDiscountPercent;
    //public Double numberOfItems;
    //public Double subTotalAmount;
    public Double taxAndTollSum;
    public Double totalAmount;

    @Uid(4767081540993918574L)
    public  int restaurantTableID;
    public  Double PaidAmount;

    public Long getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(Long transactionID) {
        this.transactionID = transactionID;
    }

    public Long getTransNumber() {
        return transNumber;
    }

    public void setTransNumber(Long transNumber) {
        this.transNumber = transNumber;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getBusinessID() {
        return businessID;
    }

    public void setBusinessID(int businessID) {
        this.businessID = businessID;
    }

    public Double getTransDiscountAmount() {
        return transDiscountAmount;
    }

    public void setTransDiscountAmount(Double transDiscountAmount) {
        this.transDiscountAmount = transDiscountAmount;
    }

    public Double getTransDiscountPercent() {
        return transDiscountPercent;
    }

    public void setTransDiscountPercent(Double transDiscountPercent) {
        this.transDiscountPercent = transDiscountPercent;
    }

    public Double getTaxAndTollSum() {
        return taxAndTollSum;
    }

    public void setTaxAndTollSum(Double taxAndTollSum) {
        this.taxAndTollSum = taxAndTollSum;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public int getRestaurantTableID() {
        return restaurantTableID;
    }

    public void setRestaurantTableID(int restaurantTableID) {
        this.restaurantTableID = restaurantTableID;
    }

    public Double getPaidAmount() {
        return PaidAmount;
    }

    public void setPaidAmount(Double paidAmount) {
        PaidAmount = paidAmount;
    }
}
