package com.arpa.speed.customIconView;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.arpa.speed.R;
import com.arpa.speed.customViews.customEditText;
import com.arpa.speed.customViews.customTextView;
import com.arpa.speed.utils.Constants;

public class titleValueEditText extends RelativeLayout {

    customEditText editValue;
    customTextView titleTText;
    float size = 7, etSize = 3;

    public titleValueEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        defineViews(context, attrs);
    }

    private void defineViews(Context context, AttributeSet attrs) {

        View view = LayoutInflater.from(context)
                .inflate(R.layout.title_value_edittext, this, true);

        titleTText = view.findViewById(R.id.editText);
        editValue = view.findViewById(R.id.editValue);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.titleValueEditText);

        int textColor = typedArray.getInt(R.styleable.titleValueEditText_eColor, R.color.black);
        //float dimension = typedArray.getDimension(R.styleable.titleValueEditText_eSize, 10);
        size = typedArray.getDimension(R.styleable.titleValueEditText_eSize, 10);
        etSize = typedArray.getDimension(R.styleable.titleValueEditText_etSize, 3);
        int inputType = typedArray.getInt(R.styleable.titleValueEditText_eInputType, 2);


        if (inputType != 0 ) {
            switch (inputType) {

                case 1:
                    editValue.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    break;

                case 2:
                    editValue.setInputType(InputType.TYPE_CLASS_TEXT);
                    break;

                case 3:
                    editValue.setInputType(InputType.TYPE_CLASS_NUMBER);
                    break;
            }
        }


        String tTitle_sty = typedArray.getString(
                R.styleable.titleValueEditText_eTitle
        );

        if (tTitle_sty != null) {
            titleTText.setText(tTitle_sty);
            titleTText.setTextColor(textColor);
            titleTText.setTextSize(size);
        }

        String titleValue_sty = typedArray.getString(
                R.styleable.titleValueEditText_eValue
        );

        if (titleValue_sty != null) {
            setValueText(titleValue_sty);
            editValue.setSelection(editValue.text().length());
            editValue.setTextColor(textColor);
            //editValue.setTextSize(etSize);
        }

        int typeFaceNumber = 1;
        if (typedArray.hasValue(R.styleable.titleValueEditText_eFont)) {
            typeFaceNumber = typedArray.getInt(R.styleable.titleValueEditText_eFont, 1);
        }

        Typeface yekan = Typeface.createFromAsset(
                context.getAssets(), Constants.yekanFontName
        );

        Typeface iranSans = Typeface.createFromAsset(
                context.getAssets(), Constants.appFontName
        );

        if (typeFaceNumber == 1)
            editValue.setTypeface(iranSans);
        if (typeFaceNumber == 2)
            editValue.setTypeface(yekan);




    }

    public void setTitleText(String value) {
        titleTText.setText(value);
    }

    public String getTitleText() {
        return titleTText.getText().toString();
    }

    public void setValueText(String value) {
        editValue.setText(value);
        editValue.setTextSize(etSize);

    }

    public String getValueText() {
        return editValue.getText().toString();
    }

}
