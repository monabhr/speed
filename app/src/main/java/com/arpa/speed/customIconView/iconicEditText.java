package com.arpa.speed.customIconView;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.arpa.speed.R;
import com.arpa.speed.customViews.customEditText;
import com.arpa.speed.customViews.customImageView;
import com.arpa.speed.utils.Constants;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class iconicEditText extends RelativeLayout {
    customEditText iconText;
    customImageView iconPic;

    public iconicEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        View view = LayoutInflater.from(context)
                .inflate(R.layout.icon_edittext, this, true);

        iconText = findViewById(R.id.iconText);
        iconPic = findViewById(R.id.iconPic);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.iconicEditText);

        Drawable drawable = typedArray.getDrawable(R.styleable.iconicEditText_hintIcon);
        //String hintIcon = typedArray.getString(R.styleable.iconicEditText_hintIcon);
        String hintText = typedArray.getString(R.styleable.iconicEditText_hintText);
        int inputType = typedArray.getInt(R.styleable.iconicEditText_inputType_,2);
        int textColor = typedArray.getInt(R.styleable.iconicEditText_textColor, R.color.black);
        float dimension = typedArray.getDimension(R.styleable.iconicEditText_textSize_, 9);


        if (drawable != null) {
            iconPic.setBackground(drawable);
            //iconPic.loadIamge(hintIcon);
        }

        if (hintText != null) {
            iconText.setHint(hintText);
        }

        if(inputType != 0)
        {
            switch (inputType){

                case 1:
                    iconText.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    break;

                case 2:
                    iconText.setInputType(InputType.TYPE_CLASS_TEXT);
                    break;

                case 3:
                    iconText.setInputType(InputType.TYPE_CLASS_NUMBER);
                    break;
            }


        }

        String textValue = typedArray.getString(
                R.styleable.iconicEditText_textValue
        );


        if (textValue != null) {
            iconText.setText(textValue);
            //if (textColor) {
            iconText.setTextColor(textColor);
            iconText.setTextSize(dimension);


        }


        int typeFaceNumber = 1;
        //font
        if (typedArray.hasValue(R.styleable.iconicEditText_textFont)) {
            typeFaceNumber = typedArray.getInt(R.styleable.iconicEditText_textFont, 1);
        }

        Typeface yekan = Typeface.createFromAsset(
                context.getAssets(), Constants.yekanFontName
        );

        Typeface iranSans = Typeface.createFromAsset(
                context.getAssets(), Constants.appFontName
        );

        if (typeFaceNumber == 1)
            iconText.setTypeface(iranSans);
        if (typeFaceNumber == 2)
            iconText.setTypeface(yekan);


    }

    public void setText(String value) {
        iconText.setText(value);
    }

    public String getText() {
        return iconText.text();
    }

    public void setIcon(String url) {
        iconPic.loadIamge(url);
    }

    public boolean isValid(ValidationType type) {
        String value = iconText.getText().toString();

        if (value.length() > 0)
            if (type == ValidationType.EMAIL) {
                return isEmail(value);

            } else if (type == ValidationType.MOBILE) {

            } else if (type == ValidationType.WEBSITE) {

            }
        return false;
    }

    enum ValidationType {
        EMAIL, MOBILE, WEBSITE;
    }


    boolean isEmail(String txt) {
        String regex = "^([_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*(\\.[a-zA-Z]{1,6}))?$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(txt);
        if (!matcher.matches()) {
            return false;
        } else
            return true;
    }
}
