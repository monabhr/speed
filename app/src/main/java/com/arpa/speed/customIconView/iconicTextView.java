package com.arpa.speed.customIconView;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.arpa.speed.R;
import com.arpa.speed.customViews.customEditText;
import com.arpa.speed.customViews.customImageView;
import com.arpa.speed.customViews.customTextView;
import com.arpa.speed.utils.Constants;

public class iconicTextView extends RelativeLayout {

    customTextView iconText;
    customImageView iconPic;

    public iconicTextView(Context context, AttributeSet attrs) {
        super(context, attrs);


        View view = LayoutInflater.from(context)
                .inflate(R.layout.icon_textview, this, true);

        iconText = findViewById(R.id.iconText);
        iconPic = findViewById(R.id.iconPic);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.iconicTextView);

        Drawable drawable = typedArray.getDrawable(R.styleable.iconicTextView_viewIcon);
        //String hintIcon = typedArray.getString(R.styleable.iconicEditText_hintIcon);
        //String hintText = typedArray.getString(R.styleable.iconicTextView_viewValue);

        if (drawable != null) {
            iconPic.setBackground(drawable);
            //iconPic.loadIamge(hintIcon);
        }

        String textValue = typedArray.getString(
                R.styleable.iconicTextView_viewValue
        );
        if (textValue != null) {
            iconText.setText(textValue);
        }

        int typeFaceNumber = 1;
        if (typedArray.hasValue(R.styleable.iconicTextView_viewFont)) {
            typeFaceNumber = typedArray.getInt(R.styleable.iconicTextView_viewFont, 1);
        }

        Typeface yekan = Typeface.createFromAsset(
                context.getAssets(), Constants.yekanFontName
        );

        Typeface iranSans = Typeface.createFromAsset(
                context.getAssets(), Constants.appFontName
        );

        if (typeFaceNumber == 1)
            iconText.setTypeface(iranSans);
        if (typeFaceNumber == 2)
            iconText.setTypeface(yekan);

    }

    public void setText(String value) {
        iconText.setText(value);
    }

    public String getText() {
        return iconText.getText().toString();
    }

    public void setIcon(String url) {
        iconPic.loadIamge(url);
    }
}
