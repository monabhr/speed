package com.arpa.speed.customIconView;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.arpa.speed.R;
import com.arpa.speed.customViews.customTextView;
import com.arpa.speed.utils.Constants;

public class titleValueTextView extends RelativeLayout {

    customTextView titleText, titleValue;
    int tGravity, vGravity;

    public titleValueTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        defineViews(context, attrs);
    }

    private void defineViews(Context context, AttributeSet attrs) {

        View view = LayoutInflater.from(context)
                .inflate(R.layout.title_value_textview, this, true);


        titleText = findViewById(R.id.titleText);
        titleValue = findViewById(R.id.titleValue);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.titleValueTextView);

        int textColor = typedArray.getInt(R.styleable.titleValueTextView_tColor, R.color.black);
        float dimension = typedArray.getDimension(R.styleable.titleValueTextView_tSize, 10);
        //tGravity = typedArray.getInt(R.styleable.titleValueTextView_tGravity, 5);
        //vGravity = typedArray.getInt(R.styleable.titleValueTextView_vGravity, 3);


        String tTitle_sty = typedArray.getString(
                R.styleable.titleValueTextView_tTitle
        );

        if (tTitle_sty != null) {
            titleText.setText(tTitle_sty);
            titleText.setTextColor(textColor);
            titleText.setTextSize(dimension);
           // titleText.setGravity(tGravity);
        }

        String titleValue_sty = typedArray.getString(
                R.styleable.titleValueTextView_tValue
        );

        if (titleValue_sty != null) {
            titleValue.setText(titleValue_sty);
            titleValue.setTextColor(textColor);
            titleValue.setTextSize(dimension);
           // titleValue.setGravity(vGravity);

        }

        int typeFaceNumber = 1;
        if (typedArray.hasValue(R.styleable.titleValueTextView_tFont)) {
            typeFaceNumber = typedArray.getInt(R.styleable.titleValueTextView_tFont, 1);
        }

        Typeface yekan = Typeface.createFromAsset(
                context.getAssets(), Constants.yekanFontName
        );

        Typeface iranSans = Typeface.createFromAsset(
                context.getAssets(), Constants.appFontName
        );

        if (typeFaceNumber == 1)
            titleValue.setTypeface(iranSans);
        if (typeFaceNumber == 2)
            titleValue.setTypeface(yekan);


    }

    public void setTitleText(String value) {
        titleText.setText(value);
        //titleText.setGravity(tGravity);

    }

    public String getTitleText() {
        return titleText.getText().toString();
    }

    public void setValueText(String value) {
        titleValue.setText(value);
       // titleValue.setGravity(vGravity);
    }

    public String getValueText() {
        return titleValue.getText().toString();
    }
}
