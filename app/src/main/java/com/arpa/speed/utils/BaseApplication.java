package com.arpa.speed.utils;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.content.ContextCompat;

import com.arpa.speed.BuildConfig;
import com.arpa.speed.boxDatabase.MyObjectBox;
import com.arpa.speed.services.checkPaid.checkPaidService;
import com.crashlytics.android.Crashlytics;
import com.joanzapata.iconify.Iconify;
import com.joanzapata.iconify.fonts.FontAwesomeModule;
import com.orhanobut.hawk.Hawk;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

import io.fabric.sdk.android.Fabric;
import io.objectbox.BoxStore;

public class BaseApplication extends Application {

    private static BaseApplication baseApp;
    //public static Contex mContex;
    public static Typeface typeFace;
    private static BoxStore boxStore;

    @Override
    public void onCreate() {
        super.onCreate();

        baseApp = this;

        typeFace = Typeface.createFromAsset(getAssets(), Constants.appFontName);

        boxStore = MyObjectBox.builder().androidContext(baseApp).build();

        Fabric.with(this, new Crashlytics());

        Iconify.with(new FontAwesomeModule());
      /*  if (BuildConfig.DEBUG) {
            AndroidObjectBrowser(boxStore).start(this);
        }*/

        Hawk.init(this).build();

        Logger.addLogAdapter(new AndroidLogAdapter() {
            @Override
            public boolean isLoggable(int priority, String tag) {
                return true;//BuildConfig.DEBUG;
            }
        });

        //doBindService();
    }
    /*public void doBindService() {

        Intent intent = new Intent(baseApp, checkPaidService.class);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            ContextCompat.startForegroundService(getBaseContext(), intent);
        } else {
            startService(intent);
        }


    }
*/

    public static BaseApplication getApp() {
        return baseApp;
    }

    public BoxStore getBoxStore() {
        return boxStore;
    }
}
