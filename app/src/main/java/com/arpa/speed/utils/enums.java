package com.arpa.speed.utils;

public enum enums {

    Reserved("Reserved", 1),
    Paid("Paid", 2);

    private String stringValue;
    private int intValue;
    private enums(String toString, int value) {
        stringValue = toString;
        intValue = value;
    }

    @Override
    public String toString() {
        return stringValue;
    }



}
