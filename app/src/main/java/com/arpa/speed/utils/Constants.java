package com.arpa.speed.utils;

import com.arpa.speed.R;

public class Constants {

    public static String yekanFontName = "fonts/Yekan.ttf";
    public static String appFontName = "fonts/MobileFonts/IRANSansMobile.ttf";

    private static String serverPrefix = "/serv/AppReport/";
    private static String tokenPrefix = "/serv/Token/";

    public static String loginAddress = tokenPrefix + "GetPingServerToken";

    public static String RestTableCatAddress = serverPrefix + "GetRestTableCategories";
    public static String RestTableAddress = serverPrefix + "GetRestTable";

    public static String itemsAddress = serverPrefix + "GetItems";
    public static String itemCatsAddress = serverPrefix + "GetItemCats";
    public static String itemAttachmentAddress = serverPrefix + "GetItemAttachment";

    public static String BusinessAddress = serverPrefix + "GetBusiness";
    public static String defaultsAddress = serverPrefix + "GetDefaults";
    public static String transListAAddress = serverPrefix + "GetTransactions";
    public static String transPostAAddress = serverPrefix + "PostTransaction";


    public static String user_model = "user";
    public static String user_defaults = "user_defaults";
    public static String item_Intent_args = "item_intent_args";
    public static String delete_order_row = "delete_order_row";
    public static String edit_order_row = "edit_order_row";
    public static String sync_confirm = "sync_confirm";
    public static String sync_cancel = "sync_cancel";
    public static String save_order = "save_order";
    public static String cancel_order = "cancel_order";
    public static String archive_order = "archive_order";


    public static String failed_to_connect_to = "failed to connect to";
    public static String  connection_failed = BaseApplication.getApp().getString(R.string.connection_failed);

    public static int reserved = 1;
    public static int paid = 2;




}
