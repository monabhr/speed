package com.arpa.speed.utils;

import java.io.Serializable;

public class GeneralSetting implements Serializable {
    private boolean isLoggedIn;
    private Long businessId, defaultTransStateID, defStockID;

    public GeneralSetting() {
        isLoggedIn = false;
        businessId = 0L;
        defaultTransStateID = 0L;
        defStockID = 0L;
    }

    public GeneralSetting(boolean isLoggedIn) {
        this.isLoggedIn = isLoggedIn;
    }

    public static GeneralSetting getInstance() {
        GeneralSetting generalSetting;
        hawkHandler handler = hawkHandler.getInstance();
        if (handler.getData(Constants.user_model) instanceof GeneralSetting) {
            generalSetting = (GeneralSetting) handler
                    .getData(Constants.user_model);
        } else {
            generalSetting = new GeneralSetting();
        }

        return generalSetting;
    }

    private static transient hawkHandler hawkHandler;
    private void saveValues() {
        if (hawkHandler == null) {
            hawkHandler = hawkHandler.getInstance();
        }
        hawkHandler.saveData(Constants.user_model,this);
    }

    public void clearValues(){
        new GeneralSetting().saveValues();
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        isLoggedIn = loggedIn;
        saveValues();
    }

    public Long getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Long businessId) {
        this.businessId = businessId;
        saveValues();
    }

    public Long getDefaultTransStateID() {
        return defaultTransStateID;

    }

    public void setDefaultTransStateID(Long defaultTransStateID) {
        this.defaultTransStateID = defaultTransStateID;
        saveValues();
    }

    public Long getDefStockID() {
        return defStockID;
    }

    public void setDefStockID(Long defStockID) {
        this.defStockID = defStockID;
        saveValues();
    }
}
