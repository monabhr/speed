package com.arpa.speed.utils;

import com.orhanobut.hawk.Hawk;

public class hawkHandler {

    private static hawkHandler handler;

    public static <T> void saveData(String key, T value) {
        Hawk.put(key, value);
    }

    public static <T> T getData(String key) {
        return Hawk.get(key);
    }

    public static void clearData(String key) {
        Hawk.delete(key);
    }

    public static void clearAll() {
        Hawk.deleteAll();
    }

    public static boolean containsKey(String key) {
        return Hawk.contains(key);
    }

    public static hawkHandler getInstance() {
        if (handler == null) {
            handler = new hawkHandler();
        }
        return handler;
    }
}
