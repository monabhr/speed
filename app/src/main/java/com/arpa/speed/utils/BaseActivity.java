package com.arpa.speed.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.arpa.speed.R;
import com.google.firebase.analytics.FirebaseAnalytics;

public class BaseActivity extends AppCompatActivity {

    private FirebaseAnalytics mFirebaseAnalytics;
    public Context mContext;
    public Activity mActivity;
    public ProgressDialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;
        mActivity = this;


        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        dialog = new ProgressDialog(mContext);

        dialog.setMessage(getString(R.string.plz_wait));
        //dialog.setMessage(getString(R.string.plz_wait));

        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

        dialog.setIndeterminate(false);

/*//set the max value for the dialog
        dialog.setMax(fileTableCursor.getCount());
        for (int i = 0; i < fileTableCursor.getCount(); i++) {
            //set the progress
            dialog.setProgress(i + 1);*/
        //}
    }


}
