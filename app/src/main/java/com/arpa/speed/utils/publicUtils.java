package com.arpa.speed.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.DisplayMetrics;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

public class publicUtils {

    private static char ArabicYeChar = (char) 1610;
    private static char PersianYeChar = (char) 1740;

    private static char ArabicKeChar = (char) 1603;
    private static char PersianKeChar = (char) 1705;

    public static String ArabicStringToPersian(String value) {
       /* for (int i = 0; i < value.length(); i++)
        {
            char c = value.charAt(i);
            int valuee = c;
        }*/

        return value.replace(ArabicYeChar, PersianYeChar)
                .replace(ArabicKeChar, PersianKeChar);
    }

    public static String PersianStringToArabic(String value) {
        return value.replace(PersianYeChar, ArabicYeChar);
        //.replace(PersianKeChar, ArabicKeChar);

    }

    public static String arabicToDecimal(String number) {
        if (number != null && number != "") {
            char[] chars = new char[number.length()];
            for (int i = 0; i < number.length(); i++) {
                char ch = number.charAt(i);
                if (ch >= 0x0660 && ch <= 0x0669)
                    ch -= 0x0660 - '0';
                else if (ch >= 0x06f0 && ch <= 0x06F9)
                    ch -= 0x06f0 - '0';
                chars[i] = ch;
            }
            return new String(chars);
        } else
            return "";
    }

    public static void toast(String msg) {
        Toast.makeText(BaseApplication.getApp(), msg, Toast.LENGTH_SHORT).show();
    }

    public static String formatCommaStringNumber(String number) {
        if (number == null || number == "")
            return number;
        try {
            return String.format("%,d", Long.parseLong(number));
        } catch (Exception ex) {
            return number;
        }

    }

    public static String commaStringDouble(Double number) {
        try {
            NumberFormat nf = new DecimalFormat("#.####");
            String formattedPrice = nf.format(number);

            return String.format("%,d", Long.parseLong(formattedPrice));

        } catch (Exception ex) {
            return number.toString();
        }

    }

    public static <T> int memberIndex(List<T> members, T order) {
        int index = members.indexOf(order);
        if (index < 0) { // person is not a current member
            index = members.size();
            //members.add(order);
        }
        return index;
    }

    public static void freeMemory() {
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
    }

    public static int calculateNoOfColumns(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        int noOfColumns = (int) (dpWidth / 180);
        return noOfColumns;
    }

    public static boolean isConnectedToInternet(){

        Context ctx=BaseApplication.getApp();

        ConnectivityManager connectivityManager = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();

       /* if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        }
        else
            connected = false;*/
    }
}
