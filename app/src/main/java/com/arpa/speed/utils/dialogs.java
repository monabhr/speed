package com.arpa.speed.utils;

import android.app.ProgressDialog;

public class dialogs {

    public  void showProgressDialog(ProgressDialog dialog, int count, String title, String message) {
        //set the max value for the dialog

       // dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        //dialog.setIndeterminate(false);
        dialog.setTitle(title);
        dialog.setMax(count);
        for (int i = 0; i < count; i++) {
            //set the progress
            dialog.setProgress(i + 1);
        }

      //  dialog.show();
    }

    public  void hideProgressDialog(ProgressDialog dialog) {
        dialog.hide();
    }

}
