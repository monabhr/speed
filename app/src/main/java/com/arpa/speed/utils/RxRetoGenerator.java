package com.arpa.speed.utils;

//import com.arpa.speed.boxDatabase.boxFitSerialized;

import com.arpa.speed.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RxRetoGenerator {

    public static <S> S createPingService(Class<S> serviceClass, String serverAddress) {

        serverAddress = "http://" + serverAddress;

        HttpLoggingInterceptor loggerInterceptorBody = new HttpLoggingInterceptor();
        loggerInterceptorBody.setLevel(HttpLoggingInterceptor.Level.BODY);

        HttpLoggingInterceptor loggerInterceptorHeader = new HttpLoggingInterceptor();
        loggerInterceptorHeader.setLevel(HttpLoggingInterceptor.Level.HEADERS);

        OkHttpClient.Builder httpclient = new OkHttpClient.Builder();
        httpclient.addInterceptor(chain -> {
            Request original = chain.request();

            Request request = original.newBuilder()
                    //.header("Connection", "close")
                    .header("Content-Encoding", "gzip, deflate")
                    .header("Content-Type", "application/json;charset=utf-8")
                    .header("Accept", "application/json")
                    .method(original.method(), original.body())
                    .build();

            return chain.proceed(request);
        });

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpclient.addInterceptor(logging);
        }

        OkHttpClient client = httpclient
                .addInterceptor(loggerInterceptorBody)
                .addInterceptor(loggerInterceptorHeader)
                .readTimeout(30L, TimeUnit.SECONDS)
                .connectTimeout(30L, TimeUnit.SECONDS)
                .build();


        Retrofit retrofit = (new Retrofit.Builder())
                .baseUrl(serverAddress)
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                //.addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create()).
                        build();

        return retrofit.create(serviceClass);
    }


    public static <S> S createService(Class<S> serviceClass, String serverAddress) {

        serverAddress = "http://" + serverAddress;

        HttpLoggingInterceptor loggerInterceptorBody = new HttpLoggingInterceptor();
        loggerInterceptorBody.setLevel(HttpLoggingInterceptor.Level.BODY);

        HttpLoggingInterceptor loggerInterceptorHeader = new HttpLoggingInterceptor();
        loggerInterceptorHeader.setLevel(HttpLoggingInterceptor.Level.HEADERS);

        OkHttpClient client = (new OkHttpClient.Builder())
                .addInterceptor(chain -> {
                    Request request = chain.request().newBuilder()
                            .addHeader("Authorization", "Bearer $token")
                            .addHeader("Connection", "close")
                            .addHeader("Content-Encoding", "gzip, deflate")
                            .addHeader("Content-Type", "application/json;charset=utf-8")
                            .build();
                    return chain.proceed(request);
                })
                .addInterceptor(loggerInterceptorBody)
                .addInterceptor(loggerInterceptorHeader)
                .readTimeout(30L, TimeUnit.SECONDS)
                .connectTimeout(30L, TimeUnit.SECONDS)
                .build();


        Retrofit retrofit = (new Retrofit.Builder())
                .baseUrl(serverAddress)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create()).
                        addCallAdapterFactory(RxJava2CallAdapterFactory.create()).
                        build();

        return retrofit.create(serviceClass);
    }


}
