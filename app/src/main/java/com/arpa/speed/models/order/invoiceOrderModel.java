package com.arpa.speed.models.order;

public class invoiceOrderModel {

    public long Id;

    public long transactionID;
    public int translineID;
    public int itemID;
    public String itemCode;
    public String itemName;
    public Integer itemCategoryID;
    public Double salePrice;
    public Double unitsRatio;
    public Double inverseUnitsRatio;
    public int itemCount;
    public Double rowSum;
    public Double totalSum;
    public int totalCount;
    public long tableID;
    public long tableName;
    public double paidAmount;
    public int transNumber;
    public boolean isArchived;


    public long getTableName() {
        return tableName;
    }

    public void setTableName(long tableName) {
        this.tableName = tableName;
    }

    public boolean isArchived() {
        return isArchived;
    }

    public void setArchived(boolean archived) {
        isArchived = archived;
    }

    public int getTransNumber() {
        return transNumber;
    }

    public void setTransNumber(int transNumber) {
        this.transNumber = transNumber;
    }

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public double getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(double paidAmount) {
        this.paidAmount = paidAmount;
    }

    public invoiceOrderModel() {
    }

    public invoiceOrderModel(long transactionID, int itemID, String itemCode,
                             String itemName, Integer itemCategoryID, Double salePrice,
                             Double unitsRatio, Double inverseUnitsRatio, int itemCount,
                             Double rowSum, Double totalSum, long tableID, int totalCount, int translineID) {
        this.transactionID = transactionID;
        this.translineID = translineID;
        this.itemCode = itemCode;
        this.itemName = itemName;
        this.itemCategoryID = itemCategoryID;
        this.salePrice = salePrice;
        this.unitsRatio = unitsRatio;
        this.inverseUnitsRatio = inverseUnitsRatio;
        this.itemCount = itemCount;
        this.rowSum = rowSum;
        this.totalSum = totalSum;
        this.totalCount = totalCount;
        this.tableID = tableID;
    }

    public int getTranslineID() {
        return translineID;
    }

    public void setTranslineID(int translineID) {
        this.translineID = translineID;
    }

    public int getItemID() {
        return itemID;
    }

    public long getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(long transactionID) {
        this.transactionID = transactionID;
    }

    public void setItemID(int itemID) {
        this.itemID = itemID;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Integer getItemCategoryID() {
        return itemCategoryID;
    }

    public void setItemCategoryID(Integer itemCategoryID) {
        this.itemCategoryID = itemCategoryID;
    }

    public Double getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Double salePrice) {
        this.salePrice = salePrice;
    }

    public Double getUnitsRatio() {
        return unitsRatio;
    }

    public void setUnitsRatio(Double unitsRatio) {
        this.unitsRatio = unitsRatio;
    }

    public Double getInverseUnitsRatio() {
        return inverseUnitsRatio;
    }

    public void setInverseUnitsRatio(Double inverseUnitsRatio) {
        this.inverseUnitsRatio = inverseUnitsRatio;
    }

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    public Double getRowSum() {
        return rowSum;
    }

    public void setRowSum(Double rowSum) {
        this.rowSum = rowSum;
    }

    public Double getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(Double totalSum) {
        this.totalSum = totalSum;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public long getTableID() {
        return tableID;
    }

    public void setTableID(long tableID) {
        this.tableID = tableID;
    }

    @Override
    public String toString() {
        return "invoiceOrderModel{" +
                "itemID=" + itemID +
                ", itemCode='" + itemCode + '\'' +
                ", itemName='" + itemName + '\'' +
                ", itemCategoryID=" + itemCategoryID +
                ", salePrice=" + salePrice +
                ", unitsRatio=" + unitsRatio +
                ", inverseUnitsRatio=" + inverseUnitsRatio +
                ", itemCount=" + itemCount +
                ", rowSum=" + rowSum +
                ", totalSum=" + totalSum +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        //return super.equals(o);

        if (o == this) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (o.getClass() == this.getClass()) {
            long a = this.itemID;
            long b = ((invoiceOrderModel) o).itemID;
            return a == b;
        }
        return false;
    }
}
