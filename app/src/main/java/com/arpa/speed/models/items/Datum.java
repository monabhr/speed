
package com.arpa.speed.models.items;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("itemID")
    @Expose
    private Integer itemID;
    @SerializedName("itemCode")
    @Expose
    private String itemCode;
    @SerializedName("itemName")
    @Expose
    private String itemName;
    @SerializedName("itemCategoryID")
    @Expose
    private Integer itemCategoryID;
    @SerializedName("salePrice")
    @Expose
    
    private Double salePrice;
    @SerializedName("unitsRatio")
    @Expose

    private Double unitsRatio;
    @SerializedName("inverseUnitsRatio")
    @Expose
    private Double inverseUnitsRatio;

    public Integer getItemID() {
        return itemID;
    }

    public void setItemID(Integer itemID) {
        this.itemID = itemID;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Integer getItemCategoryID() {
        return itemCategoryID;
    }

    public void setItemCategoryID(Integer itemCategoryID) {
        this.itemCategoryID = itemCategoryID;
    }

    public Double getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Double salePrice) {
        this.salePrice = salePrice;
    }

    public Double getUnitsRatio() {
        return unitsRatio;
    }

    public void setUnitsRatio(Double unitsRatio) {
        this.unitsRatio = unitsRatio;
    }

    public Double getInverseUnitsRatio() {
        return inverseUnitsRatio;
    }

    public void setInverseUnitsRatio(Double inverseUnitsRatio) {
        this.inverseUnitsRatio = inverseUnitsRatio;
    }

}
