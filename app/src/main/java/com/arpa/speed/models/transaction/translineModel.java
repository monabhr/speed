package com.arpa.speed.models.transaction;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

public class translineModel {

    @SerializedName("transLineID")
    public int transLineID;

    @SerializedName("itemID")
    public int itemID;

    @SerializedName("stockAreaID")
    public int stockAreaID;

    @SerializedName("transLineLinkedTo")
    public int transLineLinkedTo;

    @SerializedName("nQty")
    public BigDecimal nQty;

    @SerializedName("price")
    public double price;

    @SerializedName("amount")
    public double amount;

    @SerializedName("discountAmount")
    public double discountAmount;

    @SerializedName("discountPercent")
    public double discountPercent;

    @SerializedName("taxAmount")
    public double taxAmount;

    @SerializedName("tollAmount")
    public double tollAmount;

    @SerializedName("transactionID")
    public long transactionID;

    public long getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(long transactionID) {
        this.transactionID = transactionID;
    }

    public int getTransLineID() {
        return transLineID;
    }

    public void setTransLineID(int transLineID) {
        this.transLineID = transLineID;
    }

    public int getItemID() {
        return itemID;
    }

    public void setItemID(int itemID) {
        this.itemID = itemID;
    }

    public int getStockAreaID() {
        return stockAreaID;
    }

    public void setStockAreaID(int stockAreaID) {
        this.stockAreaID = stockAreaID;
    }

    public int getTransLineLinkedTo() {
        return transLineLinkedTo;
    }

    public void setTransLineLinkedTo(int transLineLinkedTo) {
        this.transLineLinkedTo = transLineLinkedTo;
    }

    public BigDecimal getnQty() {
        return nQty;
    }

    public void setnQty(BigDecimal nQty) {
        this.nQty = nQty;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public double getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(double discountPercent) {
        this.discountPercent = discountPercent;
    }

    public double getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(double taxAmount) {
        this.taxAmount = taxAmount;
    }

    public double getTollAmount() {
        return tollAmount;
    }

    public void setTollAmount(double tollAmount) {
        this.tollAmount = tollAmount;
    }
}
