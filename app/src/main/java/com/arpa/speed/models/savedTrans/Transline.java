
package com.arpa.speed.models.savedTrans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Transline {

    @SerializedName("translineId")
    @Expose
    private Integer translineId;
    @SerializedName("nQty")
    @Expose
    private Integer nQty;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("taxAmount")
    @Expose
    private Integer taxAmount;
    @SerializedName("tollAmount")
    @Expose
    private Integer tollAmount;
    @SerializedName("discountPercent")
    @Expose
    private Integer discountPercent;
    @SerializedName("discountAmount")
    @Expose
    private Integer discountAmount;
    @SerializedName("itemName")
    @Expose
    private String itemName;

    @SerializedName("itemId")
    @Expose
    private Integer itemId;

    @SerializedName("transactionId")
    @Expose
    private Integer transactionId;

    @SerializedName("itemCategoryId")
    @Expose
    private Integer itemCategoryId;


    public Integer getItemCategoryId() {
        return itemCategoryId;
    }

    public void setItemCategoryId(Integer itemCategoryId) {
        this.itemCategoryId = itemCategoryId;
    }

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    public Integer getTranslineId() {
        return translineId;
    }

    public Integer getnQty() {
        return nQty;
    }

    public void setnQty(Integer nQty) {
        this.nQty = nQty;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public void setTranslineId(Integer translineId) {
        this.translineId = translineId;
    }

    public Integer getNQty() {
        return nQty;
    }

    public void setNQty(Integer nQty) {
        this.nQty = nQty;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(Integer taxAmount) {
        this.taxAmount = taxAmount;
    }

    public Integer getTollAmount() {
        return tollAmount;
    }

    public void setTollAmount(Integer tollAmount) {
        this.tollAmount = tollAmount;
    }

    public Integer getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(Integer discountPercent) {
        this.discountPercent = discountPercent;
    }

    public Integer getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(Integer discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

}
