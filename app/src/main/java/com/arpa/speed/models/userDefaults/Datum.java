
package com.arpa.speed.models.userDefaults;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("defaultTransStateID")
    @Expose
    private Integer defaultTransStateID;
    @SerializedName("defStockID")
    @Expose
    private Integer defStockID;
    @SerializedName("defCustomerID")
    @Expose
    private Integer defCustomerID;

    public Integer getDefaultTransStateID() {
        return defaultTransStateID;
    }

    public void setDefaultTransStateID(Integer defaultTransStateID) {
        this.defaultTransStateID = defaultTransStateID;
    }

    public Integer getDefStockID() {
        return defStockID;
    }

    public void setDefStockID(Integer defStockID) {
        this.defStockID = defStockID;
    }

    public Integer getDefCustomerID() {
        return defCustomerID;
    }

    public void setDefCustomerID(Integer defCustomerID) {
        this.defCustomerID = defCustomerID;
    }

}
