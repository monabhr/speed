
package com.arpa.speed.models.restTableCat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@BoxfitClass
public class Datum {
    //@BoxfitField
    @SerializedName("restaurantTableCategoryID")
    @Expose
    private Integer restaurantTableCategoryID;

    //@BoxfitField
    @SerializedName("restaurantTableCategoryCode")
    @Expose
    private String restaurantTableCategoryCode;

    //@BoxfitField
    @SerializedName("restaurantTableCategoryName")
    @Expose
    private String restaurantTableCategoryName;

    //@BoxfitField
    @SerializedName("descriptions")
    @Expose
    private String descriptions;

    public Integer getRestaurantTableCategoryID() {
        return restaurantTableCategoryID;
    }

    public void setRestaurantTableCategoryID(Integer restaurantTableCategoryID) {
        this.restaurantTableCategoryID = restaurantTableCategoryID;
    }

    public String getRestaurantTableCategoryCode() {
        return restaurantTableCategoryCode;
    }

    public void setRestaurantTableCategoryCode(String restaurantTableCategoryCode) {
        this.restaurantTableCategoryCode = restaurantTableCategoryCode;
    }

    public String getRestaurantTableCategoryName() {
        return restaurantTableCategoryName;
    }

    public void setRestaurantTableCategoryName(String restaurantTableCategoryName) {
        this.restaurantTableCategoryName = restaurantTableCategoryName;
    }

    public String getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String descriptions) {
        this.descriptions = descriptions;
    }

}
