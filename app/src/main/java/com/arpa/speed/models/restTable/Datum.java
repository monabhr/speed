
package com.arpa.speed.models.restTable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("restaurantTableID")
    @Expose
    private Integer restaurantTableID;
    @SerializedName("tableName")
    @Expose
    private String tableName;
    @SerializedName("tableCode")
    @Expose
    private String tableCode;
    @SerializedName("tableNumber")
    @Expose
    private Integer tableNumber;
    @SerializedName("restaurantTableCategoryID")
    @Expose
    private Integer restaurantTableCategoryID;

    public Integer getRestaurantTableID() {
        return restaurantTableID;
    }

    public void setRestaurantTableID(Integer restaurantTableID) {
        this.restaurantTableID = restaurantTableID;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getTableCode() {
        return tableCode;
    }

    public void setTableCode(String tableCode) {
        this.tableCode = tableCode;
    }

    public Integer getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(Integer tableNumber) {
        this.tableNumber = tableNumber;
    }

    public Integer getRestaurantTableCategoryID() {
        return restaurantTableCategoryID;
    }

    public void setRestaurantTableCategoryID(Integer restaurantTableCategoryID) {
        this.restaurantTableCategoryID = restaurantTableCategoryID;
    }

}
