
package com.arpa.speed.models.savedTrans;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("transactionID")
    @Expose
    private Integer transactionID;
    @SerializedName("transDate")
    @Expose
    private String transDate;
    @SerializedName("businessName")
    @Expose
    private String businessName;
    @SerializedName("transNumber")
    @Expose
    private Integer transNumber;
    @SerializedName("docAliasID")
    @Expose
    private Integer docAliasID;
    @SerializedName("totalSum")
    @Expose
    private Integer totalSum;
    @SerializedName("creatorUserID")
    @Expose
    private Integer creatorUserID;
    @SerializedName("transDiscountAmount")
    @Expose
    private Integer transDiscountAmount;
    @SerializedName("transDiscountPercent")
    @Expose
    private Integer transDiscountPercent;
    @SerializedName("businessID")
    @Expose
    private Integer businessID;

    @SerializedName("restaurantTableID")
    @Expose
    private Integer restaurantTableID;

    @SerializedName("paidAmount")
    @Expose
    private Integer paidAmount;

    @SerializedName("translines")
    @Expose
    private List<Transline> translines = null;


    public Integer getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(Integer paidAmount) {
        this.paidAmount = paidAmount;
    }

    public Integer getRestaurantTableID() {
        return restaurantTableID;
    }

    public void setRestaurantTableID(Integer restaurantTableID) {
        this.restaurantTableID = restaurantTableID;
    }



    public Integer getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(Integer transactionID) {
        this.transactionID = transactionID;
    }

    public String getTransDate() {
        return transDate;
    }

    public void setTransDate(String transDate) {
        this.transDate = transDate;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public Integer getTransNumber() {
        return transNumber;
    }

    public void setTransNumber(Integer transNumber) {
        this.transNumber = transNumber;
    }

    public Integer getDocAliasID() {
        return docAliasID;
    }

    public void setDocAliasID(Integer docAliasID) {
        this.docAliasID = docAliasID;
    }

    public Integer getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(Integer totalSum) {
        this.totalSum = totalSum;
    }

    public Integer getCreatorUserID() {
        return creatorUserID;
    }

    public void setCreatorUserID(Integer creatorUserID) {
        this.creatorUserID = creatorUserID;
    }

    public Integer getTransDiscountAmount() {
        return transDiscountAmount;
    }

    public void setTransDiscountAmount(Integer transDiscountAmount) {
        this.transDiscountAmount = transDiscountAmount;
    }

    public Integer getTransDiscountPercent() {
        return transDiscountPercent;
    }

    public void setTransDiscountPercent(Integer transDiscountPercent) {
        this.transDiscountPercent = transDiscountPercent;
    }

    public Integer getBusinessID() {
        return businessID;
    }

    public void setBusinessID(Integer businessID) {
        this.businessID = businessID;
    }

    public List<Transline> getTranslines() {
        return translines;
    }

    public void setTranslines(List<Transline> translines) {
        this.translines = translines;
    }

}
