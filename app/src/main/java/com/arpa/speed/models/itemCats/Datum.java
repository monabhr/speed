
package com.arpa.speed.models.itemCats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("itemCategoryID")
    @Expose
    private Integer itemCategoryID;
    @SerializedName("itemCategoryName")
    @Expose
    private String itemCategoryName;
    @SerializedName("itemCategoryCode")
    @Expose
    private String itemCategoryCode;

    public Integer getItemCategoryID() {
        return itemCategoryID;
    }

    public void setItemCategoryID(Integer itemCategoryID) {
        this.itemCategoryID = itemCategoryID;
    }

    public String getItemCategoryName() {
        return itemCategoryName;
    }

    public void setItemCategoryName(String itemCategoryName) {
        this.itemCategoryName = itemCategoryName;
    }

    public String getItemCategoryCode() {
        return itemCategoryCode;
    }

    public void setItemCategoryCode(String itemCategoryCode) {
        this.itemCategoryCode = itemCategoryCode;
    }

}
