package com.arpa.speed.models.transaction;

import com.google.gson.annotations.SerializedName;

public class transactionModel {

    @SerializedName("transactionID")
    public long transactionID;

    @SerializedName("transNumber")
    public int transNumber;

    @SerializedName("businessID")
    public int businessID;

    @SerializedName("docAliasID")
    public int docAliasID;

    @SerializedName("transferedStockAreaID")
    public int transferedStockAreaID;

    @SerializedName("fiscalYearID")
    public int fiscalYearID;

    @SerializedName("transactionLinkedTo")
    public int transactionLinkedTo;

    @SerializedName("sumOfCogs")
    public double sumOfCogs;

    @SerializedName("transDiscountAmount")
    public double transDiscountAmount;

    @SerializedName("transDiscountPercent")
    public double transDiscountPercent;

    @SerializedName("userID")
    public int userID;

    @SerializedName("paidAmount")
    public double paidAmount;

    @SerializedName("restaurantTableID")
    public int restaurantTableID;

    public long getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(long transactionID) {
        this.transactionID = transactionID;
    }

    public int getTransNumber() {
        return transNumber;
    }

    public void setTransNumber(int transNumber) {
        this.transNumber = transNumber;
    }

    public int getBusinessID() {
        return businessID;
    }

    public void setBusinessID(int businessID) {
        this.businessID = businessID;
    }

    public int getDocAliasID() {
        return docAliasID;
    }

    public void setDocAliasID(int docAliasID) {
        this.docAliasID = docAliasID;
    }

    public int getTransferedStockAreaID() {
        return transferedStockAreaID;
    }

    public void setTransferedStockAreaID(int transferedStockAreaID) {
        this.transferedStockAreaID = transferedStockAreaID;
    }

    public int getFiscalYearID() {
        return fiscalYearID;
    }

    public void setFiscalYearID(int fiscalYearID) {
        this.fiscalYearID = fiscalYearID;
    }

    public int getTransactionLinkedTo() {
        return transactionLinkedTo;
    }

    public void setTransactionLinkedTo(int transactionLinkedTo) {
        this.transactionLinkedTo = transactionLinkedTo;
    }

    public double getSumOfCogs() {
        return sumOfCogs;
    }

    public void setSumOfCogs(double sumOfCogs) {
        this.sumOfCogs = sumOfCogs;
    }

    public double getTransDiscountAmount() {
        return transDiscountAmount;
    }

    public void setTransDiscountAmount(double transDiscountAmount) {
        this.transDiscountAmount = transDiscountAmount;
    }

    public double getTransDiscountPercent() {
        return transDiscountPercent;
    }

    public void setTransDiscountPercent(double transDiscountPercent) {
        this.transDiscountPercent = transDiscountPercent;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public double getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(double paidAmount) {
        this.paidAmount = paidAmount;
    }

    public int getRestaurantTableID() {
        return restaurantTableID;
    }

    public void setRestaurantTableID(int restaurantTableID) {
        this.restaurantTableID = restaurantTableID;
    }
}
