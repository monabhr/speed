package com.arpa.speed.models.restTableCat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
//import com.manuege.boxfit.annotations.BoxfitClass;

import java.util.List;
//@BoxfitClass
public class restTableCatDbModel {
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("error")
    @Expose
    private String error;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

}
