package com.arpa.speed.models.transaction;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ObjectToSend {

    @SerializedName("data")
    public transactionModel data;

    @SerializedName("items")
    public List<translineModel> items;


    public ObjectToSend() {
    }
}
